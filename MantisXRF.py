'''
Created on Feb 7, 2014

@author: Mirna Lerotic, 2nd Look Consulting
http://2ndlookconsulting.com
'''

import sys
import os
import numpy as np
import time
import csv
import datetime 
import h5py

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import Qt, QCoreApplication


import matplotlib 
matplotlib.rcParams['backend.qt4'] = 'PyQt4'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.gridspec as gridspec
matplotlib.interactive( True )
matplotlib.rcParams['svg.fonttype'] = 'none'
from matplotlib.text import Text



import file_dls_nexus_hdf5
import file_anka_h5
import file_maps_h5
import analysis
#import mapspy_helper
import fitting_xrf
import fit_parameters

try:
    import xrftomo.xt_tools as xt
    havexrftomo = 1
except:
    havexrftomo = 0
    xt = None
    

Winsizex = 1050
Winsizey = 750

PlotH = 4.0
PlotW = PlotH*1.61803

version = '1.0.11'

def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

#----------------------------------------------------------------------
class DataStack:
    def __init__(self):
        
        self.stack_loaded = 0
        self.ROI_maps_loaded = 0
        
                
        self.path = ''
        self.filename = ''

        self.stack = []
        self.fullSpectrum = []
        self.dScanPars = None
        self.realX = []
        self.realY = []
        self.stack_ncols = 0
        self.stack_nrows = 0
        self.inenergy = 0
        self.channels = []
        
        self.ecurrent = []
        self.ecmean = 0
        
        self.ioncount = []
        
        self.tomodata = 0
        self.fullSpectrum4D = []
        self.ecurrent4D = []
        self.theta = []
        self.ROI_maps4D = []
        self.fit_maps4D = []
        self.ROI_maps4D_calculated = 0        
        
        
        self.elementstofit = []

        
        self.fits_calculated = 0
        self.fits4D_calculated = 0
        self.fit_maps = []
        self.fitmap_names = []
        
                
        self.ROI_maps_calculated = 0        
        self.ROI_info = []
        self.ROI_names = []
        self.ROI_maps = []
        
       
        self.fits_calculated = []
        self.fit_maps = []
        self.fit_chanel_names = []
        
        
        self.engcal_slope = 10.
        self.engcal_offset = 0.
        
        self.mp_outdir = ''
        
        #File format:
        # 1 = '.nxs'
        # 2 = '.hdf5'
        self.fileformat = ''
        
        self.monitored_folder = ''
        
        self.fontsize_labels = 8
        self.fontsize_title = 12
        
        
        #Anka multiimage stacks
        self.multiimg = 0
        self.readimg = 0
        self.anka_glitch = 0
        


""" ------------------------------------------------------------------------------------------------"""
class PageXT(QtGui.QWidget):
    def __init__(self, datastack):
        super(PageXT, self).__init__()

        self.initUI(datastack)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack): 
        
        self.datastack = datastack
        
        if havexrftomo:
            self.tr = xt.Cxrftomo()
        else:
            self.tr = None
        
        self.tomo_calculated = 0
        

        self.icomp = 0
        self.islice = 0
        
        self.maxIters = 10
      

        #panel 2
        sizer2 = QtGui.QGroupBox('Tomography Reconstruction')
        vbox2 = QtGui.QVBoxLayout()
        vbox2.setContentsMargins(10,10,10,10)


        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setFrameShadow(QtGui.QFrame.Sunken) 
        
        self.button_calctomo = QtGui.QPushButton( 'Calculate SIRT')
        self.button_calctomo.clicked.connect( self.OnCalcTomo)
        self.button_calctomo.setEnabled(False)
        vbox2.addWidget(self.button_calctomo)
        

        vbox2.addStretch(1)
        vbox2.addWidget(line) 
        vbox2.addStretch(1)  
 
        hbox22 = QtGui.QHBoxLayout()
        text1 = QtGui.QLabel(self)
        text1.setText('Number of iterations')
        hbox22.addWidget(text1)
        hbox22.addStretch(1)
        self.ntc_niterations = QtGui.QLineEdit(self)
        self.ntc_niterations.setFixedWidth(65)
        self.ntc_niterations.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_niterations.setAlignment(QtCore.Qt.AlignRight)   
        self.ntc_niterations.setText(str(self.maxIters))
        hbox22.addWidget(self.ntc_niterations)  
  
        vbox2.addLayout(hbox22) 
          
                
#         hbox23 = QtGui.QHBoxLayout()
#         tc1 = QtGui.QLabel(self)
#         tc1.setText("CS Parameter Beta")  
#         hbox23.addWidget(tc1)      
#         self.ntc_beta = QtGui.QLineEdit(self)
#         self.ntc_beta.setFixedWidth(65)
#         self.ntc_beta.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
#         self.ntc_beta.setAlignment(QtCore.Qt.AlignRight)         
#         hbox23.addStretch(1)
#         self.ntc_beta.setText(str(self.beta))
#         hbox23.addWidget(self.ntc_beta) 
#         
#         vbox2.addLayout(hbox23) 
#         
#         
#         hbox24 = QtGui.QHBoxLayout()
#         text1 = QtGui.QLabel(self)
#         text1.setText('Sample Thickness')
#         hbox24.addWidget(text1)
#         hbox24.addStretch(1)
#         self.ntc_samplethick = QtGui.QLineEdit(self)
#         self.ntc_samplethick.setFixedWidth(65)
#         self.ntc_samplethick.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
#         self.ntc_samplethick.setAlignment(QtCore.Qt.AlignRight)   
#         self.ntc_samplethick.setText(str(self.samplethick))
#         hbox24.addWidget(self.ntc_samplethick)  
#   
#         vbox2.addLayout(hbox24) 
      
        
        sizer2.setLayout(vbox2)

 
        #panel 3
        sizer3 = QtGui.QGroupBox('Results')
        vbox3 = QtGui.QVBoxLayout()
 
         
        self.button_save = QtGui.QPushButton( 'Save as .mrc')
        self.button_save.clicked.connect( self.OnSaveMrc)
        self.button_save.setEnabled(False)
        vbox3.addWidget(self.button_save)
         
        sizer3.setLayout(vbox3)

 
        #panel 5     
        vbox5 = QtGui.QVBoxLayout()
        
        self.tc_imagecomp = QtGui.QLabel(self)
        self.tc_imagecomp.setText("Tomographic reconstruction")
        vbox5.addWidget(self.tc_imagecomp)
        

        gridsizertop = QtGui.QGridLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.absimgfig = Figure((PlotH*1.25, PlotH*1.25))

        self.AbsImagePanel = FigureCanvas(self.absimgfig)
        self.AbsImagePanel.setParent(self)
        
        
        fbox.addWidget(self.AbsImagePanel)
        frame.setLayout(fbox)
        gridsizertop.addWidget(frame, 0, 0, QtCore .Qt. AlignLeft)
        

        self.slider_slice = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.slider_slice.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_slice.valueChanged[int].connect(self.OnScrollSlice)
        self.slider_slice.setRange(0, 100)
        
        gridsizertop.addWidget(self.slider_slice, 0, 1, QtCore .Qt. AlignLeft)
        
        
        self.slider_elemnt = QtGui.QScrollBar(QtCore.Qt.Horizontal)
        self.slider_elemnt.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_elemnt.valueChanged[int].connect(self.OnScrollElemnt)
        self.slider_elemnt.setRange(0, 100)     
        #self.slider_elemnt.setVisible(False)  
        self.tc_1 = QtGui.QLabel(self)
        self.tc_1.setText("Element: ")
        #self.tc_1.setVisible(False)
        hbox51 = QtGui.QHBoxLayout()
        hbox51.addWidget(self.tc_1) 
        hbox51.addWidget(self.slider_elemnt)
        gridsizertop.addLayout(hbox51, 1, 0)
        

        vbox5.addLayout(gridsizertop)
        vbox5.addStretch(1)
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
#         vboxt1.addWidget(sizer1)
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer2)
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer3)
        vboxt1.addStretch (1)
        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vbox5)
        hboxtop.addStretch (0.5)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.9)

        vboxtop.setContentsMargins(50,50,50,50)
        self.setLayout(vboxtop)
        
        
        
#----------------------------------------------------------------------          
    def OnCalcTomo(self, event):
        
        if self.window().page1.showmaps == 1:
            data = self.datastack.ROI_maps4D
        else:
            data = self.datastack.fit_maps4D
            
        theta = self.datastack.theta
        
        
        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  
              
        value = self.ntc_niterations.text()
        maxIters = int(value)  

                
        self.tomorecons = self.tr.calcxrftomo(data, theta, maxIters)
        
        self.tomo_calculated = 1
        
        self.button_save.setEnabled(True)
        
        dims = self.tomorecons.shape
         
        self.islice = int(dims[3]/2)
         
        self.slider_slice.setRange(0, dims[3]-1)
         
        self.tc_1.setText('Element: '+ self.datastack.ROI_names[self.icomp])
        
        self.slider_elemnt.setRange(0, len(self.datastack.ROI_names)-1)   
         
        self.ShowImage()
        
        
        QtGui.QApplication.restoreOverrideCursor()
        
        
#----------------------------------------------------------------------            
    def OnScrollSlice(self, value):
        self.islice = value

        self.ShowImage()
        
        
#----------------------------------------------------------------------            
    def OnScrollElemnt(self, value):
        self.icomp = value
        
        self.tc_1.setText('Element: '+ self.datastack.ROI_names[self.icomp])

        self.ShowImage()
        

#----------------------------------------------------------------------          
    def OnSaveMrc(self, event):       
        
        wildcard = "Mrc files (*.mrc)"

        filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save results as .mrc (.mrc)', '', wildcard)

        filepath = str(filepath)
        if filepath == '':
            return
        
        self.tr.savemrc(filepath, self.datastack.ROI_names)
        
#----------------------------------------------------------------------        
    def ShowImage(self):
        
        if self.tomo_calculated == 0:
            return
        
        image = self.tomorecons[self.icomp,:,:,self.islice].copy() 

        fig = self.absimgfig
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes.imshow(np.rot90(image), cmap=matplotlib.cm.get_cmap("gray")) 
         
#         if self.window().page1.show_scale_bar == 1:
#             #Show Scale Bar
#             if self.com.white_scale_bar == 1:
#                 sbcolor = 'white'
#             else:
#                 sbcolor = 'black'
#             startx = int(self.stk.n_cols*0.05)
#             starty = self.stk.n_rows-int(self.stk.n_rows*0.05)-self.stk.scale_bar_pixels_y
#             um_string = ' $\mathrm{\mu m}$'
#             microns = '$'+self.stk.scale_bar_string+' $'+um_string
#             axes.text(self.stk.scale_bar_pixels_x+startx+1,starty+1, microns, horizontalalignment='left', verticalalignment='center',
#                       color = sbcolor, fontsize=14)
#             #Matplotlib has flipped scales so I'm using rows instead of cols!
#             p = matplotlib.patches.Rectangle((startx,starty), self.stk.scale_bar_pixels_x, self.stk.scale_bar_pixels_y,
#                                    color = sbcolor, fill = True)
#             axes.add_patch(p)
             
        
        axes.axis("off")      
        self.AbsImagePanel.draw()
         
        
#----------------------------------------------------------------------        
    def UpdateDisplay(self):            
            
        if self.datastack.ROI_maps4D_calculated == 1 or self.datastack.fits4D_calculated == 1:
            self.button_calctomo.setEnabled(True)
        else:
            self.button_calctomo.setEnabled(False)      
                
         
 
""" ------------------------------------------------------------------------------------------------"""
class PageFit(QtGui.QWidget):
    def __init__(self, datastack):
        super(PageFit, self).__init__()

        self.initUI(datastack)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack): 
        
        self.datastack = datastack
        
        self.beamlinelist = ['ANKA-NANO', 'DLS-I08','2-ID-E']
        
        
        #panel 1
        sizer1 = QtGui.QGroupBox('Spectra Fitting')
        vbox1 = QtGui.QVBoxLayout()


        self.button_calcint = QtGui.QPushButton('Fit Integrated Spectrum')
        self.button_calcint.clicked.connect( self.OnCalcIntSpecFit)
        self.button_calcint.setMinimumWidth(200)
        vbox1.addWidget(self.button_calcint)       
        
        self.button_calc = QtGui.QPushButton('Calculate Fit Maps')
        self.button_calc.clicked.connect( self.OnCalcFits)
        self.button_calc.setMinimumWidth(200)
        #self.button_calc.setEnabled(False)
        vbox1.addWidget(self.button_calc)
        
        
        self.button_save = QtGui.QPushButton('Save Fit Plot')
        self.button_save.clicked.connect( self.OnSaveFitPlot)   
        self.button_save.setEnabled(False)
        vbox1.addWidget(self.button_save)
        
        self.button_savesp = QtGui.QPushButton('Save Fit Spectra')
        self.button_savesp.clicked.connect( self.OnSaveFitSpectra)   
        self.button_savesp.setEnabled(False)
        vbox1.addWidget(self.button_savesp)
        
        
        if havexrftomo:
            line = QtGui.QFrame()
            line.setFrameShape(QtGui.QFrame.HLine)
            line.setFrameShadow(QtGui.QFrame.Sunken) 
            vbox1.addWidget(line)
        
            self.button_calc_4D = QtGui.QPushButton('Calculate Maps for all angles')
            self.button_calc_4D.clicked.connect(self.OnCalcFits4D)
            self.button_calc_4D.setEnabled(False)
            vbox1.addWidget(self.button_calc_4D)
        
        sizer1.setLayout(vbox1)
        
        
#         #panel2
#         sizer2 = QtGui.QGroupBox('Fit Parameters')
#         vbox2 = QtGui.QVBoxLayout()
#         
#         self.button_fpfile = QtGui.QPushButton('Edit Fit Parameters')
#         #self.button_fpfile.clicked.connect( self.OnFitParams)
#         self.button_fpfile.setMinimumWidth(200)
#         vbox2.addWidget(self.button_fpfile)
#         
#         sizer2.setLayout(vbox2)

        
        
        #panel 3
        sizer3 = QtGui.QGroupBox('Settings')
        vbox3 = QtGui.QVBoxLayout()
        
   
        
        self.tc_elements = QtGui.QLabel(self)
        vbox3.addWidget(self.tc_elements)
        self.tc_elements.setText('Elements: ') 
        
        hbox30 = QtGui.QHBoxLayout()
        self.tc_inceng = QtGui.QLabel(self)
        hbox30.addWidget(self.tc_inceng)
        self.tc_inceng.setText('Incident Beam Energy: ') 
        self.ntc_inceng = QtGui.QDoubleSpinBox(self)  
        self.ntc_inceng.setRange(0, 20000)
        self.ntc_inceng.setAlignment(QtCore.Qt.AlignRight)
        self.ntc_inceng.setFixedWidth(85)
        self.ntc_inceng.valueChanged.connect(self.OnIncEng)
        hbox30.addWidget(self.ntc_inceng)   
        tc = QtGui.QLabel(self)
        tc.setText("eV")  
        hbox30.addWidget(tc)  
        hbox30.addStretch(1)
        vbox3.addLayout(hbox30) 

        vbox3.addStretch(1)
        
        sizer31 = QtGui.QGroupBox('Energy Calibration')
        vbox31 = QtGui.QVBoxLayout()
        
#         self.cb_engaxis = QtGui.QCheckBox('ROI', self)
#         self.cb_engaxis.setChecked(self.scattroi)
#         self.cb_engaxis.stateChanged.connect(self.OnScatterROI)
#         vbox31.addWidget(self.cb_engaxis)               

        hbox31 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Slope  ")  
        hbox31.addWidget(tc)
        self.ntc_ecals = QtGui.QLineEdit(self)
        self.ntc_ecals.setFixedWidth(65)
        self.ntc_ecals.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_ecals.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_ecals.setText(str(self.datastack.engcal_slope))
        #self.ntc_ecals.textChanged.connect(self.OnEnergyCal)
        self.ntc_ecals.setReadOnly(True)
        hbox31.addWidget(self.ntc_ecals) 
        tc = QtGui.QLabel(self)
        tc.setText("eV/channel")  
        hbox31.addWidget(tc)  
        hbox31.addStretch(1)   
        
        hbox32 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Offset")  
        hbox32.addWidget(tc)
        self.ntc_ecalo = QtGui.QLineEdit(self)
        self.ntc_ecalo.setFixedWidth(65)
        self.ntc_ecalo.setValidator(QtGui.QDoubleValidator(-99999, 99999, 2, self))
        self.ntc_ecalo.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_ecalo.setText(str(self.datastack.engcal_offset))
        #self.ntc_ecalo.textChanged.connect(self.OnEnergyCal)
        self.ntc_ecalo.setReadOnly(True)
        hbox32.addWidget(self.ntc_ecalo) 
        tc = QtGui.QLabel(self)
        tc.setText("eV")  
        hbox32.addWidget(tc)      

        hbox32.addStretch(1)
        vbox31.addLayout(hbox31) 
        vbox31.addLayout(hbox32)          
        sizer31.setLayout(vbox31)
        vbox3.addWidget(sizer31)
        
        #hbox3.addStretch(1)
        
        vbox3.setContentsMargins(20,20,20,20)
        sizer3.setLayout(vbox3)

        
        #panel 5     
        vbox5 = QtGui.QVBoxLayout()
        
        self.tc_imageeng = QtGui.QLabel(self)
        self.tc_imageeng.setText("Integrated Spectrum")
        vbox5.addWidget(self.tc_imageeng)
        

        hbox51 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.specfig = Figure((PlotW*1.3, PlotH*1.3))
        self.SpecPanel = FigureCanvas(self.specfig)
        self.SpecPanel.setParent(self)
        self.mpl_toolbar = NavigationToolbar(self.SpecPanel, self)

        
        fbox.addWidget(self.SpecPanel)
        frame.setLayout(fbox)
        hbox51.addWidget(frame)
        

#         self.slider_eng = QtGui.QScrollBar(QtCore.Qt.Vertical)
#         self.slider_eng.setFocusPolicy(QtCore.Qt.StrongFocus)
#         self.slider_eng.valueChanged[int].connect(self.OnScrollEng)
#         self.slider_eng.setRange(0, 100)
#         
#         hbox51.addWidget(self.slider_eng)        

        vbox5.addStretch(1)
        vbox5.addLayout(hbox51)
        vbox5.addWidget(self.mpl_toolbar)
        vbox5.addStretch(1)
        
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer1)
#         vboxt1.addStretch (1)
#         vboxt1.addWidget(sizer2)
        
        vboxt1.addStretch (1)
        #vboxt1.addWidget(sizer2)
        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vbox5)
        hboxtop.addStretch (0.5)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.5)
        vboxtop.addWidget(sizer3)
        vboxtop.addStretch (0.5)
        #vboxtop.addWidget(sizer4)
        #vboxtop.addStretch (0.5)

        vboxtop.setContentsMargins(50,20,50,20)
        self.setLayout(vboxtop)
        
        self.UpdateDisplay()
               
               
#----------------------------------------------------------------------        
    def OnCalcFits(self):
        

        if self.datastack.stack_loaded == 0:
            return
        
        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  
        

        #Calculate fits 
        calcfits = fitting_xrf.Cfits()


        print 'Start fitting'
        
        spectra = self.datastack.fullSpectrum.copy()
        
        fitmaps, parameternames, fitted_temp, Ka_temp, Kb_temp, l_temp, bkground_temp, info_elements = calcfits.calc_fits(spectra, 
                                                                       self.datastack.elementstofit, 
                                                                       self.datastack.engcal_slope/1000, 
                                                                       self.datastack.engcal_offset/1000,
                                                                       self.datastack.inenergy/1000.)
        
        
        
            
        #Show fitted spectrum
        spectra = [fitted_temp, Ka_temp, Kb_temp, l_temp, bkground_temp]
        self.PlotFitSpec(info_elements, spectra)      
        
        self.spectra = np.array(spectra)
        self.spectra = self.spectra.T
        self.spectra_names = ['fitted', 'K alpha', 'K beta', 'L lines','background']
        
         
        dims = self.datastack.fullSpectrum.shape
        if self.datastack.fullSpectrum.ndim > 3:
            ndetectors = dims[2]
        else: ndetectors = 1

        dims2 = fitmaps[0].shape

        self.datastack.fitmap_names = []
        mapindices = []
        for i in range(len(parameternames)):
            if (parameternames[i] in self.datastack.elementstofit) or (parameternames[i]+'_K' in self.datastack.elementstofit):
                self.datastack.fitmap_names.append(parameternames[i])
                mapindices.append(i)
                
        nelementmaps = len(mapindices)
        
        #Nmaps,nx,ny,ndetectors
        self.datastack.fit_maps = np.zeros((nelementmaps,dims[0],dims[1],dims[2]), dtype=np.float64)

        
        for i in range(ndetectors):
            for j in range(nelementmaps):
                ind = mapindices[j]
                self.datastack.fit_maps[j,:,:,i] = fitmaps[i][:,:,ind]
                
        sortmaps = 0
        if sortmaps:                
            #Sort the maps to show the highest count map fits
            nmaps = len(self.datastack.fitmap_names)
            counts = np.zeros((nmaps), dtype=np.int64)
            for i in range(nmaps):
                counts[i] = np.sum(self.datastack.fit_maps[i,:,:,:])
    
    
            sort_indices = np.argsort(counts)
            self.datastack.fit_maps = self.datastack.fit_maps[sort_indices[::-1],:,:,:]
            self.datastack.fitmap_names = np.array(self.datastack.fitmap_names)
            self.datastack.fitmap_names = self.datastack.fitmap_names[sort_indices[::-1]]
            self.datastack.fitmap_names = self.datastack.fitmap_names.tolist()
       

        self.datastack.fits_calculated = 1      
        self.datastack.ROI_maps_loaded = 1       
        self.window().page1.showmaps = 2
        self.window().page1.rb_fit.setEnabled(True)
        self.window().page1.rb_fit.setChecked(True)
        self.button_save.setEnabled(True)
        self.button_savesp.setEnabled(True)
        
        self.window().page1.UpdateDisplay()
        
        self.window().tabs.setCurrentIndex(4)
        
        QtGui.QApplication.restoreOverrideCursor()
        
        
        
#----------------------------------------------------------------------        
    def OnCalcFits4D(self):
        

        if self.datastack.stack_loaded == 0:
            return
        
        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  
        

        #Calculate fits 
        calcfits = fitting_xrf.Cfits()


        print 'Start fitting'
        
        self.datastack.fit_maps4D = []
        self.spectra4D = []
        
        tempfitmaps = []

        
        for i in range(len(self.datastack.theta)):
          
            data = self.datastack.fullSpectrum4D[:,:,:,:,i].copy()
        
            fitmaps, parameternames, fitted_temp, Ka_temp, Kb_temp, l_temp, bkground_temp, info_elements = calcfits.calc_fits(data, 
                                                                       self.datastack.elementstofit, 
                                                                       self.datastack.engcal_slope/1000, 
                                                                       self.datastack.engcal_offset/1000,
                                                                       self.datastack.inenergy/1000.)
        
            tempfitmaps.append(fitmaps)
            self.spectra4D.append([fitted_temp, Ka_temp, Kb_temp, l_temp, bkground_temp])
        
            
        #Show fitted spectrum
        spectra = self.spectra4D[0]
        self.PlotFitSpec(info_elements, spectra)      
        
        self.spectra = np.array(spectra)
        self.spectra = self.spectra.T
        self.spectra_names = ['fitted', 'K alpha', 'K beta', 'L lines','background']
        
         
        dims = self.datastack.fullSpectrum.shape
        if self.datastack.fullSpectrum.ndim > 3:
            ndetectors = dims[2]
        else: ndetectors = 1

        dims2 = fitmaps[0].shape

        self.datastack.fitmap_names = []
        mapindices = []
        for i in range(len(parameternames)):
            if (parameternames[i] in self.datastack.elementstofit) or (parameternames[i]+'_K' in self.datastack.elementstofit):
                self.datastack.fitmap_names.append(parameternames[i])
                mapindices.append(i)
                
        nelementmaps = len(mapindices)
        
        
        for k in range(len(self.datastack.theta)):
            #Nmaps,nx,ny,ndetectors
            thisfm = np.zeros((nelementmaps,dims[0],dims[1],dims[2]), dtype=np.float64)
    
            fitmaps = tempfitmaps[k]
            
            for i in range(ndetectors):
                for j in range(nelementmaps):
                    ind = mapindices[j]
                    thisfm[j,:,:,i] = fitmaps[i][:,:,ind]
                    
            self.datastack.fit_maps4D.append(thisfm)
                
        self.datastack.fit_maps = self.datastack.fit_maps4D[0]
        
        
        sortmaps = 0
        if sortmaps:                
            #Sort the maps to show the highest count map fits
            nmaps = len(self.datastack.fitmap_names)
            counts = np.zeros((nmaps), dtype=np.int64)
            for i in range(nmaps):
                counts[i] = np.sum(self.datastack.fit_maps[i,:,:,:])
    
    
            sort_indices = np.argsort(counts)
            self.datastack.fit_maps = self.datastack.fit_maps[sort_indices[::-1],:,:,:]
            self.datastack.fitmap_names = np.array(self.datastack.fitmap_names)
            self.datastack.fitmap_names = self.datastack.fitmap_names[sort_indices[::-1]]
            self.datastack.fitmap_names = self.datastack.fitmap_names.tolist()
       

        self.datastack.fits_calculated = 1   
        self.datastack.fits4D_calculated = 1   
        self.datastack.ROI_maps_loaded = 1       
        self.window().page1.showmaps = 2
        self.window().page1.rb_fit.setEnabled(True)
        self.window().page1.rb_fit.setChecked(True)
        self.button_save.setEnabled(True)
        self.button_savesp.setEnabled(True)
        
        self.window().page1.UpdateDisplay()
        
        if havexrftomo:
            self.window().page6.UpdateDisplay()
        
        self.window().tabs.setCurrentIndex(4)
        
        QtGui.QApplication.restoreOverrideCursor()
        
        
        
#----------------------------------------------------------------------        
    def OnCalcIntSpecFit(self):
        
        if self.datastack.stack_loaded == 0:
            return
        
        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  
        
        #Integrated spectrum
        intspec = np.sum(self.datastack.stack, axis=(0,1))
        
        n_channels = intspec.shape[0]
        max_spectra = 4096
        max_ICs = 6
        
        
        calcfits = fitting_xrf.Cfits()
        
        spectra = fitting_xrf.define_spectra(n_channels, max_spectra, max_ICs, mode = 'plot_spec')

        spectra[0].name = 'Integrated spectrum'
        spectra[0].used_chan = n_channels
        spectra[0].used = 1
        spectra[0].data[0:spectra[0].used_chan] = intspec[:]
        spectra[0].calib['off'] = self.datastack.engcal_offset/1000.
        spectra[0].calib['lin'] = self.datastack.engcal_slope/1000.
        spectra[0].calib['quad'] = 0.0
        spectra[0].IC[0]['cts'] = 1.
            
        me = fitting_xrf.Celements()
        info_elements = me.get_element_info()
        
        # now start the fitting of the integrated spectra 
        fp = fit_parameters.Cfit_parameters()
        fitp = fp.define_fitp(info_elements)
          
        fitp.g.no_iters = 4    
        
        avg_fitp = fp.define_fitp(info_elements)
        
        first_run = 0
        
        if (first_run == 1) : 
            fitp, avg_fitp, spectra, add_plot_spectra, add_plot_names, fitinfo = calcfits.do_fits(fitp, spectra, 
                                                                                         self.datastack.elementstofit, 
                                                                                         self.datastack.inenergy/1000.,
                                                                                         self.datastack.engcal_slope/1000, 
                                                                                         self.datastack.engcal_offset/1000,
                                                                                         maxiter = 10, per_pix = 1, generate_img = 1,  info_elements = info_elements)  # do the first fit twice, because the very first spectrum is not fitted right 
            first_run = 0
        
        fitp, avg_fitp, spectra, add_plot_spectra, add_plot_names, fitinfo = calcfits.do_fits(fitp, spectra, self.datastack.elementstofit, 
                                                                                     self.datastack.inenergy/1000.,
                                                                                     self.datastack.engcal_slope/1000, 
                                                                                     self.datastack.engcal_offset/1000,
                                                                                     maxiter = 500, per_pix = 1, generate_img = 1, info_elements = info_elements) 
    

        
        fitinfo = "\n".join(fitinfo)
        
        self.PlotFitIntSpec(info_elements, spectra = add_plot_spectra, add_plot_names = add_plot_names) 
        
        self.spectra = add_plot_spectra
        self.spectra_names = add_plot_names
        
        rewin = FitParams(self, fitinfo)
        rewin.show()
        self.button_save.setEnabled(True)
        self.button_savesp.setEnabled(True)
        
        QtGui.QApplication.restoreOverrideCursor()
         
        return
        
        
#----------------------------------------------------------------------        
    def OnIncEng(self): 
         
        try:       
            self.datastack.inenergy = float(self.ntc_inceng.text())
            
            self.window().page2.ShowImage()
            self.window().page4.UpdateDisplay()
            
        except:
            pass
        
# #----------------------------------------------------------------------        
#     def OnEnergyCal(self): 
#          
#         try:       
#             self.datastack.engcal_slope = float(self.ntc_ecals.text())
#             self.datastack.engcal_offset = float(self.ntc_ecalo.text())
#             
#             self.window().page2.ShowImage()
#             self.window().page2.ntc_ecals.setText(str(self.datastack.engcal_slope))
#             self.window().page2.ntc_ecalo.setText(str(self.datastack.engcal_offset))
#             self.window().page4.UpdateDisplay()
#             
#         except:
#             pass
#         
#----------------------------------------------------------------------        
    def OnSaveFitPlot(self):

        #Save images      
        wildcard = "Portable Network Graphics (*.png);;Adobe PDF Files (*.pdf);; SVG (*.svg)"

        fileName = QtGui.QFileDialog.getSaveFileName(self, 'Save Plot', '', wildcard)

        fileName = str(fileName)
        if fileName == '':
            return
        
        path, ext = os.path.splitext(fileName) 
        ext = ext[1:].lower() 
        
       
        if ext != 'png' and ext != 'pdf' and ext != 'svg': 
            error_message = ( 
                  'Only the PNG, PDF and SVG image formats are supported.\n' 
                 'A file extension of `png\' or `pdf\' must be used.') 

            QtGui.QMessageBox.warning(self, 'Error', 'Could not save file: %s' % error_message)
            return 
   
   
                          
        try: 

            matplotlib.rcParams['pdf.fonttype'] = 42
            
            fig = self.specfig
            fig.savefig(fileName)

            
        except IOError, e:
            if e.strerror:
                err = e.strerror 
            else: 
                err = e 
   
            
            QtGui.QMessageBox.warning(self, 'Error', 'Could not save file: %s' % err)

#----------------------------------------------------------------------        
    def OnSaveFitSpectra(self):

        try: 
            wildcard = "CSV files (*.csv)"
 
            filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save spectra as .csv (.csv)', '', wildcard)
 
            filepath = str(filepath)
            if filepath == '':
                return
         
                             
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))                   
             
            spectrum = np.sum(self.datastack.stack, axis=(0,1))
            xaxis = (np.arange((spectrum.size))*float(self.datastack.engcal_slope) + float(self.datastack.engcal_offset))/1000. 
             
            f = open(filepath, 'w')
            
            tempstr = 'energy, integrated'
            for i in range(len(self.spectra_names)):
                tempstr = tempstr + ', '+self.spectra_names[i]
                
            print>>f,tempstr
            
                
            print tempstr
                       
            for i in range(spectrum.size):
                tempstr =  '%.6f, %.6f' %(xaxis[i], spectrum[i])
                for j in range(len(self.spectra_names)):
                    tempstr = tempstr + ', %.6f' %(self.spectra[i,j])
                print>>f, tempstr

            f.close()            
              
            QtGui.QApplication.restoreOverrideCursor()    
  
        except:
   
            QtGui.QApplication.restoreOverrideCursor()
            QtGui.QMessageBox.warning(self, 'Error', "Could not save .csv file.")
            
#----------------------------------------------------------------------        
    def UpdateDisplay(self):            
            

        elementsstr = ''
        for item in self.datastack.elementstofit:
            elementsstr += item.replace('_K','') + ',  '
            
            
        elementsstr = elementsstr[0:-3]
        self.tc_elements.setText('Elements: ' + elementsstr)
        
        #self.tc_inceng.setText('Incident Beam Energy [eV]: '+str(self.datastack.inenergy)+' eV') 
        self.ntc_inceng.setValue(self.datastack.inenergy)
        

        if havexrftomo:
            if self.datastack.tomodata == 1:
                self.button_calc_4D.setEnabled(True)
            else:
                self.button_calc_4D.setEnabled(False)        
        
        

#----------------------------------------------------------------------   
    def PlotFitSpec(self, info_elements, spectra = [], add_plot_names = 0, ps = 0, fitp = 0, perpix = 0, save_csv = 0):
      
        fig = self.specfig
        fig.clf()
        fig.add_axes((0.15,0.15,0.75,0.75))
        axes = fig.gca()
            
#         from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
#         mplot.rcParams['pdf.fonttype'] = 42
#         
#         fontsize = 9
#         mplot.rcParams['font.size'] = fontsize
        
        colortable = []
    
        colortable.append((0., 0., 0.)) # ; black
        colortable.append((1., 0., 0.)) # ; red
        colortable.append((0., 1., 0.)) # ; green
        colortable.append((0., 0., 1.)) # ; blue
        colortable.append((0., 1., 1.)) # ; turquois
        colortable.append((1., 0., 1.)) # ; magenta
        colortable.append((1., 1., 0.)) # ; yellow
        colortable.append((0.7, 0.7, 0.7)) # ; light grey
        colortable.append((1., 0.8, 0.75)) # ; flesh
        colortable.append(( 0.35,  0.35,  0.35)) # ; dark grey       
        colortable.append((0., 0.5, 0.5)) # ; sea green
        colortable.append((1., 0., 0.53)) # ; pink-red
        colortable.append((0.5, 0., 1.)) # ; violet        
        colortable.append((0., 1., 0.68)) # ; bluegreen 
        colortable.append((1., 0.5, 0.)) # ; orange
        colortable.append((0., 0.68, 1.)) # ; another blue
        colortable.append((1., 1., 1.)) # ; white
        
        foreground_color = colortable[0]
        background_color = colortable[-1]
                
        droplist_spectrum = 0
        droplist_scale = 0
        png = 0
        if ps == 0: png = 2
        if spectra == [] : return 

        have_name = 1
        names = []

        filename = os.path.splitext(os.path.basename(self.datastack.filename))[0]
        names.append('fit_'+filename)
        names.append('fitted')
        names.append('K alpha')
        names.append('background')
        names.append('tail')
        names.append('elastic')
        names.append('compton')
        names.append('pileup')
        names.append('escape')
        
        if save_csv == 1:
            csvfilename = 'csv_'+filename+'.csv'
            file_csv = os.path.join(self.main['output_dir'], csvfilename)
            

        
#         if (png > 0) or (ps > 0):
#             if (png > 0):
#                 dpi = 100
#                 canvas_xsize_in = 900./dpi
#                 canvas_ysize_in = 700./dpi
#                 fig = mplot.figure.Figure(figsize=(canvas_xsize_in, canvas_ysize_in), dpi=dpi, facecolor=background_color, edgecolor=None)
#                 canvas = FigureCanvas(fig)
#                 fig.add_axes()
#                 axes = fig.gca()
#                 for child in axes.get_children():
#                     if isinstance(child, mplot.spines.Spine):
#                         child.set_color(foreground_color)
#                 axes.set_axis_bgcolor(background_color)
#                 ya = axes.yaxis                  
#                 xa = axes.xaxis                          
#                 ya.set_tick_params(labelcolor=foreground_color) 
#                 ya.set_tick_params(color=foreground_color) 
#                 xa.set_tick_params(labelcolor=foreground_color) 
#                 xa.set_tick_params(color=foreground_color) 
#                                         
#                
#             if (ps > 0): 
#                 ps_filename = 'ps_'+filename+'.pdf'
#                 if ps_filename == '' : return 
#                 eps_plot_xsize = 8.
#                 eps_plot_ysize = 6.
#                                     
#                 fig = mplot.figure.Figure(figsize =(eps_plot_xsize, eps_plot_ysize))
#                 canvas = FigureCanvas(fig)
#                 fig.add_axes()
#                 axes = fig.gca()
#                     
#                 file_ps = os.path.join(self.main['output_dir'], ps_filename)

        
        spectrum = np.sum(self.datastack.stack, axis=(0,1))
        nchannels = spectrum.size    

        xaxis = (np.arange((nchannels))*float(self.datastack.engcal_slope) + float(self.datastack.engcal_offset))/1000.   
        xtitle = 'energy [keV]'
          

        
        minengtofit = 1.65
        xmin = minengtofit*0.5
        maxval = int(np.argmax(spectrum) + (np.argmax(spectrum)-minengtofit)*0.50)
        xmax = xaxis[maxval]


        wo_a = np.where(xaxis > xmax)[0]
        if len(wo_a) > 0 :
            wo_xmax = np.amin(wo_a) 
        else:
            wo_xmax = spectrum.size*8./10.
        wo_b = np.where(xaxis < xmin)[0]
        if len(wo_b) >0 :
            wo_xmin = np.amax(wo_b) 
        else:
            wo_xmin = 0


        wo = np.where(spectrum[wo_xmin:wo_xmax+1] > 0.)[0]
        if len(wo) > 0 : 
            ymin = np.amin(spectrum[wo+wo_xmin])*0.9 
        else:
            ymin = 0.1

        if len(wo) > 0 : 
            ymax = np.amax(spectrum[wo+wo_xmin]*1.1) 
        else: 
            ymax = np.amax(spectrum)
        # make sure ymax is larger than ymin, so as to avoid a crash during plotting
        if ymax <= ymin : ymax = ymin+0.001

        yanno = (1.01+0.04*(1-droplist_scale)) * ymax
        yanno_beta = (1.07+0.53*(1-droplist_scale)) * ymax
        if droplist_scale == 0 :
            yanno_below = 0.8*ymin 
        else:
            yanno_below = ymin -(ymax-ymin)*.04
        yanno_lowest = (0.8+0.15*(1-(1-droplist_scale)))*ymin
        this_spec = spectrum
        wo = np.where(this_spec <= 0)[0]
        if len(wo) > 0:
            this_spec[wo] = ymin
        norm_font_y_size = 10.
        
        plot1 = axes.semilogy(xaxis, this_spec, color = foreground_color, linewidth=1.0)    
        axes.set_xlabel(xtitle, color=foreground_color)
        axes.set_ylabel('counts', color=foreground_color)
        
        xmin = 0
        xmax = (self.datastack.inenergy+1000)/1000.0
        
        axes.set_xlim((xmin, xmax))
        axes.set_ylim((ymin, ymax))
        
        axes.set_position([0.10,0.18,0.85,0.70])
        
        
        axes.text( -0.10, 1.12, names[0], color = foreground_color, transform = axes.transAxes)
                  
        if spectra != []: 
            
            
            plot3 = axes.semilogy(xaxis, spectra[1], color = colortable[2], linewidth=1.0) 
            axes.text( -0.10+0.2*1, -0.18, 'K alpha',color = colortable[2], transform = axes.transAxes)
            
            plot3 = axes.semilogy(xaxis, spectra[2], color = colortable[4], linewidth=1.0) 
            axes.text( -0.10+0.2*2, -0.18, 'K beta',color = colortable[4], transform = axes.transAxes)
            
            plot3 = axes.semilogy(xaxis, spectra[4], color = colortable[3], linewidth=1.0) 
            axes.text( -0.10+0.2*3, -0.18, 'background',color = colortable[3], transform = axes.transAxes)
            
            plot2 = axes.semilogy(xaxis, spectra[0], color = colortable[1], linewidth=1.0) 
            axes.text( -0.10+0.2*0, -0.18, 'fitted',color = colortable[1], transform = axes.transAxes)
            
                        
#             for k in range(1,len(spectra)):
#                     plot2 = axes.semilogy(xaxis, spectra[k], color = colortable[1+k], linewidth=1.0) 
#  
#                     if k <= 2 :
#                         axes.text( -0.10+0.4+0.2*k, -0.12, names[k],color = colortable[1+k], transform = axes.transAxes)
#                     if (k >= 3) and (k <= 6) :
#                         axes.text( -0.10+0.2*(k-3), -0.15, names[k],color = colortable[1+k], transform = axes.transAxes)  
#                     if (k >= 7) :
#                         axes.text( -0.10+0.2*(k-7), -0.18, names[k],color = colortable[1+k], transform = axes.transAxes)
#  
#  
#           
#             # plot background next to last
#             plot3 = axes.semilogy(xaxis, spectra[2], color = colortable[1+2], linewidth=1.0)
#             # plot fit last
#             plot4 = axes.semilogy(xaxis, spectra[0], color = colortable[1+0], linewidth=1.0)
            

           

        # plot xrf ticks   
        element_list = np.array([11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 25, 26, 27, 28, 29, 30, 32, 33, 35])-1
        x_positions = []
        for i in range(len(info_elements)): x_positions.append(info_elements[i].xrf['ka1'])
        color = 2


        local_ymax = np.array([1.03, 1.15, 1.3])*ymax 
        local_ymin = ymax*0.9
        for k in range(len(element_list)): 
            i = element_list[k]
            line=matplotlib.lines.Line2D([x_positions[i],x_positions[i]], [local_ymin,local_ymax[(i-int(i/3)*3)]] ,color=colortable[color])
            line.set_clip_on(False)
            axes.add_line(line)                
            axes.text( x_positions[i], local_ymax[(i-int(i/3)*3)] , info_elements[i].name, ha='center', va='bottom', color = colortable[color])
  
     
       
#             if (png > 0) or (ps > 0) :
# 
#                 if (png>0): 
#                     axes.text(0.97, -0.23, 'mapspy', color = foreground_color, transform = axes.transAxes)
#                     if (png == 1) or (png == 2) :  
#                         image_filename = filename+'.png'
#                         print 'saving ', os.path.join(self.main['output_dir'],image_filename)
#                         fig.savefig(os.path.join(self.main['output_dir'], image_filename), dpi=dpi, facecolor=background_color, edgecolor=None)
#                        
#                     if (ps > 0):
#                         fig.savefig(file_ps)
#                         
#                         
#             if save_csv == 1:
#                 
#                 
#                 if add_plot_spectra.any(): 
#                     size = add_plot_spectra.shape
#                     if len(size) == 2 : 
#                         spectra_names = ['Energy','Spectrum']
#                         for i in range(len(add_plot_names)):
#                             spectra_names.append(add_plot_names[i])
#                         dims = add_plot_spectra.shape
#                         allspectra = np.zeros((dims[0],dims[1]+2))
#                         allspectra[:,2:] = add_plot_spectra
# 
#                         allspectra[:,0] = xaxis
#                         allspectra[:,1] = this_spec
#                             
#                         writer = csv.writer(open(file_csv, "wb"))
#                         
#                         writer.writerow(spectra_names)
#                         writer.writerows(allspectra)
   
   
        self.SpecPanel.draw()
                
                
        return
        
        
#----------------------------------------------------------------------   
    def PlotFitIntSpec(self, info_elements, spectra = [], add_plot_names = 0, ps = 0, fitp = 0, perpix = 0, save_csv = 0):
      
        fig = self.specfig
        fig.clf()
        fig.add_axes((0.15,0.15,0.75,0.75))
        axes = fig.gca()
            
#         from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
#         mplot.rcParams['pdf.fonttype'] = 42
#         
#         fontsize = 9
#         mplot.rcParams['font.size'] = fontsize
        
        colortable = []
    
        colortable.append((0., 0., 0.)) # ; black
        colortable.append((1., 0., 0.)) # ; red
        colortable.append((0., 1., 0.)) # ; green
        colortable.append((0., 0., 1.)) # ; blue
        colortable.append((0., 1., 1.)) # ; turquois
        colortable.append((1., 0., 1.)) # ; magenta
        colortable.append((1., 1., 0.)) # ; yellow
        colortable.append((0.7, 0.7, 0.7)) # ; light grey
        colortable.append((1., 0.8, 0.75)) # ; flesh
        colortable.append(( 0.35,  0.35,  0.35)) # ; dark grey       
        colortable.append((0., 0.5, 0.5)) # ; sea green
        colortable.append((1., 0., 0.53)) # ; pink-red
        colortable.append((0.5, 0., 1.)) # ; violet
        colortable.append((0., 1., 0.68)) # ; bluegreen 
        colortable.append((1., 0.5, 0.)) # ; orange
        colortable.append((0., 0.68, 1.)) # ; another blue
        colortable.append((1., 1., 1.)) # ; white
        
        foreground_color = colortable[0]
        background_color = colortable[-1]
                
        droplist_spectrum = 0
        droplist_scale = 0
        png = 0
        if ps == 0: png = 2
        if spectra == [] : return 

        have_name = 1
        names = []

        filename = os.path.splitext(os.path.basename(self.datastack.filename))[0]
        names.append('fit_'+filename)
        names.append('fitted')
        names.append('K alpha')
        names.append('background')
        names.append('tail')
        names.append('elastic')
        names.append('compton')
        names.append('pileup')
        names.append('escape')
        
        if save_csv == 1:
            csvfilename = 'csv_'+filename+'.csv'
            file_csv = os.path.join(self.main['output_dir'], csvfilename)
            

        
#         if (png > 0) or (ps > 0):
#             if (png > 0):
#                 dpi = 100
#                 canvas_xsize_in = 900./dpi
#                 canvas_ysize_in = 700./dpi
#                 fig = mplot.figure.Figure(figsize=(canvas_xsize_in, canvas_ysize_in), dpi=dpi, facecolor=background_color, edgecolor=None)
#                 canvas = FigureCanvas(fig)
#                 fig.add_axes()
#                 axes = fig.gca()
#                 for child in axes.get_children():
#                     if isinstance(child, mplot.spines.Spine):
#                         child.set_color(foreground_color)
#                 axes.set_axis_bgcolor(background_color)
#                 ya = axes.yaxis                  
#                 xa = axes.xaxis                          
#                 ya.set_tick_params(labelcolor=foreground_color) 
#                 ya.set_tick_params(color=foreground_color) 
#                 xa.set_tick_params(labelcolor=foreground_color) 
#                 xa.set_tick_params(color=foreground_color) 
#                                         
#                
#             if (ps > 0): 
#                 ps_filename = 'ps_'+filename+'.pdf'
#                 if ps_filename == '' : return 
#                 eps_plot_xsize = 8.
#                 eps_plot_ysize = 6.
#                                     
#                 fig = mplot.figure.Figure(figsize =(eps_plot_xsize, eps_plot_ysize))
#                 canvas = FigureCanvas(fig)
#                 fig.add_axes()
#                 axes = fig.gca()
#                     
#                 file_ps = os.path.join(self.main['output_dir'], ps_filename)

        spectrum = np.sum(self.datastack.stack, axis=(0,1))

        nchannels = spectrum.shape[0]
    

        xaxis = (np.arange((nchannels))*float(self.datastack.engcal_slope) + float(self.datastack.engcal_offset))/1000.   
        xtitle = 'energy [keV]'
          
        
        
        minengtofit = 1.65
        xmin = minengtofit*0.5
        maxval = int(np.argmax(spectrum) + (np.argmax(spectrum)-minengtofit)*0.50)
        xmax = xaxis[maxval]



        wo_a = np.where(xaxis > xmax)[0]
        if len(wo_a) > 0 :
            wo_xmax = np.amin(wo_a) 
        else:
            wo_xmax = spectrum.size*8./10.
        wo_b = np.where(xaxis < xmin)[0]
        if len(wo_b) >0 :
            wo_xmin = np.amax(wo_b) 
        else:
            wo_xmin = 0


        wo = np.where(spectrum[wo_xmin:wo_xmax+1] > 0.)[0]
        if len(wo) > 0 : 
            ymin = np.amin(spectrum[wo+wo_xmin])*0.9 
        else:
            ymin = 0.1

        if len(wo) > 0 : 
            ymax = np.amax(spectrum[wo+wo_xmin]*1.3) 
        else: 
            ymax = np.amax(spectrum)*1.3
        # make sure ymax is larger than ymin, so as to avoid a crash during plotting
        if ymax <= ymin : ymax = ymin+0.001

        yanno = (1.01+0.04*(1-droplist_scale)) * ymax
        yanno_beta = (1.07+0.53*(1-droplist_scale)) * ymax
        if droplist_scale == 0 :
            yanno_below = 0.8*ymin 
        else:
            yanno_below = ymin -(ymax-ymin)*.04
        yanno_lowest = (0.8+0.15*(1-(1-droplist_scale)))*ymin
        this_spec = spectrum
        wo = np.where(this_spec <= 0)[0]
        if len(wo) > 0:
            this_spec[wo] = ymin
        norm_font_y_size = 10.
        
        plot1 = axes.semilogy(xaxis, this_spec, color = foreground_color, linewidth=1.0)    
        #plot1 = axes.plot(xaxis, this_spec, color = foreground_color, linewidth=1.0)  
        
        axes.set_xlabel(xtitle, color=foreground_color)
        axes.set_ylabel('counts', color=foreground_color)
        
        xmin = 0
        xmax = (self.datastack.inenergy+1000)/1000.0
                
        axes.set_xlim((xmin, xmax))
        axes.set_ylim((ymin, ymax))
        
        axes.set_position([0.10,0.18,0.85,0.70])
        
        
        axes.text( -0.10, 1.12, names[0], color = foreground_color, transform = axes.transAxes)
        
        
        if spectra.any(): 
            size = spectra.shape
            if len(size) == 2 : 
                #for k = size[2]-1, 0, -1 :
                for k in np.arange(size[1]-1, -1, -1):
                    plot2 = axes.semilogy(xaxis, spectra[:, k], color = colortable[1+k], linewidth=1.0) 
                    #plot2 = axes.plot(xaxis, spectra[:, k], color = colortable[1+k], linewidth=1.0) 

                    if k <= 2 :
                        axes.text( -0.10+0.4+0.2*k, -0.15, add_plot_names[k],color = colortable[1+k], transform = axes.transAxes)
                    if (k >= 3) and (k <= 6) :
                        axes.text( -0.10+0.2*(k-3), -0.20, add_plot_names[k],color = colortable[1+k], transform = axes.transAxes)  
                    if (k >= 7) :
                        axes.text( -0.10+0.2*(k-7), -0.25, add_plot_names[k],color = colortable[1+k], transform = axes.transAxes)

         
                # plot background next to last
                plot3 = axes.semilogy(xaxis, spectra[:, 2], color = colortable[1+2], linewidth=1.0)
                #plot3 = axes.plot(xaxis, spectra[:, 2], color = colortable[1+2], linewidth=1.0)
                # plot fit last
                plot4 = axes.semilogy(xaxis, spectra[:, 0], color = colortable[1+0], linewidth=1.0)
                #plot4 = axes.plot(xaxis, spectra[:, 0], color = colortable[1+0], linewidth=1.0)
                  
#         if spectra != []: 
#             
#             plot3 = axes.semilogy(xaxis, spectra[1], color = colortable[2], linewidth=1.0) 
#             axes.text( -0.10+0.2*1, -0.18, 'K alpha',color = colortable[2], transform = axes.transAxes)
#             
#             plot3 = axes.semilogy(xaxis, spectra[3], color = colortable[3], linewidth=1.0) 
#             axes.text( -0.10+0.2*2, -0.18, 'background',color = colortable[3], transform = axes.transAxes)
#             
#             plot2 = axes.semilogy(xaxis, spectra[0], color = colortable[1], linewidth=1.0) 
#             axes.text( -0.10+0.2*0, -0.18, 'fitted',color = colortable[1], transform = axes.transAxes)
            
                        
#             for k in range(1,len(spectra)):
#                     plot2 = axes.semilogy(xaxis, spectra[k], color = colortable[1+k], linewidth=1.0) 
#  
#                     if k <= 2 :
#                         axes.text( -0.10+0.4+0.2*k, -0.12, names[k],color = colortable[1+k], transform = axes.transAxes)
#                     if (k >= 3) and (k <= 6) :
#                         axes.text( -0.10+0.2*(k-3), -0.15, names[k],color = colortable[1+k], transform = axes.transAxes)  
#                     if (k >= 7) :
#                         axes.text( -0.10+0.2*(k-7), -0.18, names[k],color = colortable[1+k], transform = axes.transAxes)
#  
#  
#           
#             # plot background next to last
#             plot3 = axes.semilogy(xaxis, spectra[2], color = colortable[1+2], linewidth=1.0)
#             # plot fit last
#             plot4 = axes.semilogy(xaxis, spectra[0], color = colortable[1+0], linewidth=1.0)
            

           

        # plot xrf ticks   
        element_list = np.array([11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 25, 26, 27, 28, 29, 30, 32, 33, 35])-1
        x_positions = []
        for i in range(len(info_elements)): x_positions.append(info_elements[i].xrf['ka1'])
        color = 2


        local_ymax = np.array([1.03, 1.15, 1.3])*ymax 
        local_ymin = ymax*0.9
        for k in range(len(element_list)): 
            i = element_list[k]
            line=matplotlib.lines.Line2D([x_positions[i],x_positions[i]], [local_ymin,local_ymax[(i-int(i/3)*3)]] ,color=colortable[color])
            line.set_clip_on(False)
            axes.add_line(line)                
            axes.text( x_positions[i], local_ymax[(i-int(i/3)*3)] , info_elements[i].name, ha='center', va='bottom', color = colortable[color])
  
     
       
#             if (png > 0) or (ps > 0) :
# 
#                 if (png>0): 
#                     axes.text(0.97, -0.23, 'mapspy', color = foreground_color, transform = axes.transAxes)
#                     if (png == 1) or (png == 2) :  
#                         image_filename = filename+'.png'
#                         print 'saving ', os.path.join(self.main['output_dir'],image_filename)
#                         fig.savefig(os.path.join(self.main['output_dir'], image_filename), dpi=dpi, facecolor=background_color, edgecolor=None)
#                        
#                     if (ps > 0):
#                         fig.savefig(file_ps)
#                         
#                         
#             if save_csv == 1:
#                 
#                 
#                 if add_plot_spectra.any(): 
#                     size = add_plot_spectra.shape
#                     if len(size) == 2 : 
#                         spectra_names = ['Energy','Spectrum']
#                         for i in range(len(add_plot_names)):
#                             spectra_names.append(add_plot_names[i])
#                         dims = add_plot_spectra.shape
#                         allspectra = np.zeros((dims[0],dims[1]+2))
#                         allspectra[:,2:] = add_plot_spectra
# 
#                         allspectra[:,0] = xaxis
#                         allspectra[:,1] = this_spec
#                             
#                         writer = csv.writer(open(file_csv, "wb"))
#                         
#                         writer.writerow(spectra_names)
#                         writer.writerows(allspectra)
   
   
        self.SpecPanel.draw()
                
                
        return
        

# """ ------------------------------------------------------------------------------------------------"""
# class PageMapsPy(QtGui.QWidget):
#     def __init__(self, datastack):
#         super(PageMapsPy, self).__init__()
# 
#         self.initUI(datastack)
#         
# #----------------------------------------------------------------------          
#     def initUI(self, datastack): 
#         
#         self.datastack = datastack
#         
#         self.Canalysis = analysis.roi_defs()
#         
#         self.Cmp = mapspy_helper.mapspy()
#         
#         self.beamlinelist = ['ANKA-NANO', 'DLS-I08','2-ID-E']
#         
#         
#         #panel 1
#         sizer1 = QtGui.QGroupBox('Pixel Spectra Fits')
#         vbox1 = QtGui.QVBoxLayout()
#         
#         self.button_outdir = QtGui.QPushButton('Select Output Directory')
#         self.button_outdir.clicked.connect(self.OnSelectOutDir)
#         self.button_outdir.setMinimumWidth(200)
#         vbox1.addWidget(self.button_outdir)
#         
#         self.button_calc = QtGui.QPushButton('Calculate Fits')
#         self.button_calc.clicked.connect( self.OnCalcFits)
#         self.button_calc.setMinimumWidth(200)
#         vbox1.addWidget(self.button_calc)
#         
#         self.button_quantfile = QtGui.QPushButton('Add Standard File')
#         self.button_quantfile.clicked.connect( self.OnAddStandard)
#         self.button_quantfile.setMinimumWidth(200)
#         vbox1.addWidget(self.button_quantfile)
#         
#         self.button_save = QtGui.QPushButton('Save Fit Results')
#         self.button_save.clicked.connect( self.OnSaveFits)   
#         self.button_save.setEnabled(False)
#         vbox1.addWidget(self.button_save)
#         
#         sizer1.setLayout(vbox1)
#         
#         
#         #panel2
#         sizer2 = QtGui.QGroupBox('Fit Parameters')
#         vbox2 = QtGui.QVBoxLayout()
#         
#         self.button_fpfile = QtGui.QPushButton('Edit Fit Parameters')
#         self.button_fpfile.clicked.connect( self.OnFitParams)
#         self.button_fpfile.setMinimumWidth(200)
#         vbox2.addWidget(self.button_fpfile)
#         
#         sizer2.setLayout(vbox2)
#         
#         
#         
#         #panel 3
#         sizer3 = QtGui.QGroupBox('Settings')
#         vbox3 = QtGui.QVBoxLayout()
#         
#         self.tc_dir = QtGui.QLabel(self)
#         vbox3.addWidget(self.tc_dir)
#         self.tc_dir.setText('Output Directory: ')
#         
#         hbox31 = QtGui.QHBoxLayout()
#         self.tc_beamline = QtGui.QLabel(self)
#         hbox31.addWidget(self.tc_beamline)
#         self.tc_beamline.setText('Beamline: ')
#         
#         self.cb_beamline = QtGui.QComboBox(self)
#         self.cb_beamline.addItems(self.beamlinelist)
#         #self.cb_nimgs.activated[int].connect(self.OnChangeNImgs) 
#         hbox31.addWidget(self.cb_beamline)
#         hbox31.addStretch(1)
#         vbox3.addLayout(hbox31)       
# 
#         
#         self.cb_usefit = QtGui.QCheckBox('Use Fit', self)
#         #self.cb_scalebar.setChecked(self.show_scale_bar)
#         #self.cb_scalebar.stateChanged.connect(self.OnCBScaleBar)
#         vbox3.addWidget(self.cb_usefit)      
#         
# #         self.tc_ecal = QtGui.QLabel(self)
# #         vbox3.addWidget(self.tc_ecal)
# #         self.tc_ecal.setText('Energy Calibration:')
# 
# 
#         hbox32 = QtGui.QHBoxLayout()
#         tc1 = QtGui.QLabel(self)
#         tc1.setText("Detector Elements\t")  
#         hbox32.addWidget(tc1)      
#         self.ntc_detelements = QtGui.QLineEdit(self)
#         self.ntc_detelements.setFixedWidth(65)
#         self.ntc_detelements.setValidator(QtGui.QIntValidator(1, 4, self))
#         self.ntc_detelements.setAlignment(QtCore.Qt.AlignRight)         
#         
#         self.ntc_detelements.setText(str(1))
#         hbox32.addWidget(self.ntc_detelements) 
#         hbox32.addStretch(1)
#         vbox3.addLayout(hbox32) 
#         
#         
#         hbox33 = QtGui.QHBoxLayout()
#         tc1 = QtGui.QLabel(self)
#         tc1.setText("Max Processors\t")  
#         hbox33.addWidget(tc1)      
#         self.ntc_maxprocs = QtGui.QLineEdit(self)
#         self.ntc_maxprocs.setFixedWidth(65)
#         self.ntc_maxprocs.setValidator(QtGui.QIntValidator(1, 99, self))
#         self.ntc_maxprocs.setAlignment(QtCore.Qt.AlignRight)         
#         
#         self.ntc_maxprocs.setText(str(2))
#         hbox33.addWidget(self.ntc_maxprocs) 
#         hbox33.addStretch(1)
#         vbox3.addLayout(hbox33) 
#         
#         
#         self.tc_stand = QtGui.QLabel(self)
#         vbox3.addWidget(self.tc_stand)
#         self.tc_stand.setText('Standards: [ ]')       
#         
#         self.tc_elements = QtGui.QLabel(self)
#         vbox3.addWidget(self.tc_elements)
#         self.tc_elements.setText('Elements: ') 
#                 
#         vbox3.setContentsMargins(20,20,20,20)
#         sizer3.setLayout(vbox3)
# 
# 
#         
#         #panel 5     
#         vbox5 = QtGui.QVBoxLayout()
#         
#         self.tc_imageeng = QtGui.QLabel(self)
#         self.tc_imageeng.setText("Integrated Spectrum")
#         vbox5.addWidget(self.tc_imageeng)
#         
# 
#         hbox51 = QtGui.QHBoxLayout()
#         
#         frame = QtGui.QFrame()
#         frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
#         fbox = QtGui.QHBoxLayout()
#    
#         self.specfig = Figure((PlotW, PlotH))
#         self.SpecPanel = FigureCanvas(self.specfig)
#         self.SpecPanel.setParent(self)
#         
#         fbox.addWidget(self.SpecPanel)
#         frame.setLayout(fbox)
#         hbox51.addWidget(frame)
#         
# 
# #         self.slider_eng = QtGui.QScrollBar(QtCore.Qt.Vertical)
# #         self.slider_eng.setFocusPolicy(QtCore.Qt.StrongFocus)
# #         self.slider_eng.valueChanged[int].connect(self.OnScrollEng)
# #         self.slider_eng.setRange(0, 100)
# #         
# #         hbox51.addWidget(self.slider_eng)        
# 
#         vbox5.addStretch(1)
#         vbox5.addLayout(hbox51)
#         vbox5.addStretch(1)
#         
#         
#        
#         vboxtop = QtGui.QVBoxLayout()
#         
#         hboxtop = QtGui.QHBoxLayout()
#         vboxt1 = QtGui.QVBoxLayout()
#         vboxt1.addStretch (1)
#         vboxt1.addWidget(sizer1)
#         vboxt1.addStretch (1)
#         vboxt1.addWidget(sizer2)
#         
#         vboxt1.addStretch (1)
#         #vboxt1.addWidget(sizer2)
#         
#         hboxtop.addStretch (0.5)
#         hboxtop.addLayout(vboxt1)
#         hboxtop.addStretch (0.5)
#         hboxtop.addLayout(vbox5)
#         hboxtop.addStretch (0.5)
#         
#         vboxtop.addStretch (0.5)
#         vboxtop.addLayout(hboxtop)
#         vboxtop.addStretch (0.5)
#         vboxtop.addWidget(sizer3)
#         vboxtop.addStretch (0.5)
#         #vboxtop.addWidget(sizer4)
#         #vboxtop.addStretch (0.5)
# 
#         vboxtop.setContentsMargins(50,20,50,20)
#         self.setLayout(vboxtop)
#         
#         self.UpdateDisplay()
#         
# #----------------------------------------------------------------------        
#     def OnCalcFits(self):
#         
#         if self.datastack.mp_outdir == '':
#             QtGui.QMessageBox.warning(self,'Warning',"Please select an output directory.")
#             return
#         
#         QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  
#         
#         self.GetSettings()
#         
#         if self.datastack.fileformat == 'nxs':
#             self.Cmp.run_maps_nxs(self.datastack.filename, self.datastack.mp_outdir, self.datastack.elementstofit)
#         elif self.datastack.fileformat == 'maps-h5':
#             self.Cmp.run_maps_h5(self.datastack.filename, self.datastack.mp_outdir, self.datastack.fullSpectrum, self.datastack.elementstofit)
#         elif self.datastack.fileformat == 'anka-h5':
#             self.Cmp.run_anka_h5(self.datastack.filename, self.datastack.mp_outdir, self.datastack.elementstofit, 
#                                  self.datastack.multiimg, self.datastack.readimg)
# 
#         #Get fit spectra from csv file
#         filename =  os.path.basename(str(self.datastack.filename))
#         
#         if self.datastack.fileformat == 'nxs':
#             csv_fn = 'csv_fit_'+filename[:-4]+'_det0.csv'
#             mapsh5filename = filename[:-4]+'.h5'
#         elif self.datastack.fileformat == 'maps-h5':
#             csv_fn = 'csv_fit_'+filename[:-3]+'_det0.csv'
#             mapsh5filename = filename
#         elif self.datastack.fileformat == 'anka-h5':
#             csv_fn = 'csv_fit_'+filename[:-3]+'_det0.csv'
#             mapsh5filename = filename
#                         
#         file_csv = os.path.join(os.path.join(str(self.datastack.mp_outdir),'output.fits'), csv_fn)
#         
#         reader = csv.reader(open(file_csv,"r"))
# 
#         temp = []
# 
#         for row in reader:
#             temp.append(row)
#             
#         self.fitspec_names = temp[0]
#         
#         nchn = len(temp) - 1
#         
#         
#         self.fitspectra = np.zeros((len(temp[0]),nchn))
#         self.nfitlines = len(temp[0])
# 
#         
#         for i in range(1,len(temp)):
#             row = temp[i]
#             for j in range(len(row)):                
#                 self.fitspectra[j,i-1] = float(row[j])
#         
#         #Show fitted spectrum
#         self.ShowSpectrum()
#         
#         
#         #Load fit maps from hdf5 file
#         mapsh5filename = os.path.join(os.path.join(str(self.datastack.mp_outdir),'img.dat'), mapsh5filename)
# 
#         fits, channelnames = self.Cmp.get_fits_h5(mapsh5filename)     
#         
#         if fits != None:
#             self.datastack.fits_calculated = 1
#             self.datastack.fit_maps = fits
#             self.datastack.fitmap_names = channelnames        
#             self.datastack.ROI_maps_loaded = 1       
#             self.window().page1.showmaps = 2
#             self.window().page1.rb_fit.setEnabled(True)
#             self.window().page1.rb_fit.setChecked(True)
#             
#                
#         
#         self.window().page1.UpdateDisplay()
#         
#         QtGui.QApplication.restoreOverrideCursor()
#         
#         
# #----------------------------------------------------------------------        
#     def OnSaveFits(self):
#         pass        
#     
# #----------------------------------------------------------------------
#     def OnSelectOutDir(self):
#         """
#         Select output directory
#         """
#         
#         try:
# 
#             directory = QtGui.QFileDialog.getExistingDirectory(self, "Choose a directory", '', QtGui.QFileDialog.ShowDirsOnly|QtGui.QFileDialog.ReadOnly )       
#                                                                 
#        
#             if directory == '':
#                 return
#                  
#             self.datastack.mp_outdir = directory
#             
#             self.tc_dir.setText('Output Directory: '+self.datastack.mp_outdir)
#  
#              
#         except:
#                                   
#             QtGui.QMessageBox.warning(self,'Error',"Error selecting a directory.")
#             #print sys.exc_info()     
#             
#         #If there is a settings file in the folder read the values
#         maps_settingsfile = 'maps_settings.txt'     
# 
#         try:
#             f = open(os.path.join(str(directory), maps_settingsfile), 'rt')
#             for line in f:
#                 if ':' in line : 
#                     slist = line.split(':')
#                     tag = slist[0]
#                     value = ''.join(slist[1:])
#                     
#                     if   tag == 'VERSION': self.Cmp.fileversion = float(value)
#                     elif tag == 'DETECTOR_ELEMENTS' : self.Cmp.total_number_detectors  =  int(value)
#                     elif tag == 'MAX_NUMBER_OF_FILES_TO_PROCESS' : self.Cmp.max_no_processors_files = int(value)
#                     elif tag == 'MAX_NUMBER_OF_LINES_TO_PROCESS' : self.Cmp.max_no_processors_lines = int(value)
#                     elif tag == 'WRITE_HDF5' : self.Cmp.write_hdf = int(value)
#                     elif tag == 'USE_FIT' : self.Cmp.use_fit = int(value)
#                     elif tag == 'QUICK_DIRTY'  :  self.Cmp.quick_dirty  = int(value)
#                     elif tag == 'XRF_BIN'  :  self.Cmp.xrf_bin  = int(value)
#                     elif tag == 'NNLS'  :  self.Cmp.nnls  = int(value)
#                     elif tag == 'XANES_SCAN'  :  self.Cmp.xanes_scan  = int(value)
#                     elif tag == 'DETECTOR_TO_START_WITH'  :  self.Cmp.detector_to_start_with  = int(value)
#                     elif tag == 'BEAMLINE'  :  self.Cmp.beamline  = str(value).strip()
#                     elif tag == 'STANDARD'  :  self.Cmp.AddStandard(str(value).strip())
#                     
#     
#             f.close()
#             
#             
#                       
#         except: pass  
#         
#         
#         self.UpdateDisplay()   
# 
# 
# #----------------------------------------------------------------------
#     def OnFitParams(self, event):  
#         
#         if self.datastack.mp_outdir == '':
#             QtGui.QMessageBox.warning(self,'Warning',"Please select an output directory.")
#             return
# 
#         fpwin = FitParameters(self.window(), self.datastack, self.Cmp)
#         fpwin.show()            
#             
# #----------------------------------------------------------------------
#     def OnAddStandard(self, event):
#         
#         #try:
#         if True:
#             
#             wildcard =  "MAPS Standard files (*.mca)" 
# 
#             filepath = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '', wildcard)
#             
# 
#             filepath = str(filepath)
#             if filepath == '':
#                 return
# 
#             filename =  os.path.basename(str(filepath))
#         
#             self.Cmp.add_standard_file(filename)
#             
#             
#         self.tc_stand.setText('Standards: '+str(self.Cmp.standards))
#             
# 
# #----------------------------------------------------------------------        
#     def UpdateDisplay(self):            
#             
#         self.tc_stand.setText('Standards: '+str(self.Cmp.standards))
#         elementsstr = ''
#         for item in self.datastack.elementstofit:
#             elementsstr += item + ',  '
#         self.tc_elements.setText('Elements: ' + elementsstr)
#         
#         
#         self.ntc_detelements.setText(str(self.Cmp.total_number_detectors))
#         self.ntc_maxprocs.setText(str(self.Cmp.max_no_processors_lines))
#             
#             
#         if self.Cmp.beamline in self.beamlinelist:
#             index = self.beamlinelist.index(self.Cmp.beamline)
#         else:  
#             index = 0
#             self.cb_beamline = self.beamlinelist[0]
#         self.cb_beamline.setCurrentIndex(index)
#         
#         
#         self.cb_usefit.setChecked(self.Cmp.use_fit)
#         
# 
# #----------------------------------------------------------------------        
#     def GetSettings(self):            
#         
#         self.Cmp.total_number_detectors = int(self.ntc_detelements.text())
#         self.Cmp.max_no_processors_lines = int(self.ntc_maxprocs.text())
#             
#         index = self.cb_beamline.currentIndex()
#         self.Cmp.beamline = self.beamlinelist[index]        
#         
#         self.Cmp.use_fit = self.cb_usefit.isChecked()        
#         
# #----------------------------------------------------------------------        
#     def ShowSpectrum(self):
#         
# #         if self.datastack.fits_calculated == 0:
# #             return
#                     
# 
#         fig = self.specfig
#         fig.clf()
#         fig.add_axes((0.15,0.15,0.75,0.75))
#         axes = fig.gca()
#         
#         xaxis = self.fitspectra[0,:]
#         spectrum = self.fitspectra[1,:]
#         
#         
#         minengtofit = 1.65
#         xmin = minengtofit*0.5
#         maxval = int(np.argmax(spectrum) + (np.argmax(spectrum)-minengtofit)*0.50)
#         xmax = xaxis[maxval]
# 
# 
#         wo_a = np.where(xaxis > xmax)[0]
#         if len(wo_a) > 0 :
#             wo_xmax = np.amin(wo_a) 
#         else:
#             wo_xmax = spectrum.size*8./10.
#         wo_b = np.where(xaxis < xmin)[0]
#         if len(wo_b) >0 :
#             wo_xmin = np.amax(wo_b) 
#         else:
#             wo_xmin = 0
# 
# 
#         wo = np.where(spectrum[wo_xmin:wo_xmax+1] > 0.)[0]
#         if len(wo) > 0 : 
#             ymin = np.amin(spectrum[wo+wo_xmin])*0.9 
#         else:
#             ymin = 0.1
# 
#         if len(wo) > 0 : 
#             ymax = np.amax(spectrum[wo+wo_xmin]*1.1) 
#         else: 
#             ymax = np.amax(spectrum)
#         # make sure ymax is larger than ymin, so as to avoid a crash during plotting
#         if ymax <= ymin : ymax = ymin+0.001
#         
#         axes.set_xlim((xmin, xmax))
#         axes.set_ylim((ymin, ymax))
#          
#         axes.set_xlabel('Photon Energy [keV]')
#         axes.set_ylabel('Counts')
# 
#         axes.set_yscale('log')
#         
#             
#         
#         for i in range(1,self.nfitlines):
#             if np.sum(self.fitspectra[i,:]) == 0:
#                 continue
#             specplot = axes.plot(self.fitspectra[0,:], self.fitspectra[i,:], label = self.fitspec_names[i])
#             
#         
#         fontP = matplotlib.font_manager.FontProperties()
#         fontP.set_size('small')
#        
#         axes.legend(loc=1, prop = fontP)            
# 
#          
#         self.SpecPanel.draw()
#         
#         
#         
#         
#         
# """ ------------------------------------------------------------------------------------------------"""        
# class FitParameters(QtGui.QDialog):
# 
#     def __init__(self, parent, datastack, Cmp):    
#         QtGui.QWidget.__init__(self, parent)
#         
#         self.parent = parent
#         self.datastack = datastack
#         self.Cmp = Cmp
#         
#         self.resize(700, 800)
#         self.setWindowTitle('Fit Parameters')
#         
#         pal = QtGui.QPalette()
#         self.setAutoFillBackground(True)
#         pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
#         self.setPalette(pal)       
#         
#         vbox1 = QtGui.QVBoxLayout()
#         
#         self.te = QtGui.QPlainTextEdit(self)
#         vbox1.addWidget(self.te)
#         
#         hbox1 = QtGui.QHBoxLayout()
#         self.button_save = QtGui.QPushButton('Save Fit Parameters')
#         self.button_save.clicked.connect( self.OnSave)
#         self.button_save.setMinimumWidth(200)
#         hbox1.addWidget(self.button_save)
#         
#         
#         self.button_close = QtGui.QPushButton('Close')
#         self.button_close.clicked.connect( self.OnClose)
#         self.button_close.setMinimumWidth(200)
#         hbox1.addWidget(self.button_close)
#         
# 
#         vbox1.addLayout(hbox1)
# 
#         self.setLayout(vbox1)
#         
#         self.ShowText()
#                 
# #----------------------------------------------------------------------        
#     def OnSave(self):
#         #Create mapspy settings file
#         maps_fpfile = 'maps_fit_parameters_override.txt'     
#         fname = os.path.join(str(self.datastack.mp_outdir),maps_fpfile)
#         f = open(fname, 'w')                          
#         filedata = self.te.toPlainText()
#         f.write(filedata) 
#         f.close() 
#         
#         msg = 'Fit parameters were saved to\n'+fname+'.'
#         QtGui.QMessageBox.information(self,'Info',msg)
#         
#         self.close() 
#     
# #----------------------------------------------------------------------        
#     def OnClose(self):
#         self.close()     
#         
# 
# #----------------------------------------------------------------------        
#     def ShowText(self):
#            
#         self.te.appendPlainText( '   This file will override default fit settings for the maps program for a 3 element detector remove: removeme_*elementdetector_to make it work. ')
#         self.te.appendPlainText( '   note, the filename MUST be maps_fit_parameters_override.txt')
#     
#         self.te.appendPlainText( 'VERSION:' + str(self.Cmp.fileversion))
#         self.te.appendPlainText( 'DATE:' + str(datetime.datetime.utcnow()))
#         self.te.appendPlainText( '   put below the number of detectors that were used to acquire spectra. IMPORTANT:')
#         self.te.appendPlainText( '   this MUST come after VERSION, and before all other options!')
#         self.te.appendPlainText( 'DETECTOR_ELEMENTS:' + str(self.Cmp.n_detector_elements))
#         self.te.appendPlainText( '   give this file an internal name, whatever you like')
#         self.te.appendPlainText( 'IDENTIFYING_NAME_[WHATEVERE_YOU_LIKE]:' + 'automatic')
#     
#         self.te.appendPlainText( '   list the elements that you want to be fit. For K lines, just use the element')
#         self.te.appendPlainText( '   name, for L lines add _L, e.g., Au_L, for M lines add _M')
# 
#         elements = []
#         for i in range(len(self.datastack.elementstofit)):
#             elements.append(self.datastack.elementstofit[i].replace('_K',''))
#         self.te.appendPlainText( 'ELEMENTS_TO_FIT: '+', '.join(elements))
#             
#         self.te.appendPlainText( '   list the element combinations you want to fit for pileup, e.g., Si_Si, Si_Si_Si, Si_Cl, etc')
#         self.te.appendPlainText( 'ELEMENTS_WITH_PILEUP: '+ self.Cmp.pileup_string)
#                      
#  
#          
#         self.te.appendPlainText( '   offset of energy calibration, in kev')
#         self.te.appendPlainText( 'CAL_OFFSET_[E_OFFSET]:'+ str(self.Cmp.cal_offset))
#         self.te.appendPlainText( 'CAL_OFFSET_[E_OFFSET]_MAX:'+ str(self.Cmp.cal_offset_min))
#         self.te.appendPlainText( 'CAL_OFFSET_[E_OFFSET]_MIN:'+ str(self.Cmp.cal_offset_max))
#      
#         self.te.appendPlainText( '   slope of energy calibration, in leV / channel')
#         self.te.appendPlainText( 'CAL_SLOPE_[E_LINEAR]:'+ str(self.Cmp.cal_slope))
#         self.te.appendPlainText( 'CAL_SLOPE_[E_LINEAR]_MAX:'+ str(self.Cmp.cal_slope_min))
#         self.te.appendPlainText( 'CAL_SLOPE_[E_LINEAR]_MIN:'+ str(self.Cmp.cal_slope_max))
#      
#         self.te.appendPlainText( '   quadratic correction for energy calibration, unless you know exactly what you are doing, please leave it at 0.')
#         self.te.appendPlainText( 'CAL_QUAD_[E_QUADRATIC]:'+ str(self.Cmp.cal_quad))
#         self.te.appendPlainText( 'CAL_QUAD_[E_QUADRATIC]_MAX:'+ str(self.Cmp.cal_quad_min))
#         self.te.appendPlainText( 'CAL_QUAD_[E_QUADRATIC]_MIN:'+ str(self.Cmp.cal_quad_max))
#      
#         self.te.appendPlainText( '    energy_resolution at 0keV')
#         self.te.appendPlainText( 'FWHM_OFFSET:'+ str(self.Cmp.fwhm_offset))
#      
#         self.te.appendPlainText( '    energy dependence of the energy resolution')
#         self.te.appendPlainText( 'FWHM_FANOPRIME:'+ str(self.Cmp.fwhm_fanoprime))
#      
#         self.te.appendPlainText( '    incident energy')
#         self.te.appendPlainText( 'COHERENT_SCT_ENERGY:'+ str(self.Cmp.coherent_sct_energy))
#         self.te.appendPlainText( '    upper constraint for the incident energy')
#         self.te.appendPlainText( 'COHERENT_SCT_ENERGY_MAX:'+ str(self.Cmp.coherent_sct_energy_max))
#         self.te.appendPlainText( '    lower constraint for the incident energy')
#         self.te.appendPlainText( 'COHERENT_SCT_ENERGY_MIN:'+ str(self.Cmp.coherent_sct_energy_min))
#         self.te.appendPlainText( '    angle for the compton scatter (in degrees)')
#         self.te.appendPlainText( 'COMPTON_ANGLE:'+ str(self.Cmp.compton_angle))
#         self.te.appendPlainText( 'COMPTON_ANGLE_MAX:'+ str(self.Cmp.compton_angle_max))
#         self.te.appendPlainText( 'COMPTON_ANGLE_MIN:'+ str(self.Cmp.compton_angle_min))
#         self.te.appendPlainText( '    additional width of the compton')
#         self.te.appendPlainText( 'COMPTON_FWHM_CORR:'+ str(self.Cmp.compton_fwhm_corr))
#         self.te.appendPlainText( 'COMPTON_STEP:'+ str(self.Cmp.compton_step))
#         self.te.appendPlainText( 'COMPTON_F_TAIL:'+ str(self.Cmp.compton_f_tail))
#         self.te.appendPlainText( 'COMPTON_GAMMA:'+ str(self.Cmp.compton_gamma))
#         self.te.appendPlainText( 'COMPTON_HI_F_TAIL:'+ str(self.Cmp.compton_hi_f_tail))
#         self.te.appendPlainText( 'COMPTON_HI_GAMMA:'+ str(self.Cmp.compton_hi_gamma))
#          
#         self.te.appendPlainText( '    tailing parameters, see also Grieken, Markowicz, Handbook of X-ray spectrometry')
#         self.te.appendPlainText( '    2nd ed, van Espen spectrum evaluation page 287.  _A corresponds to f_S, _B to'   ) 
#         self.te.appendPlainText( '    f_T and _C to gamma'    )
#         self.te.appendPlainText( 'STEP_OFFSET:'+ str(self.Cmp.step_offset))
#         self.te.appendPlainText( 'STEP_LINEAR:'+ str(self.Cmp.step_linear))
#         self.te.appendPlainText( 'STEP_QUADRATIC:'+ str(self.Cmp.step_quadratic))
#         self.te.appendPlainText( 'F_TAIL_OFFSET:'+ str(self.Cmp.f_tail_offset))
#         self.te.appendPlainText( 'F_TAIL_LINEAR:'+ str(self.Cmp.f_tail_linear))
#         self.te.appendPlainText( 'F_TAIL_QUADRATIC:'+ str(self.Cmp.f_tail_quadratic))
#         self.te.appendPlainText( 'KB_F_TAIL_OFFSET:'+ str(self.Cmp.kb_f_tail_offset))
#         self.te.appendPlainText( 'KB_F_TAIL_LINEAR:'+ str(self.Cmp.kb_f_tail_linear))
#         self.te.appendPlainText( 'KB_F_TAIL_QUADRATIC:'+ str(self.Cmp.kb_f_tail_quadratic))
#         self.te.appendPlainText( 'GAMMA_OFFSET:'+ str(self.Cmp.gamma_offset))
#         self.te.appendPlainText( 'GAMMA_LINEAR:'+ str(self.Cmp.gamma_linear))
#         self.te.appendPlainText( 'GAMMA_QUADRATIC:'+ str(self.Cmp.gamma_quadratic))
#         self.te.appendPlainText( '    snip width is the width used for estimating background. 0.5 is typically a good start '    )
#         self.te.appendPlainText( 'SNIP_WIDTH:'+ str(self.Cmp.snip_width))
#         self.te.appendPlainText( '    set FIT_SNIP_WIDTH to 1 to fit the width of the snipping for background estimate, set to 0 not to. Only use if you know what it is doing!')
#         self.te.appendPlainText( 'FIT_SNIP_WIDTH:'+ str(self.Cmp.fit_snip_width))
#         self.te.appendPlainText( '    detector material: 0= Germanium, 1 = Si')
#         self.te.appendPlainText( 'DETECTOR_MATERIAL:'+ str(self.Cmp.detector_material))
#         self.te.appendPlainText( '    beryllium window thickness, in micrometers, typically 8 or 24')
#         self.te.appendPlainText( 'BE_WINDOW_THICKNESS:'+ str(self.Cmp.be_window_thickness))
#         self.te.appendPlainText( 'thickness of the detector chip, e.g., 350 microns for an SDD')
#         self.te.appendPlainText( 'DET_CHIP_THICKNESS:'+ str(self.Cmp.det_chip_thickness))
#         self.te.appendPlainText( 'thickness of the Germanium detector dead layer, in microns, for the purposes of the NBS calibration')
#         self.te.appendPlainText( 'GE_DEAD_LAYER:'+ str(self.Cmp.ge_dead_layer))
#      
#         self.te.appendPlainText( '    maximum energy value to fit up to [keV]')
#         self.te.appendPlainText( 'MAX_ENERGY_TO_FIT:'+ str(self.Cmp.max_energy_to_fit))
#         self.te.appendPlainText( '    minimum energy value [keV]')
#         self.te.appendPlainText( 'MIN_ENERGY_TO_FIT:'+ str(self.Cmp.min_energy_to_fit))
#      
#         self.te.appendPlainText( '    this allows manual adjustment of the branching ratios between the different lines of L1, L2, and L3.')
#         self.te.appendPlainText( '    note, the numbers that are put in should be RELATIVE modifications, i.e., a 1 will correspond to exactly the literature value,')
#         self.te.appendPlainText( '    0.8 will correspond to to 80% of that, etc.')
#      
#         for item in self.Cmp.branching_fam_adj_l:
#             self.te.appendPlainText( 'BRANCHING_FAMILY_ADJUSTMENT_L: ' + item)
#     
#         self.te.appendPlainText( '    this allows manual adjustment of the branching ratios between the different L lines, such as La 1, la2, etc.')
#         self.te.appendPlainText( '    Please note, these are all RELATIVE RELATIVE modifications, i.e., a 1 will correspond to exactly the literature value, etc.')
#         self.te.appendPlainText( '    all will be normalized to the La1 line, and the values need to be in the following order:')
#         self.te.appendPlainText( '    La1, La2, Lb1, Lb2, Lb3, Lb4, Lg1, Lg2, Lg3, Lg4, Ll, Ln')
#         self.te.appendPlainText( '    please note, the first value (la1) MUST BE A 1. !!!')
#      
#         for item in self.Cmp.branching_ratio_adj_l:
#             self.te.appendPlainText( 'BRANCHING_RATIO_ADJUSTMENT_L: '+ item)
#   
#         self.te.appendPlainText( '    this allows manual adjustment of the branching ratios between the different K lines, such as Ka1, Ka2, Kb1, Kb2')
#         self.te.appendPlainText( '    Please note, these are all RELATIVE RELATIVE modifications, i.e., a 1 will correspond to exactly the literature value, etc.')
#         self.te.appendPlainText( '    all will be normalized to the Ka1 line, and the values need to be in the following order:')
#         self.te.appendPlainText( '    Ka1, Ka2, Kb1(+3), Kb2')
#         self.te.appendPlainText( '    please note, the first value (Ka1) MUST BE A 1. !!!')
#  
#         for item in self.Cmp.branching_ratio_adj_k:
#             self.te.appendPlainText( 'BRANCHING_RATIO_ADJUSTMENT_K: '+item)    
#  
#      
#         self.te.appendPlainText( '    the parameter adds the escape peaks (offset) to the fit if larger than 0. You should not enable Si and Ge at the same time, ie, one of these two values should be zero')
#         self.te.appendPlainText( 'SI_ESCAPE_FACTOR:'+ str(self.Cmp.si_escape_factor))
#         self.te.appendPlainText( 'GE_ESCAPE_FACTOR:'+ str(self.Cmp.ge_escape_factor))
#         self.te.appendPlainText( '    this parameter adds a component to the escape peak that depends linear on energy')
#         self.te.appendPlainText( 'LINEAR_ESCAPE_FACTOR:'+ str(self.Cmp.linear_escape_factor))
#         self.te.appendPlainText( '    the parameter enables fitting of the escape peak strengths. set 1 to enable, set to 0 to disable. (in matrix fitting always disabled)')
#         self.te.appendPlainText( 'SI_ESCAPE_ENABLE:'+ str(self.Cmp.si_escape_enable))
#         self.te.appendPlainText( 'GE_ESCAPE_ENABLE:'+ str(self.Cmp.ge_escape_enable))
# 
#          
#         self.te.appendPlainText( '    the lines (if any) below will override the detector names built in to maps. please modify only if you are sure you understand the effect')
#         self.te.appendPlainText( 'SRCURRENT:'+ self.Cmp.srcurrent)
#         self.te.appendPlainText( 'US_IC:'+ self.Cmp.us_ic)
#         self.te.appendPlainText( 'DS_IC:'+ self.Cmp.ds_ic)
#         self.te.appendPlainText( 'DPC1_IC:'+ self.Cmp.dpc1_ic)
#         self.te.appendPlainText( 'DPC2_IC:'+ self.Cmp.dpc2_ic)
#         self.te.appendPlainText( 'CFG_1:'+ self.Cmp.cfg_1)
#         self.te.appendPlainText( 'CFG_2:'+ self.Cmp.cfg_2)
#         self.te.appendPlainText( 'CFG_3:'+ self.Cmp.cfg_3)
#         self.te.appendPlainText( 'CFG_4:'+ self.Cmp.cfg_4)
#         self.te.appendPlainText( 'CFG_5:'+ self.Cmp.cfg_5)
#         self.te.appendPlainText( 'CFG_6:'+ self.Cmp.cfg_6)
#         self.te.appendPlainText( 'CFG_7:'+ self.Cmp.cfg_7)
#         self.te.appendPlainText( 'CFG_8:'+ self.Cmp.cfg_8)
#         self.te.appendPlainText( 'CFG_9:'+ self.Cmp.cfg_9)
#         self.te.appendPlainText( 'ELT1:'+ self.Cmp.ELT1)
#         self.te.appendPlainText( 'ERT1:'+ self.Cmp.ERT1)
# #         self.te.appendPlainText( 'ICR1:'+ str(self.Cmp.ICR1))
# #         self.te.appendPlainText( 'OCR1:'+ str(self.Cmp.OCR1))
# #         self.te.appendPlainText( 'AIRPATH:'+ str(self.Cmp.airpath))
#      
#         self.te.appendPlainText( '    the lines below (if any) give backup description of IC amplifier sensitivity, in case it cannot be found in the mda file')
#         self.te.appendPlainText( '      for the amps, the _NUM value should be between 0 and 8 where 0=1, 1=2, 2=5, 3=10, 4=20, 5=50, 6=100, 7=200, 8=500')
#         self.te.appendPlainText( '      for the amps, the _UNIT value should be between 0 and 3 where 0=pa/v, 1=na/v, 2=ua/v 3=ma/v')
#         self.te.appendPlainText( 'US_AMP_SENS_NUM:'+ str(self.Cmp.US_AMP_SENS_NUM))
#         self.te.appendPlainText( 'US_AMP_SENS_UNIT:'+ str(self.Cmp.US_AMP_SENS_UNIT))
#         self.te.appendPlainText( 'DS_AMP_SENS_NUM:'+ str(self.Cmp.DS_AMP_SENS_NUM))
#         self.te.appendPlainText( 'DS_AMP_SENS_UNIT:'+ str(self.Cmp.DS_AMP_SENS_UNIT ))   
#     



#---------------------------------------------------------------------- 
class FitParams(QtGui.QDialog):

    def __init__(self, parent, fitinfo):    
        QtGui.QWidget.__init__(self, parent)
        
        #print elements[elindex].name, elements[elindex].xrf['ka1'], elements[elindex].xrf['la1']
         
        self.parent = parent
                           
        self.resize(700, 600)
        self.setWindowTitle('Fit Parameters')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)
                         
        
        vboxtop = QtGui.QVBoxLayout()
        vboxtop.setContentsMargins(20,20,20,20)

        tc_finfo = QtGui.QTextEdit(parent)
        tc_finfo.setReadOnly(True)
        tc_finfo.setLineWrapMode(QtGui.QTextEdit.NoWrap)
        
        font = tc_finfo.font()
        font.setFamily("Courier")
        font.setPointSize(10)
        
        vboxtop.addWidget(tc_finfo)        
        tc_finfo.moveCursor(QTextCursor.End)
        tc_finfo.setCurrentFont(font)
        
        tc_finfo.insertPlainText(fitinfo)
        
        sb = tc_finfo.verticalScrollBar()
        sb.setValue(sb.maximum())


        hbox1 = QtGui.QHBoxLayout()
        button_cancel = QtGui.QPushButton('Close')
        button_cancel.clicked.connect(self.close)
        hbox1.addWidget(button_cancel)
         

        vboxtop.addLayout(hbox1)

        self.setLayout(vboxtop)







""" ------------------------------------------------------------------------------------------------"""
class PageElements(QtGui.QWidget):
    def __init__(self, datastack, Canalysis):
        super(PageElements, self).__init__()

        self.initUI(datastack, Canalysis)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack, Canalysis): 
        
        self.datastack = datastack
        
        self.Canalysis = Canalysis
                

        #panel 1
        sizer1 = QtGui.QGroupBox('Elements')
        vbox1 = QtGui.QVBoxLayout()

    
#         t1 = QtGui.QLabel(self)
#         t1.setText("Peak ID Energies")       
#       vbox1.addWidget(t1)
        self.eltablesize = 34
        self.lc_1 = QTableWidget()   
        self.lc_1.setRowCount(self.eltablesize)
        self.lc_1.setColumnCount(4)
        self.lc_1.setHorizontalHeaderLabels(['Element', 'K', 'L', 'M'])
        self.lc_1.setColumnWidth(0,65)
        self.lc_1.setColumnWidth(1,50)
        self.lc_1.setColumnWidth(2,50)
        self.lc_1.setColumnWidth(3,50)
        for i in range(self.eltablesize):
            self.lc_1.setRowHeight(i, 20)
        self.lc_1.itemClicked.connect(self.OnElementListClick)
        self.lc_1.setMinimumSize(200, 600)
        
        self.lc_1.doubleClicked.connect(self.OnElTableDoubleClick)
         

        vbox1.addWidget(self.lc_1)
        sizer1.setLayout(vbox1)
        
        
        
        #panel 5     
        vbox2 = QtGui.QVBoxLayout()
        
        self.tc_imageeng = QtGui.QLabel(self)
        self.tc_imageeng.setText(" ")
        vbox2.addWidget(self.tc_imageeng)
        

        hbox21 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.specfig = Figure((PlotW, PlotH))
        self.SpecPanel = FigureCanvas(self.specfig)
        self.SpecPanel.setParent(self)
        self.mpl_toolbar = NavigationToolbar(self.SpecPanel, self)
        
        fbox.addWidget(self.SpecPanel)
        frame.setLayout(fbox)
        hbox21.addWidget(frame)
        

#         self.slider_eng = QtGui.QScrollBar(QtCore.Qt.Vertical)
#         self.slider_eng.setFocusPolicy(QtCore.Qt.StrongFocus)
#         self.slider_eng.valueChanged[int].connect(self.OnScrollEng)
#         self.slider_eng.setRange(0, 100)
#         
#         hbox51.addWidget(self.slider_eng)        

        vbox2.addStretch(1)
        vbox2.addLayout(hbox21)
        vbox2.addWidget(self.mpl_toolbar)
        vbox2.addStretch(1)
        
        
        #panel 3
        sizer3 = QtGui.QGroupBox('Energy Calibration')
        vbox3 = QtGui.QVBoxLayout()
        
        
        self.tc_31 = QtGui.QLabel(self)
        vbox3.addWidget(self.tc_31)
        self.tc_31.setText('Pick 1 Element to calculate Offset') 
        
#         self.tc_32 = QtGui.QLabel(self)
#         vbox3.addWidget(self.tc_32)
#         self.tc_32.setText('Pick 2 Elements to calculate Offset and Slope') 
        
        self.button_calibrate = QtGui.QPushButton('Calculate Energy Offset')
        self.button_calibrate.clicked.connect( self.OnCalibrate)
        self.button_calibrate.setMinimumWidth(200)
        self.button_calibrate.setMaximumWidth(300)
        vbox3.addWidget(self.button_calibrate)

        vbox3.addStretch(1)
        
        sizer31 = QtGui.QGroupBox('Energy Calibration')
        vbox31 = QtGui.QVBoxLayout()
        
#         self.cb_engaxis = QtGui.QCheckBox('ROI', self)
#         self.cb_engaxis.setChecked(self.scattroi)
#         self.cb_engaxis.stateChanged.connect(self.OnScatterROI)
#         vbox31.addWidget(self.cb_engaxis)               

        hbox31 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Slope  ")  
        hbox31.addWidget(tc)
        self.ntc_ecals = QtGui.QDoubleSpinBox(self)  
        self.ntc_ecals.setFixedWidth(85)
        self.ntc_ecals.setRange(0, 99999)    
        self.ntc_ecals.setAlignment(QtCore.Qt.AlignRight)
        self.ntc_ecals.setValue(self.datastack.engcal_slope)
        self.ntc_ecals.valueChanged.connect(self.OnEnergyCalS)
        hbox31.addWidget(self.ntc_ecals) 
        tc = QtGui.QLabel(self)
        tc.setText("eV/channel")  
        hbox31.addWidget(tc)  
        hbox31.addStretch(1)   
        
        hbox32 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Offset")  
        hbox32.addWidget(tc)
        self.ntc_ecalo = QtGui.QDoubleSpinBox(self)  
        self.ntc_ecalo.setFixedWidth(85)
        self.ntc_ecalo.setRange(-99999, 99999)
        self.ntc_ecalo.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_ecalo.setValue(self.datastack.engcal_offset)
        self.ntc_ecalo.valueChanged.connect(self.OnEnergyCalO)
        hbox32.addWidget(self.ntc_ecalo) 
        tc = QtGui.QLabel(self)
        tc.setText("eV")  
        hbox32.addWidget(tc)     
        

        hbox32.addStretch(1)
        vbox31.addLayout(hbox31) 
        vbox31.addLayout(hbox32)          
        sizer31.setLayout(vbox31)
        vbox3.addWidget(sizer31)
        
        #hbox3.addStretch(1)
        
        vbox3.setContentsMargins(20,20,20,20)
        sizer3.setLayout(vbox3)        
        
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
        vboxt2 = QtGui.QVBoxLayout()

        vboxt1.addWidget(sizer1)
        vboxt1.addStretch (1)
        #vboxt1.addWidget(sizer2)
        
        vboxt2.addStretch (1)
        vboxt2.addLayout(vbox2)
        vboxt2.addStretch (1)
        vboxt2.addWidget(sizer3)
        
        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt2)
        hboxtop.addStretch (0.5)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.5)

        #vboxtop.addWidget(sizer4)
        #vboxtop.addStretch (0.5)

        vboxtop.setContentsMargins(50,20,50,20)
        self.setLayout(vboxtop)
        
        self.SetElementLinesTable()
        
        
#----------------------------------------------------------------------        
    def OnElementListClick(self):
        
        self.datastack.elementstofit = []
        
        for i in range(self.eltablesize):
            
            #Check K lines
            
            item1 = self.lc_1.item(i, 1)
            status = item1.checkState()
            if status:
                self.datastack.elementstofit.append(self.elements[self.elementlistindices[i]].name+'_K')


            item2 = self.lc_1.item(i, 2)
            status = item2.checkState()
            if status:
                self.datastack.elementstofit.append(self.elements[self.elementlistindices[i]].name+'_L')
             
             
            item3 = self.lc_1.item(i, 3)
            status = item3.checkState()
            if status:
                self.datastack.elementstofit.append(self.elements[self.elementlistindices[i]].name+'_M')
    
        
        self.ShowImage()
        
        self.window().page2.ShowImage()
        self.window().page4.UpdateDisplay()
        self.window().page5.UpdateDisplay()


#----------------------------------------------------------------------        
    def OnElTableDoubleClick(self, evt):
        
        if evt.column() != 0:
            return
        row = evt.row()
        elindex = self.elementlistindices[row]
        rewin = ElementEdit(self, elindex, self.elements, self.Canalysis)
        rewin.show()


#----------------------------------------------------------------------        
    def OnEnergyCalS(self): 
                
        try:       
            self.datastack.engcal_slope = float(self.ntc_ecals.text())
            
            self.window().page2.ntc_ecals.setText('{0:5.2f}'.format(self.datastack.engcal_slope))
            self.window().page5.ntc_ecals.setText('{0:5.2f}'.format(self.datastack.engcal_slope))
            self.window().page2.ShowImage()
            self.window().page4.UpdateDisplay()
            
        except:
            pass
        
        
#----------------------------------------------------------------------        
    def OnEnergyCalO(self): 
                     
        try:       
            self.datastack.engcal_offset = float(self.ntc_ecalo.text())
            
            self.window().page2.ntc_ecalo.setText('{0:5.2f}'.format(self.datastack.engcal_offset))
            self.window().page5.ntc_ecalo.setText('{0:5.2f}'.format(self.datastack.engcal_offset))
            self.window().page2.ShowImage()
            self.window().page4.UpdateDisplay()
            
        except:
            pass

#----------------------------------------------------------------------        
    def OnCalibrate(self, evt):

        
        if self.datastack.stack_loaded == 0:
            return
        
        if len(self.datastack.elementstofit)>1:
            QtGui.QMessageBox.warning(self, 'Warning','Please select only one element for offset calibration.')
            return
                 
        spectrum = np.sum(self.datastack.stack, axis=(0,1))
         
        nchanel = spectrum.shape[0]
        
        fitchanpos = []
        ka = []
        
        for i in range(len(self.elementlistindices)):
            if self.elements[i].name+'_K' in self.datastack.elementstofit:
#                 print self.elements[i].name
#                 print self.elements[i].xrf['ka1']*1000
                kalev = self.elements[i].xrf['ka1']*1000
                ka.append(kalev)
                chanpos = (kalev-self.datastack.engcal_offset)/self.datastack.engcal_slope
                
                p0 = [1.0, chanpos, 1.0]
                from scipy.optimize import curve_fit
                chanind = int(chanpos)
                channels = np.arange(nchanel)
                coeff, var_matrix = curve_fit(gauss, channels[chanind-100:chanind+100], spectrum[chanind-100:chanind+100], p0=p0)
                
                # Get the fitted curve
                spec_fit = gauss(channels, *coeff)
                fitchanpos.append(coeff[1])
                #print 'Fitted mean = ', coeff[1]
                #print 'Fitted standard deviation = ', coeff[2]
                
                              

        if len(fitchanpos) == 1:
            self.datastack.engcal_offset = ka[0]-fitchanpos[0]*self.datastack.engcal_slope
        else:
            self.datastack.engcal_slope = (ka[0]-ka[1])/(fitchanpos[0]-fitchanpos[1])
            self.datastack.engcal_offset = ka[0]-fitchanpos[0]*self.datastack.engcal_slope
        
        
        self.ntc_ecals.setValue(self.datastack.engcal_slope)
        self.ntc_ecalo.setValue(self.datastack.engcal_offset)
        self.window().page2.ntc_ecals.setText('{0:5.2f}'.format(self.datastack.engcal_slope))
        self.window().page2.ntc_ecalo.setText('{0:5.2f}'.format(self.datastack.engcal_offset))
        self.window().page5.ntc_ecals.setText('{0:5.2f}'.format(self.datastack.engcal_slope))
        self.window().page5.ntc_ecalo.setText('{0:5.2f}'.format(self.datastack.engcal_offset))
        self.window().page2.ShowImage()
        self.window().page4.UpdateDisplay()       
       
            
#         fig = self.specfig
#         fig.clf()
#         fig.add_axes((0.15,0.15,0.75,0.75))
#         axes = fig.gca()
#         
#          
#         axes.set_xlabel('Photon Energy Channel')
#         axes.set_ylabel('Counts')
# 
#         if self.window().page0.show_log == 1:
#             axes.set_yscale('log')
#         
#         axes.plot(channels, spectrum, label='Test data')
#         axes.plot(channels, spec_fit, label='Fitted data')     
#         
#         self.SpecPanel.draw()      

        
        
        #Use current calibration to get the current peak position for the element lines:
        
        
        
        return
    
        
        
#----------------------------------------------------------------------           
    def SetElementLinesTable(self):    
        
                
        elements = self.Canalysis.get_element_info()
        
        if elements == None:
            print 'Error: Could not find xrf_library.csv file!'
            return
        
        self.elementlistindices = []

        vlabels = []        
        for i in range(self.eltablesize-4):
            item = QTableWidgetItem(elements[i+5].name)
            self.lc_1.setItem(i, 0, item)
            item.setFlags(QtCore.Qt.ItemIsEnabled)
            vlabels.append(str(elements[i+5].z))
            self.elementlistindices.append(i+5)


        item = QTableWidgetItem(elements[77].name)
        self.lc_1.setItem(self.eltablesize-4, 0, item)
        item.setFlags(QtCore.Qt.ItemIsEnabled)
        vlabels.append(str(elements[77].z))     
        self.elementlistindices.append(77)            
            
        item = QTableWidgetItem(elements[78].name)
        self.lc_1.setItem(self.eltablesize-3, 0, item)
        item.setFlags(QtCore.Qt.ItemIsEnabled)
        vlabels.append(str(elements[78].z))     
        self.elementlistindices.append(78)
        
        item = QTableWidgetItem(elements[81].name)
        self.lc_1.setItem(self.eltablesize-2, 0, item)
        item.setFlags(QtCore.Qt.ItemIsEnabled)
        vlabels.append(str(elements[81].z))   
        self.elementlistindices.append(81)      
        
        item = QTableWidgetItem(elements[91].name)
        self.lc_1.setItem(self.eltablesize-1, 0, item)
        item.setFlags(QtCore.Qt.ItemIsEnabled)
        vlabels.append(str(elements[91].z)) 
        self.elementlistindices.append(91)              
            
        self.lc_1.setVerticalHeaderLabels(vlabels)
        
        
        
        for i in range(self.eltablesize):
            
            
            #Check K lines
            
            item1 = QtGui.QTableWidgetItem()
            self.lc_1.setItem(i, 1, item1)
            item1.setCheckState(QtCore.Qt.Unchecked)
            if (elements[self.elementlistindices[i]].name+'_K') in self.datastack.elementstofit:
                item1.setCheckState(QtCore.Qt.Checked)
            
            item2 = QtGui.QTableWidgetItem()
            self.lc_1.setItem(i, 2, item2)
            if elements[self.elementlistindices[i]].xrf['la1'] > 0:
                item2.setCheckState(QtCore.Qt.Unchecked)
                if (elements[self.elementlistindices[i]].name+'_L') in self.datastack.elementstofit:
                    item2.setCheckState(QtCore.Qt.Checked)
            else:
                item2.setFlags(QtCore.Qt.ItemIsEnabled)
            
            
            item3 = QtGui.QTableWidgetItem()
            self.lc_1.setItem(i, 3, item3)
            if elements[self.elementlistindices[i]].xrf['ma1'] > 0:
                item3.setCheckState(QtCore.Qt.Unchecked)
                if (elements[self.elementlistindices[i]].name+'_M') in self.datastack.elementstofit:
                    item3.setCheckState(QtCore.Qt.Checked)            
            else:
                item3.setFlags(QtCore.Qt.ItemIsEnabled)     
                
        self.elements = elements
        return   


#----------------------------------------------------------------------           
    def UpdateElementLinesTable(self, maxeng):
        
        for i in range(self.eltablesize):
            if self.elements[self.elementlistindices[i]].xrf['ka1'] > maxeng:
                self.lc_1.item(i, 1).setCheckState(QtCore.Qt.Unchecked)
                self.lc_1.item(i, 1).setFlags(QtCore.Qt.ItemIsUserCheckable) 
            else:
                self.lc_1.item(i, 1).setFlags(QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)  

            if self.elements[self.elementlistindices[i]].xrf['la1'] > maxeng:
                self.lc_1.item(i, 2).setCheckState(QtCore.Qt.Unchecked)
                self.lc_1.item(i, 2).setFlags(QtCore.Qt.ItemIsUserCheckable) 
            else:
                self.lc_1.item(i, 2).setFlags(QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)              
        
            if self.elements[self.elementlistindices[i]].xrf['ma1'] > maxeng:
                self.lc_1.item(i, 3).setCheckState(QtCore.Qt.Unchecked)
                self.lc_1.item(i, 3).setFlags(QtCore.Qt.ItemIsUserCheckable) 
            else:
                self.lc_1.item(i, 3).setFlags(QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)    
                
                
#----------------------------------------------------------------------        
    def ShowImage(self):
        
        if self.datastack.stack_loaded == 0:
            return
        
        spectrum = np.sum(self.datastack.stack, axis=(0,1))
        
        
        maxchanel = (self.datastack.inenergy+1000-self.datastack.engcal_offset)/self.datastack.engcal_slope
        

        fig = self.specfig
        fig.clf()
        fig.add_axes((0.15,0.15,0.75,0.75))
        axes = fig.gca()
        
         
        axes.set_xlabel('Photon Energy Channel')
        axes.set_ylabel('Counts')

        if self.window().page0.show_log == 1:
            axes.set_yscale('log')
        

        specplot = axes.plot(spectrum)

        
        axes.set_xlim([0,maxchanel])
        
         
        #Show selected element lines
        for i in range(len(self.datastack.elementstofit)):
            indx = self.Canalysis.roi_lines.index(self.datastack.elementstofit[i])
            center = (self.Canalysis.roi_center[indx]-self.datastack.engcal_offset)/self.datastack.engcal_slope
            calib = self.Canalysis.roi_calib[indx]   
            if calib == 1:
                colr = 'g'
            elif calib == 2:
                colr = 'b'
            elif calib == 3:
                colr = 'r'
            axes.axvline(x=center, color = colr, alpha=0.5)
            
            
         
        self.SpecPanel.draw()
        

#----------------------------------------------------------------------        
    def UpdateDisplay(self):
                                                                        
        if self.datastack.stack_loaded == 1:

            maxeng = self.datastack.inenergy/1000.
            self.UpdateElementLinesTable(maxeng)
                        
        self.ShowImage()
        
        
        
        
#---------------------------------------------------------------------- 
class ElementEdit(QtGui.QDialog):

    def __init__(self, parent, elindex, elements, Canalysis):    
        QtGui.QWidget.__init__(self, parent)
        
        #print elements[elindex].name, elements[elindex].xrf['ka1'], elements[elindex].xrf['la1']
         
        self.parent = parent
        self.Canalysis = Canalysis
        self.elindex = elindex
                           
        self.resize(400, 300)
        self.setWindowTitle('Element Edit')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)
                         
        
        vboxtop = QtGui.QVBoxLayout()
        vboxtop.setContentsMargins(20,20,20,20)
        
        
        gridtop = QtGui.QGridLayout()
        gridtop.setVerticalSpacing(20)
     
        fontb = QtGui.QFont()
        fontb.setBold(True)            
         
        st1 = QtGui.QLabel(self)
        st1.setText('Element name')
        st1.setFont(fontb)
        st2 = QtGui.QLabel(self)
        st2.setText('Line')
        st2.setFont(fontb)
        st3 = QtGui.QLabel(self)
        st3.setText('Center Position [eV]   ')
        st3.setFont(fontb)
        st4 = QtGui.QLabel(self)
        st4.setText('Low [eV]')
        st4.setFont(fontb)
        st5 = QtGui.QLabel(self)
        st5.setText('High [eV]')
        st5.setFont(fontb)   
        
        st6 = QtGui.QLabel(self)
        st6.setText('Ka1')
        st6.setFont(fontb)   
        
        st7 = QtGui.QLabel(self)
        st7.setText('La1')
        st7.setFont(fontb)   
        
        st8 = QtGui.QLabel(self)
        st8.setText('Ma1')
        st8.setFont(fontb)                

        self.tc_elname = QtGui.QLineEdit(self)
        self.tc_elname.setText(elements[elindex].name)
        self.tc_elname.setMaximumWidth(65)
        self.tc_elname.setReadOnly(True)

        self.ntc_centerk = QtGui.QLineEdit(self)
        self.ntc_centerk.setFixedWidth(65)
        self.ntc_centerk.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_centerk.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_centerk.setText(str(elements[elindex].xrf['ka1']*1000))

        self.ntc_centerl = QtGui.QLineEdit(self)
        self.ntc_centerl.setFixedWidth(65)
        self.ntc_centerl.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_centerl.setAlignment(QtCore.Qt.AlignRight)         
        
        
        self.ntc_centerm = QtGui.QLineEdit(self)
        self.ntc_centerm.setFixedWidth(65)
        self.ntc_centerm.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_centerm.setAlignment(QtCore.Qt.AlignRight)         
                
        
        elind = self.Canalysis.roi_lines.index(elements[elindex].name+'_K')
        kleft_roi, kright_roi = self.Canalysis.roi_posinfo[elind]

        self.ntc_lowk = QtGui.QLineEdit(self)
        self.ntc_lowk.setFixedWidth(65)
        self.ntc_lowk.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_lowk.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_lowk.setText(str(kleft_roi))
        
        self.ntc_highk = QtGui.QLineEdit(self)
        self.ntc_highk.setFixedWidth(65)
        self.ntc_highk.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_highk.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_highk.setText(str(kright_roi))
               

        self.ntc_lowl = QtGui.QLineEdit(self)
        self.ntc_lowl.setFixedWidth(65)
        self.ntc_lowl.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_lowl.setAlignment(QtCore.Qt.AlignRight)         
        
    
        self.ntc_highl = QtGui.QLineEdit(self)
        self.ntc_highl.setFixedWidth(65)
        self.ntc_highl.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_highl.setAlignment(QtCore.Qt.AlignRight)         
              
        
        if float(elements[elindex].xrf['la1']) > 0:
            
            elind = self.Canalysis.roi_lines.index(elements[elindex].name+'_L')
            lleft_roi, lright_roi = self.Canalysis.roi_posinfo[elind]
        
            self.ntc_centerl.setText(str(elements[elindex].xrf['la1']*1000))
            self.ntc_lowl.setText(str(lleft_roi))
            self.ntc_highl.setText(str(lright_roi))  
        else:
            self.ntc_centerl.setDisabled(True)
            self.ntc_lowl.setDisabled(True)
            self.ntc_highl.setDisabled(True)

        self.ntc_lowm = QtGui.QLineEdit(self)
        self.ntc_lowm.setFixedWidth(65)
        self.ntc_lowm.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_lowm.setAlignment(QtCore.Qt.AlignRight)         

        self.ntc_highm = QtGui.QLineEdit(self)
        self.ntc_highm.setFixedWidth(65)
        self.ntc_highm.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_highm.setAlignment(QtCore.Qt.AlignRight)     
        
        
        if float(elements[elindex].xrf['ma1']) > 0:
            
            elind = self.Canalysis.roi_lines.index(elements[elindex].name+'_M')
            mleft_roi, mright_roi = self.Canalysis.roi_posinfo[elind]      
            
            self.ntc_centerm.setText(str(elements[elindex].xrf['ma1']*1000))      
            self.ntc_lowm.setText(str( mleft_roi))
            self.ntc_highm.setText(str(mright_roi))

        else:
            self.ntc_centerm.setDisabled(True)
            self.ntc_lowm.setDisabled(True)
            self.ntc_highm.setDisabled(True)
            
 
        gridtop.addWidget(st1, 0, 0)
        gridtop.addWidget(st2, 1, 0)
        gridtop.addWidget(st3, 2, 0)
        gridtop.addWidget(st4, 3, 0)
        gridtop.addWidget(st5, 4, 0)
        
        gridtop.addWidget(st6, 1, 1)
        gridtop.addWidget(st7, 1, 2)
        gridtop.addWidget(st8, 1, 3)
                                
        gridtop.addWidget(self.tc_elname, 0, 1)
        gridtop.addWidget(self.ntc_centerk, 2, 1)
        gridtop.addWidget(self.ntc_centerl, 2, 2)  
        gridtop.addWidget(self.ntc_centerm, 2, 3)  

        gridtop.addWidget(self.ntc_lowk, 3, 1)
        gridtop.addWidget(self.ntc_lowl, 3, 2)  
        gridtop.addWidget(self.ntc_lowm, 3, 3)  
        
        gridtop.addWidget(self.ntc_highk, 4, 1)
        gridtop.addWidget(self.ntc_highl, 4, 2)  
        gridtop.addWidget(self.ntc_highm, 4, 3)  

#         
        vboxtop.addStretch(0.5)
        vboxtop.addLayout(gridtop)
        vboxtop.addStretch(1)
        
          
         
        hbox1 = QtGui.QHBoxLayout()
        button_save = QtGui.QPushButton('Save Changes')
        button_save.clicked.connect(self.OnSave)
        hbox1.addWidget(button_save)
          
        button_cancel = QtGui.QPushButton('Dismiss Changes')
        button_cancel.clicked.connect(self.close)
        hbox1.addWidget(button_cancel)
         
        vboxtop.addStretch(1.0)
        vboxtop.addLayout(hbox1)

        self.setLayout(vboxtop)



#----------------------------------------------------------------------        
    def OnSave(self): 
        
        
        self.Canalysis.elements[self.elindex].xrf['ka1'] = float(self.ntc_centerk.text())/1000     
        
        
        kleft_roi = float(self.ntc_lowk.text())
        kright_roi = float(self.ntc_highk.text())
        
        elind = self.Canalysis.roi_lines.index(self.Canalysis.elements[self.elindex].name+'_K')
        self.Canalysis.roi_posinfo[elind] = (kleft_roi, kright_roi)
        self.Canalysis.roi_center[elind] = float(self.ntc_centerk.text())
              
        
        if float(self.Canalysis.elements[self.elindex].xrf['la1']) > 0:
            
            self.Canalysis.elements[self.elindex].xrf['la1'] = float(self.ntc_centerl.text())/1000
            
            lleft_roi = float(self.ntc_lowl.text())
            lright_roi = float(self.ntc_highl.text())
            
            elind = self.Canalysis.roi_lines.index(self.Canalysis.elements[self.elindex].name+'_L')
            self.Canalysis.roi_posinfo[elind] = (lleft_roi, lright_roi)
            self.Canalysis.roi_center[elind] = float(self.ntc_centerl.text())

                    
        
        if float(self.Canalysis.elements[self.elindex].xrf['ma1']) > 0:
            
            self.Canalysis.elements[self.elindex].xrf['ma1'] = float(self.ntc_centerm.text())/1000
            
            mleft_roi = float(self.ntc_lowm.text())
            mright_roi = float(self.ntc_highm.text())
            
            elind = self.Canalysis.roi_lines.index(self.Canalysis.elements[self.elindex].name+'_M')
            self.Canalysis.roi_posinfo[elind] = (mleft_roi, mright_roi)
            self.Canalysis.roi_center[elind] = float(self.ntc_centerm.text())

            
                    
        self.parent.UpdateDisplay()
        self.close()
             
                        
                        

""" ------------------------------------------------------------------------------------------------"""
class PageROI(QtGui.QWidget):
    def __init__(self, datastack, Canalysis):
        super(PageROI, self).__init__()

        self.initUI(datastack, Canalysis)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack, Canalysis): 
        
        self.datastack = datastack
        
        self.Canalysis = Canalysis
                        
        self.scattroi = False
        self.scattmin = 0
        self.scattmax = 10000
        self.scattsum = 0


        
        #panel 2
        sizer2 = QtGui.QGroupBox('ROI Analysis')
        vbox2 = QtGui.QVBoxLayout()
        
        self.button_calcrois = QtGui.QPushButton('Calculate ROIs')
        self.button_calcrois.clicked.connect( self.OnCalcROIs)
        self.button_calcrois.setMinimumWidth(200)
        vbox2.addWidget(self.button_calcrois)
        
        self.button_saverois = QtGui.QPushButton('Save ROI Plot')
        self.button_saverois.clicked.connect( self.OnSaveROIs)   
        self.button_saverois.setEnabled(False)
        vbox2.addWidget(self.button_saverois)

        self.button_savespec = QtGui.QPushButton('Save Integrated Spectrum')
        self.button_savespec.clicked.connect( self.OnSaveSpectrum)   
        vbox2.addWidget(self.button_savespec)
        
        
        if havexrftomo:
            line = QtGui.QFrame()
            line.setFrameShape(QtGui.QFrame.HLine)
            line.setFrameShadow(QtGui.QFrame.Sunken) 
            vbox2.addWidget(line)
        
            self.button_calc_4D = QtGui.QPushButton('Calculate ROIs for all angles')
            self.button_calc_4D.clicked.connect(self.OnCalcROIs4D)
            self.button_calc_4D.setEnabled(False)
            vbox2.addWidget(self.button_calc_4D)
        
        sizer2.setLayout(vbox2)
        
        
        
        #panel 3
        sizer3 = QtGui.QGroupBox('Extras')
        hbox3 = QtGui.QHBoxLayout()
        
        sizer31 = QtGui.QGroupBox('Energy Calibration')
        vbox31 = QtGui.QVBoxLayout()
        
#         self.cb_engaxis = QtGui.QCheckBox('ROI', self)
#         self.cb_engaxis.setChecked(self.scattroi)
#         self.cb_engaxis.stateChanged.connect(self.OnScatterROI)
#         vbox31.addWidget(self.cb_engaxis)               

        hbox31 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Slope  ")  
        hbox31.addWidget(tc)
        self.ntc_ecals = QtGui.QLineEdit(self)
        self.ntc_ecals.setFixedWidth(65)
        self.ntc_ecals.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_ecals.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_ecals.setText(str(self.datastack.engcal_slope))
        #self.ntc_ecals.textChanged.connect(self.OnEnergyCal)
        self.ntc_ecals.setReadOnly(True)
        hbox31.addWidget(self.ntc_ecals) 
        tc = QtGui.QLabel(self)
        tc.setText("eV/channel")  
        hbox31.addWidget(tc)  
        hbox31.addStretch(1)   
        
        hbox32 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Offset")  
        hbox32.addWidget(tc)
        self.ntc_ecalo = QtGui.QLineEdit(self)
        self.ntc_ecalo.setFixedWidth(65)
        self.ntc_ecalo.setValidator(QtGui.QDoubleValidator(-99999, 99999, 2, self))
        self.ntc_ecalo.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_ecalo.setText(str(self.datastack.engcal_offset))
        #self.ntc_ecalo.textChanged.connect(self.OnEnergyCal)
        self.ntc_ecalo.setReadOnly(True)
        hbox32.addWidget(self.ntc_ecalo) 
        tc = QtGui.QLabel(self)
        tc.setText("eV")  
        hbox32.addWidget(tc)      

        hbox32.addStretch(1)
        vbox31.addLayout(hbox31) 
        vbox31.addLayout(hbox32)          
        sizer31.setLayout(vbox31)
        hbox3.addWidget(sizer31)
        #hbox3.addStretch(1)
        
 
        sizer32 = QtGui.QGroupBox('Scatter peak')
        vbox32 = QtGui.QVBoxLayout()

        self.cb_usescatt = QtGui.QCheckBox('ROI', self)
        self.cb_usescatt.setChecked(self.scattroi)
        self.cb_usescatt.stateChanged.connect(self.OnScatterROI)
        vbox32.addWidget(self.cb_usescatt)       
         
        hbox33 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("Low [eV]")  
        hbox33.addWidget(tc)  
        self.ntc_scattmin = QtGui.QLineEdit(self)
        self.ntc_scattmin.setFixedWidth(65)
        self.ntc_scattmin.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_scattmin.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_scattmin.setText(str(self.scattmin))
        self.ntc_scattmin.textChanged.connect(self.OnScatterMM)
        hbox33.addWidget(self.ntc_scattmin) 
        hbox33.addStretch(1)   
        vbox32.addLayout(hbox33)   
        
        hbox34 = QtGui.QHBoxLayout()
        tc = QtGui.QLabel(self)
        tc.setText("High [eV]")  
        hbox34.addWidget(tc)
        self.ntc_scattmax = QtGui.QLineEdit(self)
        self.ntc_scattmax.setFixedWidth(65)
        self.ntc_scattmax.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_scattmax.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_scattmax.setText(str(self.scattmax))
        self.ntc_scattmax.textChanged.connect(self.OnScatterMM)
        hbox34.addWidget(self.ntc_scattmax) 
        vbox32.addLayout(hbox34)
        
        hbox35 = QtGui.QHBoxLayout()
        self.tc_scattsum  = QtGui.QLabel(self)
        self.tc_scattsum.setText("Sum: ")  
        hbox35.addWidget(self.tc_scattsum )
        vbox32.addLayout(hbox35)
                 
        sizer32.setLayout(vbox32)
        hbox3.addWidget(sizer32)
        hbox3.addStretch(1)
                
        hbox3.setContentsMargins(5,5,5,5)
        sizer3.setLayout(hbox3)

        
        #panel 5     
        vbox5 = QtGui.QVBoxLayout()
        
        self.tc_imageeng = QtGui.QLabel(self)
        self.tc_imageeng.setText(" ")
        vbox5.addWidget(self.tc_imageeng)
        

        hbox51 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.specfig = Figure((PlotW*1.2, PlotH*1.2))
        self.SpecPanel = FigureCanvas(self.specfig)
        self.SpecPanel.setParent(self)
        
        self.mpl_toolbar = NavigationToolbar(self.SpecPanel, self)

        #self.PlotPanel.mpl_connect('key_press_event', self.on_key_press)
             
        
        fbox.addWidget(self.SpecPanel)
        frame.setLayout(fbox)
        hbox51.addWidget(frame)
        

#         self.slider_eng = QtGui.QScrollBar(QtCore.Qt.Vertical)
#         self.slider_eng.setFocusPolicy(QtCore.Qt.StrongFocus)
#         self.slider_eng.valueChanged[int].connect(self.OnScrollEng)
#         self.slider_eng.setRange(0, 100)
#         
#         hbox51.addWidget(self.slider_eng)        

        vbox5.addStretch(1)
        vbox5.addLayout(hbox51)
        vbox5.addWidget(self.mpl_toolbar)
        vbox5.addStretch(1)
        
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer2)

        vboxt1.addStretch (1)
        #vboxt1.addWidget(sizer2)
        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vbox5)
        hboxtop.addStretch (0.5)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.5)
        vboxtop.addWidget(sizer3)
        vboxtop.addStretch (0.5)
        #vboxtop.addWidget(sizer4)
        #vboxtop.addStretch (0.5)

        vboxtop.setContentsMargins(50,20,50,20)
        self.setLayout(vboxtop)
        
                  
                

#----------------------------------------------------------------------        
    def OnScatterROI(self):                
                
        if self.cb_usescatt.isChecked():
            self.scattroi = True
        else: self.scattroi = False
        
        self.ShowImage()
        
#----------------------------------------------------------------------        
    def OnScatterMM(self): 
         
        try:       
            self.scattmin = float(self.ntc_scattmin.text())
            self.scattmax = float(self.ntc_scattmax.text())
            
            self.ShowImage()
        except:
            pass

# #----------------------------------------------------------------------        
#     def OnEnergyCalS(self): 
#          
#         try:       
#             self.datastack.engcal_slope = float(self.ntc_ecals.text())
#             
#             self.ShowImage()
#             
#             self.window().page4.UpdateDisplay()
#             
#             self.window().page5.ntc_ecals.setText(str(self.datastack.engcal_slope))
#             
#         except:
#             pass
#         
# #----------------------------------------------------------------------        
#     def OnEnergyCalO(self): 
#          
#         try:       
#             self.datastack.engcal_offset = float(self.ntc_ecalo.text())
#             
#             self.ShowImage()
#             
#             self.window().page4.UpdateDisplay()
#             
#             self.window().page5.ntc_ecalo.setText(str(self.datastack.engcal_offset))
#             
#         except:
#             pass
        
#----------------------------------------------------------------------        
    def OnCalcROIs(self):
        
        if self.datastack.stack_loaded == 0:
            return

        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))    
        
        self.datastack.ROI_data = []
        self.datastack.ROI_info = []
        self.datastack.ROI_names = []
        self.datastack.ROI_maps = []
        
        if self.scattroi:
            self.scattmin = float(self.ntc_scattmin.text())
            self.scattmax = float(self.ntc_scattmax.text())
            scatterpeak = [self.scattmin, self.scattmax]
        else:
            scatterpeak = []
        
        self.datastack.ROI_info, self.datastack.ROI_names, self.datastack.ROI_maps = self.Canalysis.cals_roi_maps(self.datastack.fullSpectrum, 
                                                                                         self.datastack.elementstofit, 
                                                                                         self.datastack.engcal_slope, 
                                                                                         self.datastack.engcal_offset, 
                                                                                         scatterpeak,
                                                                                         verbose = False)
        
        
        sortmaps = 0
        if sortmaps:
            #Sort the maps to show the highest count map first
            nmaps = len(self.datastack.ROI_names)
            counts = np.zeros((nmaps), dtype=np.int64)
            for i in range(nmaps):
                counts[i] = np.sum(self.datastack.ROI_maps[i,:,:,:])
    
    
            sort_indices = np.argsort(counts)
            self.datastack.ROI_maps = self.datastack.ROI_maps[sort_indices[::-1],:,:,:]
            self.datastack.ROI_names = np.array(self.datastack.ROI_names)
            self.datastack.ROI_names = self.datastack.ROI_names[sort_indices[::-1]]
            self.datastack.ROI_names = self.datastack.ROI_names.tolist()
        
        self.datastack.ROI_maps_calculated = 1
        self.datastack.ROI_maps_loaded = 1
        
        self.window().page1.showmaps = 1
        self.window().page1.rb_roi.setEnabled(True)
        self.window().page1.rb_roi.setChecked(True)
        self.window().page1.UpdateDisplay()
        
        self.window().tabs.setCurrentIndex(4)
        
        QtGui.QApplication.restoreOverrideCursor()
        

#----------------------------------------------------------------------        
    def OnCalcROIs4D(self):
        
        if self.datastack.stack_loaded == 0:
            return

        QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))    
        
        self.datastack.ROI_data = []
        self.datastack.ROI_info = []
        self.datastack.ROI_names = []
        self.datastack.ROI_maps = []
        
        if self.scattroi:
            self.scattmin = float(self.ntc_scattmin.text())
            self.scattmax = float(self.ntc_scattmax.text())
            scatterpeak = [self.scattmin, self.scattmax]
        else:
            scatterpeak = []
            

        self.datastack.ROI_maps4D = []

        
        for i in range(len(self.datastack.theta)):
            self.datastack.ROI_info, self.datastack.ROI_names, \
            thisROI_maps = self.Canalysis.cals_roi_maps(self.datastack.fullSpectrum4D[:,:,:,:,i], 
                                                        self.datastack.elementstofit, 
                                                        self.datastack.engcal_slope, 
                                                        self.datastack.engcal_offset, 
                                                        scatterpeak,
                                                        verbose = False)
            self.datastack.ROI_maps4D.append(thisROI_maps)
            
            
        sortmaps = 0
        if sortmaps:
            #Sort the maps to show the highest count map first
            nmaps = len(self.datastack.ROI_names)
            counts = np.zeros((nmaps), dtype=np.int64)
            for i in range(nmaps):
                counts[i] = np.sum(self.datastack.ROI_maps[i,:,:,:])
    
    
            sort_indices = np.argsort(counts)
            self.datastack.ROI_maps = self.datastack.ROI_maps[sort_indices[::-1],:,:,:]
            self.datastack.ROI_names = np.array(self.datastack.ROI_names)
            self.datastack.ROI_names = self.datastack.ROI_names[sort_indices[::-1]]
            self.datastack.ROI_names = self.datastack.ROI_names.tolist()
            
            
        self.datastack.ROI_maps = self.datastack.ROI_maps4D[0]
        
        self.datastack.ROI_maps_calculated = 1
        self.datastack.ROI_maps4D_calculated = 1
        self.datastack.ROI_maps_loaded = 1
        
        self.window().page1.showmaps = 1
        self.window().page1.rb_roi.setEnabled(True)
        self.window().page1.rb_roi.setChecked(True)
        self.window().page1.UpdateDisplay()
        
        if havexrftomo:
            self.window().page6.UpdateDisplay()
        
        self.window().tabs.setCurrentIndex(4)
        
        QtGui.QApplication.restoreOverrideCursor()
        
#----------------------------------------------------------------------        
    def OnSaveROIs(self):

        #Save images      
        wildcard = "Portable Network Graphics (*.png);;Adobe PDF Files (*.pdf);; SVG (*.svg)"

        fileName = QtGui.QFileDialog.getSaveFileName(self, 'Save Plot', '', wildcard)

        fileName = str(fileName)
        if fileName == '':
            return
        
        path, ext = os.path.splitext(fileName) 
        ext = ext[1:].lower() 
        
       
        if ext != 'png' and ext != 'pdf' and ext != 'svg': 
            error_message = ( 
                  'Only the PNG, PDF and SVG image formats are supported.\n' 
                 'A file extension of `png\' or `pdf\' must be used.') 

            QtGui.QMessageBox.warning(self, 'Error', 'Could not save file: %s' % error_message)
            return 
   
   
                          
        try: 

            matplotlib.rcParams['pdf.fonttype'] = 42
            
            fig = self.specfig
            fig.savefig(fileName)

            
        except IOError, e:
            if e.strerror:
                err = e.strerror 
            else: 
                err = e 
   
            
            QtGui.QMessageBox.warning(self, 'Error', 'Could not save file: %s' % err)
            
            
#----------------------------------------------------------------------        
    def OnSaveSpectrum(self):
        
        if self.datastack.stack_loaded == 0:
            return

        

        #try: 
        if True:
            wildcard = "CSV files (*.csv)"

            filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save spectrum as .csv (.csv)', '', wildcard)

            filepath = str(filepath)
            if filepath == '':
                return
        
                            
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))                   
            
            spectrum = np.sum(self.datastack.stack, axis=(0,1))
            if len(self.datastack.fullSpectrum.shape) > 3:
                spectrumd1 = np.sum(self.datastack.fullSpectrum[:,:,0,:], axis=(0,1))
                spectrumd2 = np.sum(self.datastack.fullSpectrum[:,:,1,:], axis=(0,1))
                spectrumd3 = np.sum(self.datastack.fullSpectrum[:,:,2,:], axis=(0,1))
                spectrumd4 = np.sum(self.datastack.fullSpectrum[:,:,3,:], axis=(0,1))            
                        
            f = open(filepath, 'w')
            
            if np.unique(self.datastack.ecurrent).size == 1:
            
                if len(self.datastack.fullSpectrum.shape) > 3:
                    print>>f, 'CHAN, DET SUM, DET1, DET2, DET3, DET4' 
                    for i in range(spectrum.size):
                        print>>f, '%.6f, %.6f, %.6f, %.6f, %.6f, %.6f' %(i, spectrum[i],spectrumd1[i],spectrumd2[i],spectrumd3[i],spectrumd4[i])                
                else:
                    for i in range(spectrum.size):
                        print>>f, '%.6f, %.6f' %(i, spectrum[i])
                        
            else:
                
                ecmean = np.mean(self.datastack.ecurrent)
                #Save normalized spectra as well
                nspectrum = spectrum*ecmean

                
                if len(self.datastack.fullSpectrum.shape) > 3:
                    nspectrumd1 = spectrumd1*ecmean
                    nspectrumd2 = spectrumd2*ecmean
                    nspectrumd3 = spectrumd3*ecmean
                    nspectrumd4 = spectrumd4*ecmean
                    
                    print>>f, 'CHAN, DET SUM, DET1, DET2, DET3, DET4, NORM DET SUM, NORM DET1, NORM DET2, NORM DET3, NORM DET4' 
                    for i in range(spectrum.size):
                        print>>f, '%.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f' %(i, spectrum[i],spectrumd1[i],spectrumd2[i],spectrumd3[i],spectrumd4[i],
                                                                                                       nspectrum[i],nspectrumd1[i],nspectrumd2[i],nspectrumd3[i],nspectrumd4[i])                
                else:
                    for i in range(spectrum.size):
                        print>>f, '%.6f, %.6f,  %.6f' %(i, spectrum[i], nspectrum[i])
                                
            f.close()            
            
            QtGui.QApplication.restoreOverrideCursor()    

#         except:
#  
#             QtGui.QApplication.restoreOverrideCursor()
#             QtGui.QMessageBox.warning(self, 'Error', "Could not save .csv file.")
                   
       
        
        
#----------------------------------------------------------------------        
    def ShowImage(self):
        
        
        if self.datastack.stack_loaded == 0:
            return
                
        spectrum = np.sum(self.datastack.stack, axis=(0,1))
        
        
        maxchanel = (self.datastack.inenergy+1000-self.datastack.engcal_offset)/self.datastack.engcal_slope
        

        fig = self.specfig
        fig.clf()
        fig.add_axes((0.15,0.15,0.75,0.75))
        axes = fig.gca()
        
         
        axes.set_xlabel('Photon Energy Channel')
        axes.set_ylabel('Counts')
        
        if self.window().page0.show_log == 1:
            axes.set_yscale('log')
        
        specplot = axes.plot(spectrum)

        
        axes.set_xlim([0,maxchanel])
         
        #Show selected element lines
        for i in range(len(self.datastack.elementstofit)):
            indx = self.Canalysis.roi_lines.index(self.datastack.elementstofit[i])
            center = (self.Canalysis.roi_center[indx]-self.datastack.engcal_offset)/self.datastack.engcal_slope
            calib = self.Canalysis.roi_calib[indx]   
            if calib == 1:
                colr = 'g'
            elif calib == 2:
                colr = 'b'
            elif calib == 3:
                colr = 'r'
                
            (lr, rr) = self.Canalysis.roi_posinfo[indx]
            left_roi = (lr-self.datastack.engcal_offset)/self.datastack.engcal_slope
            right_roi = (rr-self.datastack.engcal_offset)/self.datastack.engcal_slope
            #axes.axvline(x=center, color = colr, alpha=0.5)
            axes.axvspan(left_roi, right_roi, color = colr, alpha=0.5)
            
        if self.scattroi:
            smin = int((self.scattmin-self.datastack.engcal_offset)/self.datastack.engcal_slope)
            smax = int((self.scattmax-self.datastack.engcal_offset)/self.datastack.engcal_slope)
            
            ys = spectrum[smin:smax]
            xs = range(smin,smin+np.size(ys))
            sumscatt = np.sum(ys)
            self.tc_scattsum.setText("Sum: {0}".format(sumscatt))
            
            axes.fill_between(xs,np.min(spectrum), ys, facecolor='blue', alpha=0.5)
            
         
        self.SpecPanel.draw()
        

#----------------------------------------------------------------------        
    def UpdateDisplay(self):
                                                                        
        if self.datastack.stack_loaded == 1:
            
            self.scattmin = float(self.datastack.inenergy) - 1000
            self.scattmax = float(self.datastack.inenergy) + 1000
            self.ntc_scattmin.setText(str(self.scattmin))
            self.ntc_scattmax.setText(str(self.scattmax))
            self.button_saverois.setEnabled(True)
            
            if havexrftomo:
                if self.datastack.tomodata == 1:
                    self.button_calc_4D.setEnabled(True)
                else:
                    self.button_calc_4D.setEnabled(False)
            
        
        self.ShowImage()

        
#---------------------------------------------------------------------- 
class PlotFrame(QtGui.QDialog):

    def __init__(self, parent, datax, datay, showdet, title = " "):    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        
        self.title = title

        self.datax = datax
        self.datay = datay
        
        self.showdet = showdet
        
        self.resize(630, 500)
        self.setWindowTitle(title)
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal) 
                
        

        vbox = QtGui.QVBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.plotfig = Figure((6.0, 4.2))
        self.PlotPanel = FigureCanvas(self.plotfig)
        
        self.PlotPanel.setParent(self)
        self.PlotPanel.setFocusPolicy(Qt.StrongFocus)
        self.PlotPanel.setFocus()        
        self.mpl_toolbar = NavigationToolbar(self.PlotPanel, self)

        #self.PlotPanel.mpl_connect('key_press_event', self.on_key_press)
        
        fbox.addWidget(self.PlotPanel)
        frame.setLayout(fbox)
        vbox.addWidget(frame)
        
        vbox.addWidget(self.mpl_toolbar)
        

        hbox = QtGui.QHBoxLayout()
        
        button_save = QtGui.QPushButton('Save Spectrum')
        button_save.clicked.connect(self.OnSave)
        hbox.addWidget(button_save)        
                
        button_close = QtGui.QPushButton('Close')
        button_close.clicked.connect(self.close)
        hbox.addWidget(button_close)
        
        vbox.addLayout(hbox)

        self.setLayout(vbox)

        
        self.draw_plot(datax,datay)
        
      
#----------------------------------------------------------------------        
    def draw_plot(self, datax, datay):
        
        
        fig = self.plotfig
        fig.clf()
        fig.add_axes((0.15,0.15,0.75,0.75))
        axes = fig.gca()
        
        plot = axes.plot(datax,np.zeros((datax.shape)), color="w", linewidth=0,
                         label = self.title)        
        
        if len(datay.shape) == 1:
            plot = axes.plot(datax,datay,label='Detector Sum')
        else:
            ndet = datay.shape[0]
            for i in range(ndet):
                if self.showdet[i] == 1:
                    plot = axes.plot(datax,datay[i,:],label='Detector '+str(i+1))
                

            
        legend = axes.legend(loc='upper right', shadow=True, prop={'size':8})    
            
            
        axes.set_xlabel('Channel number')
        axes.set_ylabel('Counts')
        
        
        self.PlotPanel.draw()
        
#----------------------------------------------------------------------
    def OnSave(self, event):


        try: 
            wildcard = "CSV files (*.csv)"

            filepath = QtGui.QFileDialog.getSaveFileName(self, 'Save plot as .csv (.csv)', '', wildcard)

            filepath = str(filepath)
            if filepath == '':
                return
        
                            
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))                 
            self.Save(filepath)    
            QtGui.QApplication.restoreOverrideCursor()    

        except:
 
            QtGui.QApplication.restoreOverrideCursor()
            QtGui.QMessageBox.warning(self, 'Error', "Could not save .csv file.")
                   
        self.close()

        
        return

#----------------------------------------------------------------------
    def Save(self, filename):
            
        f = open(filename, 'w')
        print>>f, '*********************  X-ray Absorption Data  ********************'
        print>>f, '*'
        print>>f, '* Formula: '
        print>>f, '* Common name: ', self.title
        print>>f, '* Edge: '
        print>>f, '* Acquisition mode: '
        print>>f, '* Source and purity: ' 
        print>>f, '* Comments: Stack list ROI ""'
        print>>f, '* Delta eV: '
        print>>f, '* Min eV: '
        print>>f, '* Max eV: '
        print>>f, '* Y axis: '
        print>>f, '* Contact person: '
        print>>f, '* Write date: '
        print>>f, '* Journal: '
        print>>f, '* Authors: '
        print>>f, '* Title: '
        print>>f, '* Volume: '
        print>>f, '* Issue number: '
        print>>f, '* Year: '
        print>>f, '* Pages: '
        print>>f, '* Booktitle: '
        print>>f, '* Editors: '
        print>>f, '* Publisher: '
        print>>f, '* Address: '
        print>>f, '*--------------------------------------------------------------'
        dim = self.datax.shape
        n=dim[0]
        for ie in range(n):
            if len(self.datay.shape) == 0:
                print>>f, '%.6f, %.6f' %(self.datax[ie], self.datay[ie])
            else:
                print>>f, '%.6f, %.6f, %.6f, %.6f, %.6f' %(self.datax[ie], self.datay[0,ie], self.datay[1,ie], self.datay[2,ie], self.datay[3,ie])
        
        f.close()
    
        
        

""" ------------------------------------------------------------------------------------------------"""
class PageMaps(QtGui.QWidget):
    def __init__(self, datastack):
        super(PageMaps, self).__init__()

        self.initUI(datastack)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack): 
        
        self.datastack = datastack
        
        self.nimgs = 6
        self.iimgs = 0
        self.itheta = 0
        self.showdet = [1,1,1,1]
        
        #Show maps 0-no maps; 1-ROI from file; 2-ROI; 3-fits;
        self.showmaps = 0
        
        self.ecurrnorm = 0
        
        self.show_scale_bar = 0
        self.show_scale = 1
        self.rgbct = 0
        
        self.vmin = 0.0
        self.vmax = 1.0
        
        self.usemapscalemm = 0
        self.scalemin = 0
        self.scalemax = 1000
        
        self.mapscale = 1
        
        
        self.xlabel = 'X Distance [um]'
        self.ylabel = 'Y Distance [um]'

        #panel 1
        sizer1= QtGui.QGroupBox('Display')
        hbox1 = QtGui.QHBoxLayout() 
        vbox1 = QtGui.QVBoxLayout()
        

        
        hbox11 = QtGui.QHBoxLayout()
        text = QtGui.QLabel(self) 
        text.setText('Max Number of Images ')
        hbox11.addWidget(text)
 
        self.nimgslist = ['1','2','3','4','6','9']
        
        self.cb_nimgs = QtGui.QComboBox(self)
        self.cb_nimgs.addItems(self.nimgslist)
        self.cb_nimgs.activated[int].connect(self.OnChangeNImgs) 

        self.cb_nimgs.setCurrentIndex(4)
        hbox11.addWidget(self.cb_nimgs)
        hbox11.addStretch(1)       
        
        hbox12 = QtGui.QHBoxLayout()
        text = QtGui.QLabel(self) 
        text.setText('Detector ')
        hbox12.addWidget(text)
 
#        self.detlist = ['1','2','3','4','All']       
#         self.cb_det = QtGui.QComboBox(self)
#         self.cb_det.addItems(self.detlist)
#         self.cb_det.activated[int].connect(self.OnChangeShowDet) 
#         self.cb_det.setCurrentIndex(self.showdet)
#         hbox12.addWidget(self.cb_det)
        
        self.cb_det1 = QtGui.QCheckBox('1', self)
        self.cb_det1.setChecked(self.showdet[0])
        self.cb_det1.stateChanged.connect(self.OnChangeShowDet)
        hbox12.addWidget(self.cb_det1)

        self.cb_det2 = QtGui.QCheckBox('2', self)
        self.cb_det2.setChecked(self.showdet[1])
        self.cb_det2.stateChanged.connect(self.OnChangeShowDet)
        hbox12.addWidget(self.cb_det2)
        
        self.cb_det3 = QtGui.QCheckBox('3', self)
        self.cb_det3.setChecked(self.showdet[2])
        self.cb_det3.stateChanged.connect(self.OnChangeShowDet)
        hbox12.addWidget(self.cb_det3)
        
        self.cb_det4 = QtGui.QCheckBox('4', self)
        self.cb_det4.setChecked(self.showdet[3])
        self.cb_det4.stateChanged.connect(self.OnChangeShowDet)
        hbox12.addWidget(self.cb_det4)
        hbox12.addStretch(1)
        
        self.button_save = QtGui.QPushButton('Save Element MAPS')
        self.button_save.clicked.connect( self.OnSaveImgs)   
        self.button_save.setMaximumWidth(120)
         
        vbox1.addLayout(hbox11)
        vbox1.addLayout(hbox12)
        vbox1.addWidget(self.button_save)      
        hbox1.addLayout(vbox1)
       
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken) 
        hbox1.addWidget(line) 
        
        
        vbox15 = QtGui.QVBoxLayout()

        self.cb_scalebar = QtGui.QCheckBox('Scalebar', self)
        self.cb_scalebar.setChecked(self.show_scale_bar)
        self.cb_scalebar.stateChanged.connect(self.OnCBScaleBar)
        vbox15.addWidget(self.cb_scalebar) 
        
        self.cb_scale = QtGui.QCheckBox('Show Scale', self)
        self.cb_scale.setChecked(self.show_scale)
        self.cb_scale.stateChanged.connect(self.OnCBShowScale)
        vbox15.addWidget(self.cb_scale) 
        
        self.cb_rgbct = QtGui.QCheckBox('RGB Color Table', self)
        self.cb_rgbct.setChecked(self.rgbct)
        self.cb_rgbct.stateChanged.connect(self.OnCBRGBColorTable)
        vbox15.addWidget(self.cb_rgbct) 
        
        vbox15.addStretch(1)      
        hbox1.addLayout(vbox15)
        
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken)                 
        hbox1.addWidget(line) 
        
        
    
        vbox17 = QtGui.QVBoxLayout()
        fgs17 = QtGui.QGridLayout()
         
        tc_bright = QtGui.QLabel(self)
        tc_bright.setText('Min ')
         
        tc_contr = QtGui.QLabel(self)
        tc_contr.setText('Max ')
 
        self.slider_vmin = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.slider_vmin.setRange(0,50)
        self.slider_vmin.setValue(int(self.vmin*100))
        self.slider_vmin.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_vmin.valueChanged[int].connect(self.OnScrollVmin)
        self.slider_vmin.setMinimumWidth(60)

                 
        self.slider_vmax = QtGui.QSlider(QtCore.Qt.Horizontal, self)      
        self.slider_vmax.setRange(51,100)
        self.slider_vmax.setValue(int(self.vmax*100))
        self.slider_vmax.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_vmax.valueChanged[int].connect(self.OnScrollVmax)  
        self.slider_vmax.setMinimumWidth(60)
                
        fgs17.addWidget(tc_bright, 0, 0)
        fgs17.addWidget(self.slider_vmin, 0, 1)
        fgs17.addWidget(tc_contr, 1, 0) 
        fgs17.addWidget(self.slider_vmax, 1, 1)
      
        vbox17.addLayout(fgs17)
        hbox1.addLayout(vbox17)  
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken)                 
        hbox1.addWidget(line) 
        
        self.cb_scalemm = QtGui.QCheckBox('Use Min/Max Scale', self)
        self.cb_scalemm.setChecked(self.usemapscalemm)
        self.cb_scalemm.stateChanged.connect(self.OnCBScaleMinMax)
        
        
        vbox18 = QtGui.QVBoxLayout()
        fgs18 = QtGui.QGridLayout()
         
        tc_min = QtGui.QLabel(self)
        tc_min.setText('Min ')
         
        tc_max = QtGui.QLabel(self)
        tc_max.setText('Max ')
 

        self.ntc_min = QtGui.QLineEdit(self)
        self.ntc_min.setFixedWidth(65)
        self.ntc_min.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_min.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_min.setText(str(self.scalemin))
        self.ntc_min.textChanged.connect(self.OnNCBScaleMinMax)

        self.ntc_max = QtGui.QLineEdit(self)
        self.ntc_max.setFixedWidth(65)
        self.ntc_max.setValidator(QtGui.QDoubleValidator(0, 99999, 2, self))
        self.ntc_max.setAlignment(QtCore.Qt.AlignRight)         
        self.ntc_max.setText(str(self.scalemax))  
        self.ntc_max.textChanged.connect(self.OnNCBScaleMinMax)      
                
        
        fgs18.addWidget(tc_min, 0, 0)
        fgs18.addWidget(self.ntc_min, 0, 1)
        fgs18.addWidget(tc_max, 1, 0) 
        fgs18.addWidget(self.ntc_max, 1, 1)
      
        vbox18.addWidget(self.cb_scalemm)
        vbox18.addLayout(fgs18)
        hbox1.addLayout(vbox18)                 
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken)                 
        hbox1.addWidget(line) 
 
        vbox12 = QtGui.QVBoxLayout()
        self.button_rgbmap = QtGui.QPushButton('   Composite Map   ')
        self.button_rgbmap.clicked.connect( self.OnCompositeRGB)   
        self.button_rgbmap.setEnabled(True)
        vbox12.addWidget(self.button_rgbmap)  
        
        self.button_scatterplot = QtGui.QPushButton('Scatter Plots')
        self.button_scatterplot.clicked.connect( self.OnScatterplot)   
        self.button_scatterplot.setEnabled(True)
        vbox12.addWidget(self.button_scatterplot)  
        
        hbox1.addLayout(vbox12)
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken) 
        hbox1.addWidget(line) 
        
        vbox13 = QtGui.QVBoxLayout()
        
        
        self.rb_roi = QtGui.QRadioButton('ROI')
        self.rb_fit = QtGui.QRadioButton('Fit')
        self.rb_roi.setEnabled(True)
        self.rb_fit.setEnabled(False)
        self.rb_roi.setChecked(True)
        self.rb_fit.toggled.connect(self.OnROIFitMaps)
        
        vbox13.addWidget(self.rb_roi)
        vbox13.addWidget(self.rb_fit)
                         
        hbox1.addLayout(vbox13)
        
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setFrameShadow(QtGui.QFrame.Sunken) 
        hbox1.addWidget(line) 
        
        vbox14 = QtGui.QVBoxLayout()
        
        
        self.cb_ecnorm = QtGui.QCheckBox('Normalize e-current to 100mA', self)
        self.cb_ecnorm.setChecked(self.ecurrnorm)
        self.cb_ecnorm.stateChanged.connect(self.OnCBECNormalize)
        self.cb_ecnorm.setEnabled(False)
        
        self.tc_ecmean = QtGui.QLabel(self) 
        self.tc_ecmean.setText(' ')
        vbox14.addWidget(self.cb_ecnorm) 
        vbox14.addWidget(self.tc_ecmean)   
        
        hbox14 = QtGui.QHBoxLayout()
        self.button_scale = QtGui.QPushButton('Scale MAPS')
        self.button_scale.clicked.connect( self.OnScaleMaps)   
        hbox14.addWidget(self.button_scale)
        self.ntc_mscale = QtGui.QDoubleSpinBox(self)  
        self.ntc_mscale.setFixedWidth(85)
        self.ntc_mscale.setRange(0, 99999)    
        self.ntc_mscale.setAlignment(QtCore.Qt.AlignRight)
        self.ntc_mscale.setValue(self.mapscale)
        hbox14.addWidget(self.ntc_mscale) 
        hbox14.addStretch(1)     
        vbox14.addLayout(hbox14)      
        
        vbox14.addStretch(1)
        hbox1.addLayout(vbox14)
        
        
        hbox1.setContentsMargins(10,10,10,10)
        sizer1.setLayout(hbox1)
        hbox1.addStretch(1)
        


        
        #panel 2     
 
        vbox2 = QtGui.QVBoxLayout()
        
        self.tc_maps = QtGui.QLabel(self)
        self.tc_maps.setText("Element Maps")
        vbox2.addWidget(self.tc_maps)
        

        hbox21 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.mapsimgfig = Figure((PlotH, 5*PlotH))
        self.MapsImagePanel = FigureCanvas(self.mapsimgfig)
        self.MapsImagePanel.setParent(self)
        self.MapsImagePanel.mpl_connect('button_press_event', self.OnClickMap)
        self.MapsImagePanel.mpl_connect('motion_notify_event', self.OnMapMotion)
        self.MapsImagePanel.mpl_connect('pick_event', self.OnPick)
        
        fbox.addWidget(self.MapsImagePanel)
        frame.setLayout(fbox)
        hbox21.addWidget(frame)  
        
        self.slider_imgs = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.slider_imgs.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_imgs.valueChanged[int].connect(self.OnScrollImgs)
        self.slider_imgs.setRange(0, 10)
        
        hbox21.addWidget(self.slider_imgs)     
        
        self.slider_theta = QtGui.QScrollBar(QtCore.Qt.Horizontal)
        self.slider_theta.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slider_theta.valueChanged[int].connect(self.OnScrollTheta)
        self.slider_theta.setRange(0, 10)    
        self.slider_theta.setMinimumWidth(200) 
        self.slider_theta.setVisible(False)  
        self.tc_imagetheta = QtGui.QLabel(self)
        self.tc_imagetheta.setText("4D Data Angle: ")
        self.tc_imagetheta.setVisible(False)
        hbox22 = QtGui.QHBoxLayout()
        hbox22.addWidget(self.tc_imagetheta) 
        hbox22.addWidget(self.slider_theta)
        hbox22.addStretch(1)
             

        vbox2.addLayout(hbox21)
        vbox2.addLayout(hbox22)
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        
        vboxtop.addStretch (0.5)
        vboxtop.addWidget(sizer1)
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(vbox2)
        vboxtop.addStretch (0.5)
        #vboxtop.addWidget(sizer4)
        #vboxtop.addStretch (0.5)

        vboxtop.setContentsMargins(50,50,50,25)
        self.setLayout(vboxtop)
        
        

#----------------------------------------------------------------------           
    def OnChangeNImgs(self, value):
        item = value
        self.nimgs = int(self.nimgslist[int(item)])

        if self.showmaps == 1:
            sliderrange = np.ceil((self.datastack.ROI_maps.shape[0]-1)/self.nimgs)        
        elif self.showmaps == 2:
            sliderrange = np.ceil((self.datastack.fit_maps.shape[0]-1)/self.nimgs)

            
        self.slider_imgs.setRange(0, sliderrange)
        self.slider_imgs.setValue(0)
        self.iimgs = 0
        
        self.ShowImage()
        
#----------------------------------------------------------------------           
    def OnChangeShowDet(self, state):
        
        
        if self.cb_det1.isChecked():
            self.showdet[0] = 1
        else: self.showdet[0] = 0
        
        if self.cb_det2.isChecked():
            self.showdet[1] = 1
        else: self.showdet[1] = 0
        
        if self.cb_det3.isChecked():
            self.showdet[2] = 1
        else: self.showdet[2] = 0
        
        if self.cb_det4.isChecked():
            self.showdet[3] = 1
        else: self.showdet[3] = 0        
        
        self.ShowImage()
        
#----------------------------------------------------------------------           
    def OnCBECNormalize(self, state):
        
        
        if self.cb_ecnorm.isChecked():
            self.ecurrnorm = 1
        else: self.ecurrnorm = 0
        
        self.ShowImage()
        
        
#----------------------------------------------------------------------           
    def OnCBScaleMinMax(self, state):
        
        
        if self.cb_scalemm.isChecked():
            self.usemapscalemm = 1
        else: self.usemapscalemm = 0
        
        
        self.ShowImage()
        
#----------------------------------------------------------------------           
    def OnNCBScaleMinMax(self, state):
        
        
        if self.usemapscalemm == 0:
            return
        
        
        self.ShowImage()
            
#----------------------------------------------------------------------            
    def OnScrollImgs(self, value):
        
        self.iimgs = value

        self.ShowImage()
        
        
#----------------------------------------------------------------------            
    def OnScrollTheta(self, value):
        
        self.itheta = value
        
        if self.datastack.ROI_maps4D_calculated == 1:
            self.datastack.ROI_maps = self.datastack.ROI_maps4D[self.itheta]

        
        if self.datastack.fits4D_calculated == 1:
            self.datastack.fit_maps = self.datastack.fit_maps4D[self.itheta]
            
        self.datastack.ecurrent = self.datastack.ecurrent4D[:,:,self.itheta]            
        self.tc_imagetheta.setText("4D Data Angle: {0:.2f}".format(self.datastack.theta[self.itheta]))
            
        self.ShowImage()
        
    
#----------------------------------------------------------------------
    def OnCompositeRGB(self, event):
        
        if self.datastack.ROI_maps_loaded == 0:   
            return 

        compimgwin = ShowCompositeRBGmap(self.window(), self.datastack, 
                                         self.showmaps, self.showdet, 
                                         self.ecurrnorm)
        compimgwin.show()
        
#----------------------------------------------------------------------
    def OnScatterplot(self, event):
        
        if self.datastack.ROI_maps_loaded == 0: 
            return 

        sctwin = Scatterplots(self.window(), self.datastack, self.showmaps)
        sctwin.show()
        
#----------------------------------------------------------------------          
    def OnROIFitMaps(self, enabled):
        
        if self.rb_roi.isChecked():
            self.showmaps = 1
        if self.rb_fit.isChecked():
            self.showmaps = 2

        self.UpdateDisplay()
        

#----------------------------------------------------------------------
    def OnSaveImgs(self, event):

        savewin = SaveWinP1(self.window(), self.datastack)
        savewin.show()
    
#----------------------------------------------------------------------           
    def OnCBScaleBar(self, state):
        
        
        if self.cb_scalebar.isChecked():
            self.show_scale_bar = 1
        else: self.show_scale_bar = 0
        
        self.ShowImage()
        
#----------------------------------------------------------------------           
    def OnCBShowScale(self, state):
        
        
        if self.cb_scale.isChecked():
            self.show_scale = 1
        else: self.show_scale = 0
        
        self.ShowImage()
        
#----------------------------------------------------------------------           
    def OnCBRGBColorTable(self, state):
        
        
        if self.cb_rgbct.isChecked():
            self.rgbct = 1
        else: self.rgbct = 0
        
        self.ShowImage()
        
#----------------------------------------------------------------------  
    def OnClickMap(self, evt):

        
        x = evt.xdata
        y = evt.ydata
        
        if (x == None) or (y == None):
            return
        x = int(x+0.5)
        y = int(y+0.5)
        
        title = 'Point ['+str(x)+ ', '+ str(y) + ']'
        
                
        if len(self.datastack.fullSpectrum.shape) > 3:
            nengchan = self.datastack.fullSpectrum.shape[3]
            channel = np.arange(nengchan)+1
            
            spectra = self.datastack.fullSpectrum[x,y,:,:]      
            
            if self.ecurrnorm == 1:
                spectra = spectra * self.datastack.ecurrent[x,y]     
            
            plot = PlotFrame(self, channel, spectra, self.showdet, title=title)
            plot.show()
        else:
            nengchan = self.datastack.fullSpectrum.shape[2]
            channel = np.arange(nengchan)+1
            
            spectra = self.datastack.fullSpectrum[x,y,:]
            
            if self.ecurrnorm == 1:
                spectra = spectra * self.datastack.ecurrent[x,y]  
            
            plot = PlotFrame(self, channel, spectra, self.showdet, title=title)
            plot.show()            
        
        
#----------------------------------------------------------------------  
    def OnMapMotion(self, evt):

        
        x = evt.xdata
        y = evt.ydata

        
        if (x == None) or (y == None):
            self.tc_maps.setText("Element Maps")
            return
        
        x = int(x+0.5)
        y = int(y+0.5)
        
        self.tc_maps.setText('Element Maps    Point ['+str(x)+ ', '+ str(y) + ']')
  
#----------------------------------------------------------------------
    def OnScrollVmin(self, value):
        
        self.vmin = value/100.0
                                     
        self.ShowImage()
        
#----------------------------------------------------------------------
    def OnScrollVmax(self, value):
        
        self.vmax = value/100.0

        self.ShowImage()
        
#----------------------------------------------------------------------
    def OnScaleMaps(self, value):
        
        self.mapscale = float(self.ntc_mscale.text())

        if self.datastack.ROI_maps_calculated == 1:
            self.datastack.ROI_maps = self.datastack.ROI_maps * self.mapscale
            
        if self.datastack.fits_calculated == 1:      
            self.datastack.fit_maps = self.datastack.fit_maps * self.mapscale

        self.ShowImage()
                 
#----------------------------------------------------------------------        
    def EnableDetectorView(self, enable): 
        
        if enable:
            self.cb_det1.setEnabled(True)
            self.cb_det2.setEnabled(True)
            self.cb_det3.setEnabled(True)
            self.cb_det4.setEnabled(True)
            
        else:
            self.cb_det1.setEnabled(False)
            self.cb_det2.setEnabled(False)
            self.cb_det3.setEnabled(False)
            self.cb_det4.setEnabled(False)
                        
#----------------------------------------------------------------------        
    def UpdateDisplay(self): 
        

        if self.showmaps == 1:
            n_maps = len(self.datastack.ROI_maps)
        elif self.showmaps == 2:
            n_maps = len(self.datastack.fitmap_names)
            
        if self.datastack.ROI_maps_loaded == 1 :
            sliderrange = np.ceil(n_maps/self.nimgs)
            self.slider_imgs.setRange(0, sliderrange)
            
        if len(self.datastack.realY) > 0:
            self.cb_scale.setChecked(self.show_scale)
            self.cb_scale.setEnabled(True)
        else:
            self.show_scale = 0
            self.cb_scale.setChecked(self.show_scale)
            self.cb_scale.setEnabled(False)
            
        if np.unique(self.datastack.ecurrent).size == 1:
            self.cb_ecnorm.setChecked(False)
            self.cb_ecnorm.setEnabled(False)
            self.ecurrnorm = 0
            self.tc_ecmean.setText(' ')
        else:
            self.cb_ecnorm.setEnabled(True)
            self.tc_ecmean.setText('Mean Iec = {0:.2f} mA'.format(self.datastack.ecmean))
            

        if self.datastack.ROI_maps4D_calculated == 1 :
            self.slider_theta.setVisible(True)  
            self.tc_imagetheta.setText("4D Data Angle: {0:.2f}".format(self.datastack.theta[self.itheta]))
            self.tc_imagetheta.setVisible(True)   
            self.slider_theta.setRange(0, len(self.datastack.theta)-1)       
        else:           
            self.slider_theta.setVisible(False)  
            self.tc_imagetheta.setVisible(False)
            

        
        self.ShowImage()
        
#----------------------------------------------------------------------        
    def NewColorTables(self):
        
        colorts = []
        
        cdict1 = {'red':   ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),
                
                 'green': ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)),
                
                'blue':  ((0.0, 0.0, 0.0),
                          (1.0, 0.0, 0.0))
                        }
        ct1 = LinearSegmentedColormap('CT1', cdict1)
        matplotlib.pyplot.register_cmap(cmap=ct1)
        
        colorts.append(ct1)
        
        cdict2 = {'red': ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)),
                  
                  'green':   ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),

                'blue':  ((0.0, 0.0, 0.0),
                          (1.0, 0.0, 0.0))
                        }
        ct2 = LinearSegmentedColormap('CT2', cdict2)
        matplotlib.pyplot.register_cmap(cmap=ct2)
        
        colorts.append(ct2)
        
        cdict3 = {'red': ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)),
                  
                  'green':  ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)) ,

                'blue':  ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0))
                        }
        ct3 = LinearSegmentedColormap('CT3', cdict3)
        matplotlib.pyplot.register_cmap(cmap=ct3)
        
        colorts.append(ct3)
        
        cdict4 = {'red': ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),
                  
                  'green':  ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)) ,

                'blue':  ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0))
                        }
        ct4 = LinearSegmentedColormap('CT4', cdict4)
        matplotlib.pyplot.register_cmap(cmap=ct4)
        
        colorts.append(ct4)
        
        cdict5 = {'red': ((0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)),
                  
                  'green':  ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),

                'blue':  ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0))
                        }
        ct5 = LinearSegmentedColormap('CT5', cdict5)
        matplotlib.pyplot.register_cmap(cmap=ct5)
        
        colorts.append(ct5)
        
        
        cdict6 = {'red': ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),
                  
                  'green':   ((0.0, 0.0, 0.0),
                            (0.5, 0.0, 0.1),
                            (1.0, 1.0, 1.0)),

                'blue':  ((0.0, 0.0, 0.0),
                          (1.0, 0.0, 0.0))
                        }
        ct6 = LinearSegmentedColormap('CT6', cdict6)
        matplotlib.pyplot.register_cmap(cmap=ct6)
        
        colorts.append(ct6)
        
        return colorts
        
#----------------------------------------------------------------------        
    def ShowImage(self):
        
        if self.datastack.ROI_maps_loaded == 0:
            fig = self.mapsimgfig
            fig.clf()       
            self.MapsImagePanel.draw()     
            return 
        
        self.normalizedmaps = []
       
        if self.nimgs == 1:
            sr = 1
            sc = 1
        elif self.nimgs == 2:
            sr = 1
            sc = 2
        elif self.nimgs == 3:
            sr = 1
            sc = 3
        elif self.nimgs == 4:
            sr = 2
            sc = 2
        elif self.nimgs == 6:
            sr = 2
            sc = 3
        else:
            sr = 3
            sc = 3
            
       
        fig = self.mapsimgfig
        fig.clf()
        
        
        if sum(self.showdet) == 0:
            self.MapsImagePanel.draw()
            return
        
        if self.showmaps == 1:
            n_maps = self.datastack.ROI_maps.shape[0]
        elif self.showmaps == 2:
            n_maps = self.datastack.fit_maps.shape[0]
            
                        
        for i in range(self.iimgs*self.nimgs,min(self.iimgs*self.nimgs+self.nimgs,n_maps)):
            if sum(self.showdet) == 4:
                if self.showmaps == 1:
                    image = np.sum(self.datastack.ROI_maps[i,:,:,:], axis=2)
                    title = self.datastack.ROI_names[i]
                elif self.showmaps == 2: 
                    if self.datastack.fit_maps.ndim == 4:
                        image = np.sum(self.datastack.fit_maps[i,:,:,:], axis=2)    
                    else:
                        image = self.datastack.fit_maps[i,:,:]
                    title = self.datastack.fitmap_names[i]   
            else:
                image = np.zeros((self.datastack.stack_ncols, self.datastack.stack_nrows), dtype=np.int64)    
                if self.showmaps == 1:
                    for idet in range(len(self.showdet)):
                        if self.showdet[idet] == 1:
                            image = image + self.datastack.ROI_maps[i,:,:,idet]                  
                    title = self.datastack.ROI_names[i]
                elif self.showmaps == 2: 
                    for idet in range(len(self.showdet)):
                        if self.showdet[idet] == 1:
                            image = image + self.datastack.fit_maps[i,:,:,idet]                  
                    title = self.datastack.fitmap_names[i]            
                                    
    
            subplotnum = i-self.iimgs*self.nimgs+1
    
            #fig.add_axes(((0.0,0.0,1.0,1.0)))
            fig.add_subplot(sr,sc,subplotnum)
            
            axes = fig.gca()
            fig.subplots_adjust(left=0.05, right=0.95, top = 0.95, bottom = 0.05)
            
            
            divider = make_axes_locatable(axes)
            ax_cb = divider.new_horizontal(size="3%", pad=0.03)  
            fig.add_axes(ax_cb)
            
            fig.patch.set_alpha(1.0)
            
            if self.ecurrnorm == 1:
                image = np.multiply(image,self.datastack.ecurrent)
            
            
            if self.vmin != 0.0 or self.vmax != 1.0:
                imgmax = np.amax(image)
                image = np.clip(image,imgmax*self.vmin, imgmax*self.vmax)
                        
                        
            if self.usemapscalemm == 0:
                if not self.rgbct:
                    im = axes.imshow(image.T, interpolation='none', origin='lower') 
                    axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)
                else:
    
                    colorts = self.NewColorTables()
                    ctnum = i % len(colorts)
                    im = axes.imshow(image.T, interpolation='none', origin='lower', cmap=colorts[ctnum]) 
                    axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)     
            else:
                ivmin = float(self.ntc_min.text())
                ivmax = float(self.ntc_max.text())
                if not self.rgbct:
                    im = axes.imshow(image.T, interpolation='none', origin='lower',vmin=ivmin,vmax=ivmax) 
                    axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)
                else:
    
                    colorts = self.NewColorTables()
                    ctnum = i % len(colorts)
                    im = axes.imshow(image.T, interpolation='none', origin='lower', cmap=colorts[ctnum],vmin=ivmin,vmax=ivmax) 
                    axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)                        
            
            if self.show_scale_bar == 1:
                #Show Scale Bar
                scale_bar_pixels_y = int(0.01*self.datastack.stack_ncols)
                
                if scale_bar_pixels_y < 1:
                    scale_bar_pixels_y = 1
                    
                startx = int(self.datastack.stack_nrows*0.05)
                if scale_bar_pixels_y > 1:
                    starty = int(self.datastack.stack_nrows*0.1)-scale_bar_pixels_y
                else:
                    starty = int(self.datastack.stack_nrows*0.1)-2
                um_string = ' $\mathrm{\mu m}$'
                microns = '$'+self.window().page0.scale_bar_string+' $'+um_string
                axes.text(self.window().page0.scale_bar_pixels_x+startx+1,starty+0.5, microns, horizontalalignment='left', verticalalignment='center',
                          color = 'black', fontsize=14)
                #Matplotlib has flipped scales so I'm using rows instead of cols!
                p = matplotlib.patches.Rectangle((startx,starty), self.window().page0.scale_bar_pixels_x, self.window().page0.scale_bar_pixels_y,
                                       color = 'black', fill = True)
                axes.add_patch(p)   
                
            
            if self.show_scale:
                labels = axes.get_xticks().tolist()
                newxscale = self.datastack.realY - self.datastack.realY[0]
                deltax = np.round(self.datastack.realY[1] - self.datastack.realY[0])
                
                newlabels = labels[:]
                for ilb in range(len(labels)):
                    if labels[ilb]>=0:
                        newlabels[ilb] = str(np.round(labels[ilb]*deltax))
 
                newlabels[0] = ''
                newlabels[-1] = ''
                axes.set_xticklabels(newlabels, fontsize=self.datastack.fontsize_labels)
                axes.set_xlabel(self.xlabel, fontsize=self.datastack.fontsize_labels, picker=True) 


                labels = axes.get_yticks().tolist()
                
                newyscale = self.datastack.realX - self.datastack.realX[0]
                deltay = np.round(self.datastack.realX[1] - self.datastack.realX[0])
                
                newlabels = labels[:]
                for ilb in range(len(labels)):
                    if labels[ilb]>=0:
                        newlabels[ilb] = str(np.round(labels[ilb]*deltay))

                newlabels[0] = ''
                newlabels[-1] = ''
                axes.set_yticklabels(newlabels, fontsize=self.datastack.fontsize_labels)
                axes.set_ylabel(self.ylabel, fontsize=self.datastack.fontsize_labels, picker=True)       
                
           
                fig.subplots_adjust(left=0.05, right=0.95, top = 0.95, bottom = 0.10, hspace=0.5, wspace=0.5) 
                

            else:           
                axes.axis("off")      
                
            axes.set_title(title, fontsize=self.datastack.fontsize_title, picker=True)
            
        


        self.MapsImagePanel.draw()

         
        #self.tc_imageeng.setText('Image at energy: {0:5.2f} eV'.format(float(self.stk.ev[self.iev])))
        
#----------------------------------------------------------------------  
    def OnPick(self, event):
        
        
        if isinstance(event.artist, Text):
            text = event.artist
            
            newtext = text.get_text()
            
            if newtext == self.xlabel:
                self.oldlabel = 'x'
                
            if newtext == self.ylabel:
                self.oldlabel = 'y'
                
            if self.showmaps == 1:
                if newtext in self.datastack.ROI_names:
                    self.oldlabel = self.datastack.ROI_names.index(newtext)
                
        tewin = TextForm(self, newtext)
        tewin.show()
        
        
        
#----------------------------------------------------------------------  
    def ChangeLabel(self, newlabel):
        
        if self.oldlabel == 'x':
            self.xlabel = newlabel
            
        elif self.oldlabel == 'y':
            self.ylabel = newlabel
            
        else:
            self.datastack.ROI_names[self.oldlabel] = newlabel
            
            
        self.ShowImage()
        

#---------------------------------------------------------------------- 
class SaveWinP1(QtGui.QDialog):

    def __init__(self, parent, datastack):    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        self.datastack = datastack
        filename = self.datastack.filename
        
        self.resize(400, 300)
        self.setWindowTitle('Save')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)

        
        path, ext = os.path.splitext(filename) 
        ext = ext[1:].lower()   
        suffix = "." + ext
        path, fn = os.path.split(filename)
        filename = fn[:-len(suffix)]
        
        self.path = path
        self.filename = filename
                          
        
        vboxtop = QtGui.QVBoxLayout()
        vboxtop.setContentsMargins(20,20,20,20)
        
        gridtop = QtGui.QGridLayout()
        gridtop.setVerticalSpacing(20)
    
        fontb = QtGui.QFont()
        fontb.setBold(True)            
        
        st1 = QtGui.QLabel(self)
        st1.setText('Save')
        st1.setFont(fontb)
        st2 = QtGui.QLabel(self)
        st2.setText('.pdf')
        st2.setFont(fontb)
        st3 = QtGui.QLabel(self)
        st3.setText('.png')
        st3.setFont(fontb)
        st4 = QtGui.QLabel(self)
        st4.setText('.svg')
        st4.setFont(fontb)
        st5 = QtGui.QLabel(self)
        st5.setText('ASCII')
        st5.setFont(fontb)    
          
   
        st7 = QtGui.QLabel(self)
        st7.setText('ROI Maps')
        
        self.cb21 = QtGui.QCheckBox('', self)
        self.cb21.setChecked(True)
        
        self.cb22 = QtGui.QCheckBox('', self)
        self.cb23 = QtGui.QCheckBox('', self)
        self.cb24 = QtGui.QCheckBox('', self)
        
        if self.datastack.ROI_maps == []:
            self.cb21.setEnabled(False)
            self.cb22.setEnabled(False)
            self.cb23.setEnabled(False)
            self.cb24.setEnabled(False)
        
        st8 = QtGui.QLabel(self)
        st8.setText('Fit Maps')   
        
        self.cb31 = QtGui.QCheckBox('', self)
        #self.cb31.setChecked(True)
        
        self.cb32 = QtGui.QCheckBox('', self)
        self.cb33 = QtGui.QCheckBox('', self)
        self.cb34 = QtGui.QCheckBox('', self)
        
        if self.datastack.fits_calculated == 0:
            self.cb31.setEnabled(False)
            self.cb32.setEnabled(False)
            self.cb33.setEnabled(False)
            self.cb34.setEnabled(False)


        gridtop.addWidget(st1, 0, 0)
        gridtop.addWidget(st2, 0, 1)
        gridtop.addWidget(st3, 0, 2)
        gridtop.addWidget(st4, 0, 3)
        gridtop.addWidget(st5, 0, 4)
        
        gridtop.addWidget(st7, 1, 0)
        gridtop.addWidget(self.cb21, 1, 1)
        gridtop.addWidget(self.cb22, 1, 2) 
        gridtop.addWidget(self.cb23, 1, 3)  
        gridtop.addWidget(self.cb24, 1, 4)  

                                
  

        gridtop.addWidget(st8, 3, 0)
        gridtop.addWidget(self.cb31, 3, 1)
        gridtop.addWidget(self.cb32, 3, 2) 
        gridtop.addWidget(self.cb33, 3, 3)  
        gridtop.addWidget(self.cb34, 3, 4)  
        
        
        vboxtop.addStretch(0.5)
        vboxtop.addLayout(gridtop)
        vboxtop.addStretch(1)
        
         
        hbox0 = QtGui.QHBoxLayout()
         
        stf = QtGui.QLabel(self)
        stf.setText('Filename:\t')
        self.tc_savefn = QtGui.QLineEdit(self)
        self.tc_savefn.setText(self.filename)

        hbox0.addWidget(stf)
        hbox0.addWidget(self.tc_savefn)         
                  
        hbox1 = QtGui.QHBoxLayout()
                 
        stp = QtGui.QLabel(self)
        stp.setText('Path:  \t')
        self.tc_savepath = QtGui.QLineEdit(self)
        self.tc_savepath.setReadOnly(True)
        self.tc_savepath.setText(self.path)
        self.tc_savepath.setMinimumWidth(100)
        hbox1.addWidget(stp)
        hbox1.addWidget(self.tc_savepath)  
         
        button_path = QtGui.QPushButton('Browse...')
        button_path.clicked.connect(self.OnBrowseDir)
        hbox1.addWidget(button_path)
         
         
        hbox2 = QtGui.QHBoxLayout()
        button_save = QtGui.QPushButton('Save')
        button_save.clicked.connect(self.OnSave)
        hbox2.addWidget(button_save)
         
        button_cancel = QtGui.QPushButton('Cancel')
        button_cancel.clicked.connect(self.close)
        hbox2.addWidget(button_cancel)
        
        vboxtop.addLayout(hbox0)
        vboxtop.addLayout(hbox1)
        vboxtop.addStretch(1.0)
        vboxtop.addLayout(hbox2)

        
        
        self.setLayout(vboxtop)
        
#----------------------------------------------------------------------        
    def OnBrowseDir(self, evt):
        
        directory = QtGui.QFileDialog.getExistingDirectory(self, "Choose a directory", self.path, QtGui.QFileDialog.ShowDirsOnly|QtGui.QFileDialog.ReadOnly)       
                                                        
        
       
        if directory == '':
            return
                 
        directory = str(directory)
                    
        self.path = directory
        
        self.tc_savepath.setText(self.path)
            
            
                
#----------------------------------------------------------------------        
    def OnSave(self, evt):
        
        filename = str(self.tc_savefn.text())

        roi_pdf = self.cb21.isChecked()
        roi_png = self.cb22.isChecked()
        roi_svg = self.cb23.isChecked()
        roi_acsii = self.cb24.isChecked()
        
        fit_pdf = self.cb31.isChecked()
        fit_png = self.cb32.isChecked()
        fit_svg = self.cb33.isChecked()
        fit_acsii = self.cb34.isChecked()
        

        

        SaveFileName = os.path.join(self.path,filename)
      
        
        #Save roi from file images
        try: 
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor)) 
            for isel in range(1,4):
    
                if (self.datastack.ROI_maps_calculated == 0) and (isel == 1):
                    continue
                if (self.datastack.fits_calculated == 0) and (isel == 2):
                    continue
                if isel == 1:
                    n_maps = self.datastack.ROI_maps.shape[0]
                elif isel == 2:
                    n_maps = len(self.datastack.fitmap_names)
                                
                for i in range(n_maps):
                    if sum(self.parent.page1.showdet) == 4:
                        if isel == 1:
                            image = np.sum(self.datastack.ROI_maps[i,:,:,:], axis=2)
                            title = self.datastack.ROI_names[i]
                        elif isel == 2: 
                            image = np.sum(self.datastack.fit_maps[i,:,:,:], axis=2)            
                            title = self.datastack.fitmap_names[i]   
                    else:
                        image = np.zeros((self.datastack.stack_ncols, self.datastack.stack_nrows))    
                        if isel == 1:
                            for idet in range(len(self.parent.page1.showdet)):
                                if self.parent.page1.showdet[idet] == 1:
                                    image = image + self.datastack.ROI_maps[i,:,:,self.parent.page1.showdet[idet]]                  
                            title = self.datastack.ROI_names[i]
                        elif isel == 2: 
                            for idet in range(len(self.parent.page1.showdet)):
                                if self.parent.page1.showdet[idet] == 1:
                                    image = image + self.datastack.fit_maps[i,:,:,self.parent.page1.showdet[idet]]                  
                            title = self.datastack.fitmap_names[i]            
            
                    fig = matplotlib.figure.Figure()
                    canvas = FigureCanvas(fig)
                    fig.add_axes((0.05,0.10,0.90,0.85))
                    axes = fig.gca()       
                    
                    divider = make_axes_locatable(axes)
                    ax_cb = divider.new_horizontal(size="3%", pad=0.03)  
                    fig.add_axes(ax_cb)
                    
                    fig.patch.set_alpha(1.0)
                                
                    if self.parent.page1.ecurrnorm == 1:
                        image = np.multiply(image,self.datastack.ecurrent)
                
                     
                    if self.parent.page1.vmin != 0.0 or self.parent.page1.vmax != 1.0:
                        imgmax = np.amax(image)
                        image = np.clip(image,imgmax*self.parent.page1.vmin, imgmax*self.parent.page1.vmax)
                                
                    if not self.parent.page1.rgbct:
                        im = axes.imshow(image.T, interpolation='none', origin='lower') 
                        axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)
                    else:
                        colorts = self.parent.page1.NewColorTables()
                        ctnum = i % len(colorts)
                        im = axes.imshow(image.T, interpolation='none', origin='lower', cmap=colorts[ctnum]) 
                        axes.figure.colorbar(im, orientation='vertical',cax=ax_cb)      
                     
                         
                    if self.parent.page1.show_scale:
                
                        labels = axes.get_xticks().tolist()
                        
                        newxscale =self.datastack.realX - self.datastack.realX[0]
                        newlabels = labels[:]
                        for ilb in range(len(labels)):
                            if labels[ilb]>=0 and labels[ilb]<len(self.datastack.realX):
                                newlabels[ilb] = str(np.round(newxscale[labels[ilb]]))
            
                        axes.set_xticklabels(newlabels, fontsize=8)
            
            
                        labels = axes.get_yticks().tolist()
                        
                        newyscale =self.datastack.realY - self.datastack.realY[0]
                        newlabels = labels[:]
                        for ilb in range(len(labels)):
                            if labels[ilb]>=0 and labels[ilb]<len(self.datastack.realY):
                                newlabels[ilb] = str(np.round(newyscale[labels[ilb]]))
            
                        axes.set_yticklabels(newlabels, fontsize=8)        
                        
                        axes.set_xlabel('X Distance [um]', fontsize=8) 
                        axes.set_ylabel('Y Distance [um]', fontsize=8)   
                        
                        fig.subplots_adjust(left=0.05, right=0.95, top = 0.95, bottom = 0.10, hspace=0.5, wspace=0.5) 
                        
            
                    else:           
                        axes.axis("off")    
                        
                    axes.set_title(title)

                                               
                    if isel == 1:
                        if roi_pdf:
                            fileName_img = SaveFileName+'_ROI_'+title + '.pdf'       
                            fig.savefig(fileName_img, dpi=300, pad_inches = 0.0)        
                        if roi_png:
                            fileName_img = SaveFileName+'_ROI_'+title + '.png'       
                            fig.savefig(fileName_img, dpi=300, pad_inches = 0.0)      
                        if roi_svg:
                            fileName_img = SaveFileName+'_ROI_'+title + '.svg'       
                        if roi_acsii:
                            fileName_acs = SaveFileName+'_ROI_'+title + '.txt'       
                            self.save_ascii(fileName_acs, self.datastack.realY, self.datastack.realX, image.T)    
                    elif isel == 2: 
                        if fit_pdf:
                            fileName_img = SaveFileName+'_Fit_'+title + '.pdf'       
                            fig.savefig(fileName_img, dpi=300, pad_inches = 0.0)        
                        if fit_png:
                            fileName_img = SaveFileName+'_Fit_'+title + '.png'       
                            fig.savefig(fileName_img, dpi=300, pad_inches = 0.0)      
                        if fit_svg:
                            fileName_img = SaveFileName+'_Fit_'+title + '.svg'       
                            fig.savefig(fileName_img, dpi=300, pad_inches = 0.0)   
                        if fit_acsii:
                            fileName_acs = SaveFileName+'_Fit_'+title + '.txt'       
                            self.save_ascii(fileName_acs, self.datastack.realY, self.datastack.realX, image.T)  
                                                        
                            
                            
            QtGui.QApplication.restoreOverrideCursor()
                              
        except IOError, e:
            QtGui.QApplication.restoreOverrideCursor()
            if e.strerror:
                err = e.strerror 
            else: 
                err = e 
    
            QtGui.QMessageBox.warning(self,'Error','Could not save files: %s' % err)
            
            
            
    
        self.close() 
        
        
#----------------------------------------------------------------------        
    def save_ascii(self, filename, x, y, data):
        
        dims = data.shape
        nc = dims[0]
        nr = dims[1]
        print dims, len(x), len(y)
        npix = nr*nc
        f = open(filename, 'w')
        
#         for i in range(nc):
#             for j in range(nr):
#                 print>>f, '%.6f, %.6f, %.6f' %(x[i], y[j], data[i,j])
                
        for j in range(nr):
            for i in range(nc):
            
                print>>f, '%.6f, %.6f, %.6f' %(x[i], y[j], data[i,j])
        
        f.close()
    
        return          
        
        

#----------------------------------------------------------------------
class TextForm(QtGui.QDialog):

    def __init__(self, parent, text):
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent


        
        self.resize(300, 170)
        self.setWindowTitle('New Text Label')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal) 
                
              

        
        vboxtop = QtGui.QVBoxLayout()
        
        
        gridtop = QtGui.QGridLayout()

        
        #fontb = wx.SystemSettings_GetFont(wx.SYS_DEFAULT_GUI_FONT)
        #fontb.SetWeight(wx.BOLD)
        
        
        st1 = QtGui.QLabel(self)
        st1.setText('Old Label: ')
        
        st2 = QtGui.QLabel(self)
        st2.setText(text)

        st3 = QtGui.QLabel(self)
        st3.setText('New Label')

        
        self.tc_1 = QtGui.QLineEdit(self)
        self.tc_1.setText(text)
        


        gridtop.addWidget(st1, 0,0)
        gridtop.addWidget(st2, 0,1)
        gridtop.addWidget(st3, 1,0)
        gridtop.addWidget( self.tc_1, 1,1)

        
          


        button_change = QtGui.QPushButton('Change Label')
        button_change.clicked.connect(self.OnChangeLabel)
                
        button_cancel = QtGui.QPushButton('Dismiss')
        button_cancel.clicked.connect(self.close)

        vboxtop.addStretch(1.0)
        vboxtop.addLayout(gridtop)
        vboxtop.addStretch(1.0)
        vboxtop.addWidget(button_change) 
        vboxtop.addWidget(button_cancel) 
        vboxtop.addStretch(1.0)
        
        self.setLayout(vboxtop)
        
        
#---------------------------------------------------------------------- 
    def OnChangeLabel(self, evt):

        newlabel = self.tc_1.text()
        self.parent.ChangeLabel(newlabel)
        
        self.close()

        
#---------------------------------------------------------------------- 
class ShowCompositeRBGmap(QtGui.QDialog):

    def __init__(self, parent, datastack, showmaps, showdet, ecnorm):    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        self.showmaps = showmaps
        self.showdet = showdet
        self.ecurrnorm = ecnorm
        
        self.resize(800, 550)
        self.setWindowTitle('Composite RBG Map')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)
                

        self.datastack = datastack 
            
        self.roinames = []
        if self.showmaps == 1:
            n_rois = self.datastack.ROI_maps.shape[0]
            for i in range(n_rois):
                self.roinames.append(self.datastack.ROI_names[i])        
        elif self.showmaps == 2:
            n_rois = len(self.datastack.fitmap_names)
            for i in range(n_rois):
                self.roinames.append(self.datastack.fitmap_names[i] )        


        self.show_info = 0
        
        
        self.n_cols = self.datastack.stack_ncols
        self.n_rows = self.datastack.stack_nrows
         
        self.rgbimage = np.zeros((self.n_rows, self.n_cols, 3), dtype=np.float64)
        
        self.minr = 0.
        self.maxr = 100.
        self.weightr = 100.
        self.ming = 0.
        self.maxg = 100.
        self.weightg = 100.
        self.minb = 0.
        self.maxb = 100.
        self.weightb = 100.    
        
        self.r_spec = 0
        self.g_spec = 1
        self.b_spec = 2
        

        sizer1 = QtGui.QGroupBox('Red spectrum')

        fgs1 = QtGui.QGridLayout()
        
        r = QtGui.QLabel(self) 
        r.setText('Red')
        rl = QtGui.QLabel(self) 
        rl.setText('Limits')
        rw = QtGui.QLabel(self) 
        rw.setText('Weight')     
        
        
        self.combor = QtGui.QComboBox(self)
        self.combor.addItems(self.roinames)
        self.combor.activated[int].connect(self.OnSelectR) 

        #self.combor.SetToolTip(wx.ToolTip("select spectrum from dropdown-list"))
        self.combor.setCurrentIndex(self.r_spec)
        
        hbox12 = QtGui.QHBoxLayout() 
        
        
        self.tcrmin = QtGui.QSpinBox()
        self.tcrmin.setRange(0,100)
        self.tcrmin.setValue(0)
        self.tcrmin.valueChanged[int].connect(self.OnLimitMinR)
        
        self.tcrmax = QtGui.QSpinBox()
        self.tcrmax.setRange(0,100)
        self.tcrmax.setValue(100)
        self.tcrmax.valueChanged[int].connect(self.OnLimitMaxR)        
                        
        hbox12.addWidget(self.tcrmin)
        hbox12.addWidget(self.tcrmax)
        
        self.tcrweight = QtGui.QSpinBox()
        self.tcrweight.setRange(0,100)
        self.tcrweight.setValue(100)
        self.tcrweight.valueChanged[int].connect(self.OnWeightR)
        
        fgs1.addWidget(r, 0, 0)
        fgs1.addWidget(self.combor, 0, 1)  
        fgs1.addWidget(rl, 1, 0)  
        fgs1.addLayout(hbox12, 1, 1)
        fgs1.addWidget(rw, 2, 0)  
        fgs1.addWidget(self.tcrweight, 2, 1)  
        
        sizer1.setLayout(fgs1)
        
        
  
        sizer2 = QtGui.QGroupBox('Green spectrum')

        fgs2 = QtGui.QGridLayout()
        
        g = QtGui.QLabel(self) 
        g.setText('Green')
        gl = QtGui.QLabel(self) 
        gl.setText('Limits')
        gw = QtGui.QLabel(self) 
        gw.setText('Weight')     
        
        
        self.combog = QtGui.QComboBox(self)
        self.combog.addItems(self.roinames)
        self.combog.activated[int].connect(self.OnSelectG) 

        #self.combor.SetToolTip(wx.ToolTip("select spectrum from dropdown-list"))
        self.combog.setCurrentIndex(self.g_spec)
        
        hbox12 = QtGui.QHBoxLayout() 
        
        
        self.tcgmin = QtGui.QSpinBox()
        self.tcgmin.setRange(0,100)
        self.tcgmin.setValue(0)
        self.tcgmin.valueChanged[int].connect(self.OnLimitMinG)
        
        self.tcgmax = QtGui.QSpinBox()
        self.tcgmax.setRange(0,100)
        self.tcgmax.setValue(100)
        self.tcgmax.valueChanged[int].connect(self.OnLimitMaxG)        
                        
        hbox12.addWidget(self.tcgmin)
        hbox12.addWidget(self.tcgmax)
        
        self.tcgweight = QtGui.QSpinBox()
        self.tcgweight.setRange(0,100)
        self.tcgweight.setValue(100)
        self.tcgweight.valueChanged[int].connect(self.OnWeightG)
                
        
        fgs2.addWidget(g, 0, 0)
        fgs2.addWidget(self.combog, 0, 1)  
        fgs2.addWidget(gl, 1, 0)  
        fgs2.addLayout(hbox12, 1, 1)
        fgs2.addWidget(gw, 2, 0)  
        fgs2.addWidget(self.tcgweight, 2, 1)  
        
        
        sizer2.setLayout(fgs2)
                 


        sizer3 = QtGui.QGroupBox('Blue spectrum')

        fgs3 = QtGui.QGridLayout()
        
        b = QtGui.QLabel(self) 
        b.setText('Blue')
        bl = QtGui.QLabel(self) 
        bl.setText('Limits')
        bw = QtGui.QLabel(self) 
        bw.setText('Weight')     
        
        
        self.combob = QtGui.QComboBox(self)
        self.combob.addItems(self.roinames)
        self.combob.activated[int].connect(self.OnSelectB) 

        #self.combor.SetToolTip(wx.ToolTip("select spectrum from dropdown-list"))
        self.combob.setCurrentIndex(self.b_spec)
        
        hbox12 = QtGui.QHBoxLayout() 
        
        
        self.tcbmin = QtGui.QSpinBox()
        self.tcbmin.setRange(0,100)
        self.tcbmin.setValue(0)
        self.tcbmin.valueChanged[int].connect(self.OnLimitMinB)
        
        self.tcbmax = QtGui.QSpinBox()
        self.tcbmax.setRange(0,100)
        self.tcbmax.setValue(100)
        self.tcbmax.valueChanged[int].connect(self.OnLimitMaxB)        
                        
        hbox12.addWidget(self.tcbmin)
        hbox12.addWidget(self.tcbmax)
        
        self.tcbweight = QtGui.QSpinBox()
        self.tcbweight.setRange(0,100)
        self.tcbweight.setValue(100)
        self.tcbweight.valueChanged[int].connect(self.OnWeightB)
                
        
        fgs3.addWidget(b, 0, 0)
        fgs3.addWidget(self.combob, 0, 1)  
        fgs3.addWidget(bl, 1, 0)  
        fgs3.addLayout(hbox12, 1, 1)
        fgs3.addWidget(bw, 2, 0)  
        fgs3.addWidget(self.tcbweight, 2, 1)  
        
        
        sizer3.setLayout(fgs3)
        
          

        
        vbox = QtGui.QVBoxLayout()
        hbox1 = QtGui.QHBoxLayout()
        vbox1 = QtGui.QVBoxLayout()
                       
        vbox1.addWidget(sizer1)
        vbox1.addWidget(sizer2)
        vbox1.addWidget(sizer3)
        
        
        
        self.show_info_cb = QtGui.QCheckBox( 'Show Info on the Image', self)
        self.show_info_cb.stateChanged.connect(self.OnShowInfo)
        vbox1.addWidget(self.show_info_cb)
        
        hbox1.addLayout(vbox1)
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
        self.RGBImagefig = Figure((7.5, 4.0))
        self.RGBImagePanel = FigureCanvas(self.RGBImagefig)
        fbox.addWidget(self.RGBImagePanel)
        frame.setLayout(fbox)

        hbox1.addWidget(frame)
        
        
        vbox.addLayout(hbox1) 
        
             
        hbox2 = QtGui.QHBoxLayout()
        
        button_save = QtGui.QPushButton('Save image')
        button_save.clicked.connect( self.OnSave)   
        hbox2.addWidget(button_save)
        
        button_close = QtGui.QPushButton('Dismiss')
        button_close.clicked.connect( self.close)   
        hbox2.addWidget(button_close)
    
        
        vbox.addLayout(hbox2)
        
        
        self.setLayout(vbox)

        
        self.CalcR()
        self.CalcG()
        self.CalcB()        
        self.draw_image()        
        
        
#----------------------------------------------------------------------           
    def OnSelectR(self, value):
        item = value
        self.r_spec = item
        
        self.CalcR()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def CalcR(self):
        
        tsmap = np.zeros((self.datastack.stack_ncols, self.datastack.stack_nrows), dtype=np.int64)    
        

        if self.showmaps == 1:
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.ROI_maps[self.r_spec,:,:,idet]                  

        elif self.showmaps == 2: 
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.fit_maps[self.r_spec,:,:,idet]                  
     
                    
        if self.ecurrnorm == 1:
            tsmap = np.multiply(tsmap, self.datastack.ecurrent)
            print 'Normalizing RGB map with e-current.'
            #print np.mean(self.datastack.ecurrent)
         
        uscale_min = tsmap.min()
        uscale_max = tsmap.max()
        
        scale_min = uscale_min + (uscale_max-uscale_min)*float(self.minr)/100.
        scale_max = uscale_min + (uscale_max-uscale_min)*float(self.maxr)/100.
        

        if scale_min >= scale_max: 
            tsmap = np.zeros((self.n_cols, self.n_rows), dtype=float)
        else:
            tsmap = tsmap.clip(min=scale_min, max=scale_max)
            tsmap = (tsmap-scale_min) / (scale_max - scale_min)
            
            
        indices = np.where(tsmap < 0)
        tsmap[indices] = 0.0
        indices = np.where(tsmap > 1)
        tsmap[indices] = 1.0
        
        self.rgbimage[:,:,0] = tsmap.T*float(self.weightr)/100.

        
        
#----------------------------------------------------------------------           
    def OnLimitMinR(self, value):

        self.minr = value

        self.CalcR()
        self.draw_image()
    
#----------------------------------------------------------------------           
    def OnLimitMaxR(self, value):

        self.maxr = value

        self.CalcR()
        self.draw_image()
        
            
#----------------------------------------------------------------------           
    def OnWeightR(self, value):

        self.weightr = value

        self.CalcR()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def OnSelectG(self, value):
        item = value
        self.g_spec = item

        self.CalcG()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def CalcG(self):

        tsmap = np.zeros((self.datastack.stack_ncols, self.datastack.stack_nrows), dtype=np.int64)    
        

        if self.showmaps == 1:
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.ROI_maps[self.g_spec,:,:,idet]                  

        elif self.showmaps == 2: 
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.fit_maps[self.g_spec,:,:,idet]     
                    
        if self.ecurrnorm == 1:
            tsmap = np.multiply(tsmap, self.datastack.ecurrent)
    
        uscale_min = tsmap.min()
        uscale_max = tsmap.max()
        
        scale_min = uscale_min + (uscale_max-uscale_min)*float(self.ming)/100.
        scale_max = uscale_min + (uscale_max-uscale_min)*float(self.maxg)/100.

        if scale_min >= scale_max: 
            tsmap = np.zeros((self.n_cols, self.n_rows), dtype=float)
        else:
            tsmap = tsmap.clip(min=scale_min, max=scale_max)
            tsmap = (tsmap - scale_min) / (scale_max - scale_min)


        indices = np.where(tsmap < 0)
        tsmap[indices] = 0.0
        indices = np.where(tsmap > 1)
        tsmap[indices] = 1.0
    
        self.rgbimage[:,:,1] = tsmap.T*float(self.weightg)/100.
        
#----------------------------------------------------------------------           
    def OnLimitMinG(self, value):

        self.ming = value

        self.CalcG()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def OnLimitMaxG(self, value):

        self.maxg = value

        self.CalcG()
        self.draw_image()
            
#----------------------------------------------------------------------           
    def OnWeightG(self, value):

        self.weightg = value

        self.CalcG()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def OnSelectB(self, value):
        item = value
        self.b_spec = item
        
        self.CalcB()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def CalcB(self):
        
        tsmap = np.zeros((self.datastack.stack_ncols, self.datastack.stack_nrows), dtype=np.int64)    
        

        if self.showmaps == 1:
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.ROI_maps[self.b_spec,:,:,idet]                  

        elif self.showmaps == 2: 
            for idet in range(len(self.showdet)):
                if self.showdet[idet] == 1:
                    tsmap = tsmap + self.datastack.fit_maps[self.b_spec,:,:,idet] 
                    
                    
        if self.ecurrnorm == 1:
            tsmap = np.multiply(tsmap, self.datastack.ecurrent)               

        uscale_min = tsmap.min()
        uscale_max = tsmap.max()
        
        scale_min = uscale_min + (uscale_max-uscale_min)*float(self.minb)/100.
        scale_max = uscale_min + (uscale_max-uscale_min)*float(self.maxb)/100.

        if scale_min >= scale_max: 
            tsmap = np.zeros((self.n_cols, self.n_rows), dtype=float)
        else:
            tsmap = tsmap.clip(min=scale_min, max=scale_max)
            tsmap = (tsmap - scale_min) / (scale_max - scale_min)

        indices = np.where(tsmap < 0)
        tsmap[indices] = 0.0
        indices = np.where(tsmap > 1)
        tsmap[indices] = 1.0
    
        self.rgbimage[:,:,2] = tsmap.T*float(self.weightb)/100.
                
#----------------------------------------------------------------------           
    def OnLimitMinB(self, value):

        self.minb = value

        self.CalcB()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def OnLimitMaxB(self, value):

        self.maxb = value

        self.CalcB()
        self.draw_image()
            
#----------------------------------------------------------------------           
    def OnWeightB(self, value):

        self.weightb = value

        self.CalcB()
        self.draw_image()
        
#----------------------------------------------------------------------           
    def OnShowInfo(self, state):
        
        if state == QtCore.Qt.Checked:
            self.show_info = 1
        else: 
            self.show_info = 0
        
        self.draw_image()
        
#----------------------------------------------------------------------        
    def draw_image(self):
               
               
        fig = self.RGBImagefig
        fig.clf()
        #fig.add_axes(((0.0,0.0,1.0,1.0)))
              
        #axes = fig.gca()
        fig.patch.set_alpha(1.0) 
        #axes.axis("off")
        
        gs = gridspec.GridSpec(3, 4)
        gs.update(left=0.0, right=1.0, hspace=0.05, wspace=0.05)
        
        axes1 = fig.add_subplot(gs[:, 1:])
        im = axes1.imshow(self.rgbimage, interpolation='none', origin='lower')
        
        if self.parent.page1.show_scale:
    
            labels = axes1.get_xticks().tolist()
            
            newxscale =self.datastack.realX - self.datastack.realX[0]
            newlabels = labels[:]
            for ilb in range(len(labels)):
                if labels[ilb]>=0 and labels[ilb]<len(self.datastack.realX):
                    newlabels[ilb] = str(np.round(newxscale[labels[ilb]]))

            axes1.set_xticklabels(newlabels, fontsize=self.datastack.fontsize_labels)


            labels = axes1.get_yticks().tolist()
            
            newyscale =self.datastack.realY - self.datastack.realY[0]
            newlabels = labels[:]
            for ilb in range(len(labels)):
                if labels[ilb]>=0 and labels[ilb]<len(self.datastack.realY):
                    newlabels[ilb] = str(np.round(newyscale[labels[ilb]]))

            axes1.set_yticklabels(newlabels, fontsize=self.datastack.fontsize_labels)        
            
            axes1.set_xlabel('X Distance [um]', fontsize=self.datastack.fontsize_labels) 
            axes1.set_ylabel('Y Distance [um]', fontsize=self.datastack.fontsize_labels)   
            
            #fig.subplots_adjust(left=0.05, right=0.95, top = 0.95, bottom = 0.10, hspace=0.5, wspace=0.5) 
            
        else:           
            axes1.axis("off")    

        redimg = self.rgbimage.copy()
        redimg[:,:,1] = 0
        redimg[:,:,2] = 0        
        
        axes2 = fig.add_subplot(gs[0, 0])
        im = axes2.imshow(redimg, interpolation='none', origin='lower')
         
        axes2.axis("off")     
        
        grimg = self.rgbimage.copy()
        grimg[:,:,0] = 0
        grimg[:,:,2] = 0 
        
        axes3 = fig.add_subplot(gs[1, 0])
        im = axes3.imshow(grimg, interpolation='none', origin='lower')
          
        axes3.axis("off")    
        
        blimg = self.rgbimage.copy()
        blimg[:,:,0] = 0
        blimg[:,:,1] = 0 
        
        axes4 = fig.add_subplot(gs[2, 0])
        im = axes4.imshow(blimg, interpolation='none', origin='lower')
        axes4.axis("off") 
        
        if self.show_info == 1:
            startx = int(self.n_rows*0.02)
            starty = self.n_cols-int(self.n_cols*0.15)
            info = 'R:%s [%d] \nG:%s [%d] \nB:%s [%d]' % (self.roinames[self.r_spec], self.weightr,
                                                        self.roinames[self.g_spec], self.weightg,
                                                        self.roinames[self.b_spec], self.weightb)
            axes1.text(+startx+1,starty+1, info, horizontalalignment='left', verticalalignment='center',
                      color = 'white', fontsize=8)

            
        self.RGBImagePanel.draw()
        
#----------------------------------------------------------------------              
    def OnSave(self, evt):

        wildcard = "Portable Network Graphics (*.png);;Adobe PDF Files (*.pdf);;"

        SaveFileName = QtGui.QFileDialog.getSaveFileName(self, 'Save Plot', '', wildcard)

        SaveFileName = str(SaveFileName)
        if SaveFileName == '':
            return
        
        path, ext = os.path.splitext(SaveFileName) 
        ext = ext[1:].lower() 
        
       
        if ext != 'png' and ext != 'pdf': 
            error_message = ( 
                  'Only the PNG and PDF image formats are supported.\n' 
                 'A file extension of `png\' or `pdf\' must be used.') 

            QtGui.QMessageBox.warning(self, 'Error', 'Could not save file: %s' % error_message)
            return 
   

        matplotlib.rcParams['pdf.fonttype'] = 42

                            
        fig = self.RGBImagefig
        fig.savefig(SaveFileName, bbox_inches='tight', pad_inches = 0.0)
                
           


#---------------------------------------------------------------------- 
class Scatterplots(QtGui.QDialog):

    def __init__(self, parent, datastack, showmaps):    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        
        self.showmaps = showmaps

        
        self.resize(600, 470)
        self.setWindowTitle('Scatter plots')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)
                
         
         
        self.datastack = datastack

        self.ncols = self.datastack.stack_ncols
        self.nrows = self.datastack.stack_nrows
        
        self.roinames = []
        
        if self.showmaps == 1:
            self.nrois = self.datastack.ROI_maps.shape[0]
            for i in range(self.nrois):
                self.roinames.append(self.datastack.ROI_names[i])              
        
        elif self.showmaps == 2:
            self.nrois = len(self.datastack.fitmap_names)
            for i in range(self.nrois):
                self.roinames.append(self.datastack.fitmap_names[i] )  

                
                      
        self.i_y = 1
        self.i_x = 1
         
                 
        vbox = QtGui.QVBoxLayout()
           
        grid1 = QtGui.QGridLayout()

        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
        self.scattplfig = Figure((5.0, 4.8))
        self.ScatterPPanel = FigureCanvas(self.scattplfig)
        self.ScatterPPanel.setParent(self)
        fbox.addWidget(self.ScatterPPanel)
        frame.setLayout(fbox)
        
        self.slidershow_y = QtGui.QSlider(QtCore.Qt.Vertical)
        self.slidershow_y.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slidershow_y.setRange(1, self.nrois)
        self.slidershow_y.setValue(self.i_y)          
        self.slidershow_y.valueChanged[int].connect(self.OnSliderScroll_y)
               
        grid1.addWidget(self.slidershow_y, 0, 0)
        grid1.addWidget(frame, 0, 1)
                
         
        self.slidershow_x = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slidershow_x.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.slidershow_x.setRange(1, self.nrois)
        self.slidershow_x.setValue(self.i_x)          
        self.slidershow_x.valueChanged[int].connect(self.OnSliderScroll_x)
         
        #grid1.addWidget(wx.StaticText(panel, -1, ''))
        grid1.addWidget(self.slidershow_x, 1,  1)
         
        hbox = QtGui.QVBoxLayout()
         
        button_close = QtGui.QPushButton('Close')
        button_close.clicked.connect( self.close)
                
        hbox.addStretch(1)
        hbox.addWidget(button_close)
         
        vbox.addLayout(grid1)
        vbox.addLayout(hbox)
                 
         
        self.setLayout(vbox)
         
        self.draw_scatterplot()
        
        
#----------------------------------------------------------------------        
    def OnSliderScroll_x(self, value):
        self.i_x = value
        
        self.draw_scatterplot()

#----------------------------------------------------------------------        
    def OnSliderScroll_y(self, value):
        self.i_y = value
        
        self.draw_scatterplot()        

      
#----------------------------------------------------------------------        
    def draw_scatterplot(self):
                
        if self.showmaps == 1:
            x_comp = np.reshape(np.sum(self.datastack.ROI_maps[self.i_x-1,:,:,:], axis=2), (self.ncols*self.nrows), order='F')
            y_comp = np.reshape(np.sum(self.datastack.ROI_maps[self.i_y-1,:,:,:], axis=2), (self.ncols*self.nrows), order='F')   
                            
        elif self.showmaps == 2:
            x_comp = np.reshape(self.datastack.fit_maps[self.i_x-1], (self.ncols*self.nrows), order='F')
            y_comp = np.reshape(self.datastack.fit_maps[self.i_y-1], (self.ncols*self.nrows), order='F')                


        
        fig = self.scattplfig
        fig.clf()
        axes = fig.gca()
             
        
        axes.plot(x_comp, y_comp,'.')
            
        axes.set_xlabel(self.roinames[self.i_x-1])
        axes.set_ylabel(self.roinames[self.i_y-1])
      
        self.ScatterPPanel.draw()
       
  
                
#----------------------------------------------------------------------  
class InputRegionDialog(QtGui.QDialog):

    def __init__(self, parent, nregions, title='Multi Image Stack'):
    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
    
        mainLayout = QtGui.QVBoxLayout()
    
        layout = QtGui.QHBoxLayout()
        label = QtGui.QLabel()
        label.setText('Select Image to load:')
        layout.addWidget(label)
    
        combo = QtGui.QComboBox(self)
        for i in range(nregions):
            combo.addItem(str(i+1))
        combo.activated[int].connect(self.OnSelectRegion) 

        layout.addWidget(combo)
    
        mainLayout.addLayout(layout)
    

        layout = QtGui.QHBoxLayout()
        button = QtGui.QPushButton("Submit") #string or icon
        self.connect(button, QtCore.SIGNAL("clicked()"), self.close)
        layout.addWidget(button)
    
        mainLayout.addLayout(layout)
        self.setLayout(mainLayout)
    
        self.resize(250, 60)
        self.setWindowTitle(title)
                               
#----------------------------------------------------------------------           
    def OnSelectRegion(self, value):
        item = value
        selregion = item
        self.parent.read_img = selregion

          
       
""" ------------------------------------------------------------------------------------------------"""
class PageLoadData(QtGui.QWidget):
    def __init__(self, datastack):
        super(PageLoadData, self).__init__()

        self.initUI(datastack)
        
#----------------------------------------------------------------------          
    def initUI(self, datastack): 
        
        self.datastack = datastack
        
        self.initMatplotlib()
        
        self.show_scale_bar = 1
        self.show_log = 0


        #panel 1
        sizer1 = QtGui.QGroupBox('Load Data')
        vbox1 = QtGui.QVBoxLayout()
        
        
        self.button_ah5 = QtGui.QPushButton('  Load ANKA .h5 stack  ')
        self.button_ah5.clicked.connect(self.OnLoadStackH5)
        #self.button_ah5.setMaximumWidth(200)
        self.button_ah5.setMinimumWidth(200)
        vbox1.addWidget(self.button_ah5)
        
        
        self.button_mh5 = QtGui.QPushButton('  Load MAPS .h5 stack  ')
        self.button_mh5.clicked.connect(self.OnLoadStackH5)
        #self.button_mh5.setMaximumWidth(200)
        vbox1.addWidget(self.button_mh5)
        
        
        self.button_nxs = QtGui.QPushButton('  Load DLS .nxs stack  ')
        self.button_nxs.clicked.connect(self.OnLoadStackNXS)
        #self.button_nxs.setMaximumWidth(200)
        vbox1.addWidget(self.button_nxs)
        
        if havexrftomo:
            line = QtGui.QFrame()
            line.setFrameShape(QtGui.QFrame.HLine)
            line.setFrameShadow(QtGui.QFrame.Sunken) 
            vbox1.addWidget(line)
        
            self.button_ah5_4D = QtGui.QPushButton('  Load ANKA XRF-TOMO .h5 stack  ')
            self.button_ah5_4D.clicked.connect(self.OnLoadStackH54D)
            #self.button_ah5.setMaximumWidth(200)
            self.button_ah5_4D.setMinimumWidth(200)
            vbox1.addWidget(self.button_ah5_4D)
        
        
#         self.button_onlinedata = QtGui.QPushButton('      Online data monitor ...       ')
#         self.button_onlinedata.clicked.connect(self.OnOnlineDataView) 
#         self.button_onlinedata.setMaximumWidth(200)  
#         vbox1.addWidget(self.button_onlinedata)
        
        sizer1.setLayout(vbox1)
        sizer1.setMaximumWidth(300)
        
        
        #panel 2
        sizer2 = QtGui.QGroupBox('Display')
        vbox2 = QtGui.QVBoxLayout()

        self.cb_scalebar = QtGui.QCheckBox('Scalebar', self)
        self.cb_scalebar.setChecked(self.show_scale_bar)
        self.cb_scalebar.stateChanged.connect(self.OnCBScaleBar)
        vbox2.addWidget(self.cb_scalebar)       

        self.cb_log = QtGui.QCheckBox('Logscale', self)
        self.cb_log.stateChanged.connect(self.OnCBLogscale)
        vbox2.addWidget(self.cb_log)    
        
        sizer2.setLayout(vbox2)
        sizer2.setMaximumWidth(300)
        
        
        #panel 4
        sizer4 = QtGui.QGroupBox('Preprocessing')
        vbox4 = QtGui.QVBoxLayout()

        self.button_iochnorm = QtGui.QPushButton('  Normalize with IC Counts ')
        self.button_iochnorm.clicked.connect(self.OnNormalizeICCounts)
        self.button_iochnorm.setEnabled(False)
        vbox4.addWidget(self.button_iochnorm)
         
        sizer4.setLayout(vbox4)
        sizer4.setMaximumWidth(300)
                         
        
        #panel 3
        sizer3 = QtGui.QGroupBox('File')
        vbox3 = QtGui.QVBoxLayout()
 
  
        self.tc_file = QtGui.QLabel(self)
        vbox3.addWidget(self.tc_file)
        self.tc_file.setText('File name')
        
        vbox3.setContentsMargins(20,20,20,30)
        sizer3.setLayout(vbox3)
        
        #panel 5     
        vbox5 = QtGui.QVBoxLayout()
        
        self.tc_imageeng = QtGui.QLabel(self)
        self.tc_imageeng.setText(" ")
        vbox5.addWidget(self.tc_imageeng)
        

        hbox51 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.absimgfig = Figure((PlotH*1.2, PlotH*1.2))
        self.AbsImagePanel = FigureCanvas(self.absimgfig)
        self.AbsImagePanel.setParent(self)
        
        fbox.addWidget(self.AbsImagePanel)
        frame.setLayout(fbox)
        hbox51.addWidget(frame)
        

#         self.slider_eng = QtGui.QScrollBar(QtCore.Qt.Vertical)
#         self.slider_eng.setFocusPolicy(QtCore.Qt.StrongFocus)
#         self.slider_eng.valueChanged[int].connect(self.OnScrollEng)
#         self.slider_eng.setRange(0, 100)
#         
#         hbox51.addWidget(self.slider_eng)        

        
        vbox5.addLayout(hbox51)
        
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        vboxt1 = QtGui.QVBoxLayout()
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer1)
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer4)
        vboxt1.addStretch (1)
        vboxt1.addWidget(sizer2)
        vboxt1.addStretch (1)
        
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vboxt1)
        hboxtop.addStretch (0.5)
        hboxtop.addLayout(vbox5)
        hboxtop.addStretch (0.5)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (0.5)
        vboxtop.addWidget(sizer3)
        vboxtop.addStretch (0.5)
        #vboxtop.addWidget(sizer4)
        #vboxtop.addStretch (0.5)

        vboxtop.setContentsMargins(50,50,50,50)
        self.setLayout(vboxtop)
        

        
    
    
#----------------------------------------------------------------------   
    def initMatplotlib(self):  
               
       
        matplotlib.rcParams['figure.facecolor'] = 'white'
        matplotlib.rcParams['font.size'] = 10.0
                

#----------------------------------------------------------------------
    def OnLoadStackNXS(self, event, wcard = ''):
        
        wildcard =  "HDF5 Nexus files (*.nxs);;HDF5 files (*.h5)" 
        
        self.LoadStack(wildcard)
        
#----------------------------------------------------------------------
    def OnLoadStackH5(self, event, wcard = ''):    
        
        wildcard =  "HDF5 files (*.h5);;HDF5 Nexus files (*.nxs)"     
        self.LoadStack(wildcard)
                
#----------------------------------------------------------------------
    def LoadStack(self, wcard = ''):
        """
        Browse for a stack file:
        """

        try:
        #if True:
            
            wildcard =  "HDF5 Nexus files (*.nxs);;MAPS HDF5 files (*.h5)" 
            if wcard != '':
                wildcard = wcard

            filepath = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '', wildcard)
            

            filepath = str(filepath)
            if filepath == '':
                return
            
            if self.datastack.stack_loaded == 1:
                self.NewStack()
            
            
            directory =  os.path.dirname(str(filepath))
            DefaultDir = directory
            filename =  os.path.basename(str(filepath))
        
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
            basename, extension = os.path.splitext(filename)      
            
                       
            if extension == '.nxs':       
                Ch5 = file_dls_nexus_hdf5.Ch5()        
                self.datastack.stack, self.datastack.fullSpectrum, self.datastack.inenergy, self.datastack.realX, self.datastack.realY, roimaps, roiinfo = Ch5.read_nxs(filepath)
                self.datastack.stack_ncols = self.datastack.fullSpectrum.shape[0]
                self.datastack.stack_nrows = self.datastack.fullSpectrum.shape[1]
                
   
                if roimaps != []:
                    self.datastack.ROI_maps_loaded = 1
                    self.datastack.ROI_maps = roimaps
                    self.datastack.ROI_info = roiinfo
                    self.datastack.ROI_names = []
                    for j in range(len(roiinfo)):
                        self.datastack.ROI_names.append(roiinfo[j][1])

                
                self.scale_bar()
                
                self.datastack.stack_loaded = 1
                self.datastack.fileformat = 'nxs'

                if self.datastack.ROI_maps_loaded > 0:
                    self.window().page1.showmaps = 1
                    
                    
            elif extension == '.h5': 
                
                
                # Open HDF5 file
                f = h5py.File(filepath, 'r') 
 
                if 'MAPS' in f:
                    f.close() 
                    Cmp = file_maps_h5.Cmapsh5()
                    self.datastack.stack, self.datastack.fullSpectrum, self.datastack.inenergy, self.datastack.realX, self.datastack.realY, roimaps, roiinfo = Cmp.read_h5(filepath)
                
                    fits, channelnames = Cmp.get_fits_h5(filepath)  
                    
                    self.datastack.fileformat = 'maps-h5'
                    
                elif 'data' in f:
                    multi_img = 0
                    self.read_img = 0
                    fits = []
                    datagrp = f['data']
                    if 'TXM' in datagrp:
                        #Get the number of images:
                        if 'processed' in datagrp:
                            prcgrp = datagrp['processed']
                             
                            if 'sensor1d' in prcgrp:
                                s1grp = prcgrp['sensor1d']    
                                dsx = s1grp['vortex_0']
                                thisdata = dsx[...]
                                dims = thisdata.shape
                                if len(dims) == 4:
                                    multi_img = dims[0]
                                if multi_img > 0:
                                    QtGui.QApplication.restoreOverrideCursor()
                                    inputter = InputRegionDialog(self, nregions = multi_img)
                                    inputter.exec_()
                
                                    QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
                                                                                   
                    f.close() 
                    Cah5 = file_anka_h5.Cah5()
                    self.datastack.stack, self.datastack.fullSpectrum, self.datastack.inenergy, \
                    self.datastack.realX, self.datastack.realY, roimaps, roiinfo, self.datastack.channels, \
                    self.datastack.ecurrent, self.datastack.ecmean, \
                    self.datastack.ioncount = Cah5.read_h5(filepath,
                                                            multiimg = multi_img, 
                                                            readimg = self.read_img,
                                                            fix_glitch = self.datastack.anka_glitch)
                    self.datastack.fileformat = 'anka-h5'
                    self.datastack.multiimg = multi_img
                    self.datastack.readimg = self.read_img
                    
                    if len(list(self.datastack.ioncount))>0:
                        self.button_iochnorm.setEnabled(True)          
                    
                    
                self.datastack.stack_ncols = self.datastack.fullSpectrum.shape[0]
                self.datastack.stack_nrows = self.datastack.fullSpectrum.shape[1]  
                

                self.scale_bar()
                
                self.datastack.stack_loaded = 1
                self.datastack.tomodata = 0
                                
                
                if fits != []:
                    self.datastack.fits_calculated = 1
                    self.datastack.fit_maps = fits
                    self.datastack.fitmap_names = channelnames        
                    self.datastack.ROI_maps_loaded = 1     
                    self.window().page1.showmaps = 2     
                    self.window().page1.rb_fit.setEnabled(True)
                    self.window().page1.rb_fit.setChecked(True)   
                
            
            #print 'Data array shape', self.datastack.fullSpectrum.shape
            
            if len(self.datastack.fullSpectrum.shape) > 3:
                self.window().page1.EnableDetectorView(True)
            else:
                self.window().page1.EnableDetectorView(False)
                
            self.datastack.filename = filepath
            
              
                       
            QtGui.QApplication.restoreOverrideCursor()
             
        except:
            print 'Error - could not load file'
            QtGui.QApplication.restoreOverrideCursor()
            QtGui.QMessageBox.warning(self, 'Error', 'Could load file!')
            
            
        self.UpdateDisplay()
        
        self.window().page1.UpdateDisplay()
        self.window().page4.UpdateDisplay()
        self.window().page2.UpdateDisplay()
        self.window().page5.UpdateDisplay()



#----------------------------------------------------------------------
    def OnLoadStackH54D(self, event, wcard = ''):    
        
        wildcard =  "HDF5 files (*.h5)"     
        
        #try:
        if True:


            filepath = QtGui.QFileDialog.getOpenFileName(self, 'Open 4D stack file', '', wildcard)
            

            filepath = str(filepath)
            if filepath == '':
                return
            
            if self.datastack.stack_loaded == 1:
                self.NewStack()
            
            
            directory =  os.path.dirname(str(filepath))
            DefaultDir = directory
            filename =  os.path.basename(str(filepath))
        
            QtGui.QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
            basename, extension = os.path.splitext(filename)      
            
                       
                    
                    
            if extension == '.h5': 
                
                Cah5 = file_anka_h5.Cah5()
                self.datastack.stack, self.datastack.fullSpectrum, self.datastack.fullSpectrum4D,\
                self.datastack.theta, self.datastack.inenergy, self.datastack.realX, self.datastack.realY, \
                roimaps, roiinfo, self.datastack.channels, \
                self.datastack.ecurrent, self.datastack.ecurrent4D, \
                self.datastack.ecmean, self.datastack.ioncount = Cah5.read_h5_4D(filepath, fix_glitch = self.datastack.anka_glitch)
                                                                                                                                                                                                                                                                                                                                                                       
                self.datastack.fileformat = 'anka-h5'
                self.datastack.multiimg = 0
                self.datastack.readimg = 0
                
                                        
                self.datastack.stack_ncols = self.datastack.fullSpectrum.shape[0]
                self.datastack.stack_nrows = self.datastack.fullSpectrum.shape[1]  
                

                self.scale_bar()
                
                self.datastack.stack_loaded = 1
                
                self.datastack.tomodata = 1
                
                
            
            print 'Data array shape', self.datastack.fullSpectrum.shape
            
            if len(self.datastack.fullSpectrum.shape) > 3:
                self.window().page1.EnableDetectorView(True)
            else:
                self.window().page1.EnableDetectorView(False)
                
            self.datastack.filename = filepath
            
                       
            QtGui.QApplication.restoreOverrideCursor()
            
#         except:
#             print 'Error - could not load file'
#             QtGui.QApplication.restoreOverrideCursor()
            
            
        self.UpdateDisplay()
        
        self.window().page1.UpdateDisplay()
        self.window().page4.UpdateDisplay()
        self.window().page2.UpdateDisplay()
        self.window().page5.UpdateDisplay()
        
        
        
#----------------------------------------------------------------------
    def OnOnlineDataView(self, event):  

        ddwin = OnlineDataDisplay(self.window(), self.datastack)
        ddwin.show()

#----------------------------------------------------------------------           
    def OnCBScaleBar(self, state):
        
        
        if self.cb_scalebar.isChecked():
            self.show_scale_bar = 1
        else: self.show_scale_bar = 0
        
        self.ShowImage()

#----------------------------------------------------------------------           
    def OnCBLogscale(self, state):
        
        
        if self.cb_log.isChecked():
            self.show_log = 1
        else: self.show_log = 0
        
        self.ShowImage()    
        
        self.window().page4.UpdateDisplay()
        self.window().page2.UpdateDisplay()                

#----------------------------------------------------------------------
    def OnNormalizeICCounts(self, event):  
        
        
        if len(list(self.datastack.ioncount)) == 0:
            return
        
        if self.datastack.tomodata == 0:
        
            dims = self.datastack.fullSpectrum.shape
            
            nchan = dims[3]
    
            #normalize the data
            for i in range(nchan):
                self.datastack.stack[:,:,i] /= self.datastack.ioncount
                for j in range(dims[2]):
                    self.datastack.fullSpectrum[:,:,j,i] /= self.datastack.ioncount
                    
        else:
            
            dims = self.datastack.fullSpectrum.shape
            
            nchan = dims[3]
    
            #normalize the data
            for i in range(nchan):
                self.datastack.stack[:,:,i,:] /= self.datastack.ioncount
                for j in range[dims[2]]:
                    self.datastack.fullSpectrum[:,:,j,i,:] /= self.datastack.ioncount    
            

        self.button_iochnorm.setEnabled(False)
        
        self.ShowImage()    
        
        self.window().page4.UpdateDisplay()
        self.window().page2.UpdateDisplay()                      
        
        
        
#----------------------------------------------------------------------        
    def UpdateDisplay(self):
        
        
        self.ShowImage()
        
        self.tc_file.setText(self.datastack.filename)
        
        
        
#----------------------------------------------------------------------        
    def NewStack(self):  
        
        self.datastack.stack_loaded = 0
        self.datastack.ROI_maps_loaded = 0
                
        self.datastack.path = ''
        self.datastack.filename = ''

        self.datastack.stack = []
        self.datastack.fullSpectrum = []
        self.datastack.dScanPars = None
        self.datastack.realX = []
        self.datastack.stack_ncols = 0
        self.datastack.stack_nrows = 0
        self.datastack.inenergy = 0
        
        self.datastack.ecurrent = []
        
        self.datastack.tomodata = 0
        self.datastack.fullSpectrum4D = []
        self.datastack.ecurrent4D = []
        self.datastack.theta = []
        self.datastack.ROI_maps4D = []
        self.datastack.fit_maps4D = []
        self.datastack.ROI_maps4D_calculated = 0        

        
        self.datastack.fits_calculated = 0
        self.datastack.fits4D_calculated = 0
        self.datastack.fit_maps = []
        self.datastack.fitmap_names = []
        
                
        self.datastack.ROI_maps_calculated = 0        
        self.datastack.ROI_info = []
        self.datastack.ROI_names = []
        self.datastack.ROI_maps = []
        
       
        self.datastack.fits_calculated = []
        self.datastack.fit_maps = []
        self.datastack.fit_chanel_names = []
        
        
        self.datastack.engcal_slope = 10.
        self.datastack.engcal_offset = 0.
        
        self.datastack.mp_outdir = ''
        self.datastack.fileformat = ''

        self.window().page1.showmaps = 0
        
        self.UpdateDisplay()
        
        self.window().page1.UpdateDisplay()
        self.window().page4.UpdateDisplay()
        self.window().page2.UpdateDisplay()
        
        self.button_iochnorm.setEnabled(False)
        
                
#----------------------------------------------------------------------        
    def ShowImage(self):
        
        if self.datastack.stack_loaded == 0:
            fig = self.absimgfig
            fig.clf()   
            self.AbsImagePanel.draw()
            return
             
        
        self.iev = 1000
        
        #image = self.datastack.stack[:,:,int(self.iev)].copy() 
        
        
        #image = np.sum(np.asarray(self.datastack.stack, dtype=np.int64), axis=2)
        image = np.sum(self.datastack.stack, axis=2)
        
        
        
        if self.show_log == 1:
            image = np.log(image)

        fig = self.absimgfig
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes.imshow(image.T, cmap=matplotlib.cm.get_cmap("gray"), interpolation='none', origin='lower') 
         
        if self.show_scale_bar == 1:
            #Show Scale Bar
            scale_bar_pixels_y = int(0.01*self.datastack.stack_ncols)
            
            if scale_bar_pixels_y < 1:
                scale_bar_pixels_y = 1
                
            startx = int(self.datastack.stack_nrows*0.05)
            if scale_bar_pixels_y > 1:
                #starty = self.datastack.stack_nrows-int(self.datastack.stack_nrows*0.05)-self.scale_bar_pixels_y
                starty = int(self.datastack.stack_nrows*0.05)+self.scale_bar_pixels_y
            else:
                #starty = self.datastack.stack_nrows-int(self.datastack.stack_nrows*0.05)-2
                starty = int(self.datastack.stack_nrows*0.05)+2
            um_string = ' $\mathrm{\mu m}$'
            microns = '$'+self.scale_bar_string+' $'+um_string
            axes.text(self.scale_bar_pixels_x+startx+1,starty+0.5, microns, horizontalalignment='left', verticalalignment='center',
                      color = 'black', fontsize=14)
            #Matplotlib has flipped scales so I'm using rows instead of cols!
            p = matplotlib.patches.Rectangle((startx,starty), self.scale_bar_pixels_x, self.scale_bar_pixels_y,
                                   color = 'black', fill = True)
            axes.add_patch(p)
             
        
        axes.axis("off")      
        self.AbsImagePanel.draw()
         
        #self.tc_imageeng.setText('Image at energy: {0:5.2f} eV'.format(float(self.stk.ev[self.iev])))


#----------------------------------------------------------------------   
    def scale_bar(self): 
        
           
        x_start = np.amin(self.datastack.realX)
        x_stop = np.amax(self.datastack.realX)
        
        if x_start == x_stop :
            self.scale_bar_pixels_x = 1
            self.scale_bar_pixels_y = 1
            self.scale_bar_string = '0'
            return
        
                
        bar_microns = 0.2*np.abs(x_stop-x_start)
        
        
        if bar_microns >= 10.:
            bar_microns = 10.*int(0.5+0.1*int(0.5+bar_microns))
            bar_string = str(int(0.01+bar_microns)).strip()
        elif bar_microns >= 1.:      
            bar_microns = float(int(0.5+bar_microns))
            if bar_microns == 1.:
                bar_string = '1'
            else:
                bar_string = str(int(0.01+bar_microns)).strip()
        else:
            bar_microns = np.maximum(0.1*int(0.5+10*bar_microns),0.1)
            bar_string = str(bar_microns).strip()
            
        self.scale_bar_string = bar_string


        self.scale_bar_pixels_x = int(0.5+float(self.datastack.stack_ncols)*
                       float(bar_microns)/float(abs(x_stop-x_start)))
        
        self.scale_bar_pixels_y = int(0.01*self.datastack.stack_ncols)
        
        if self.scale_bar_pixels_y < 1:
                self.scale_bar_pixels_y = 1
                
                        
""" ------------------------------------------------------------------------------------------------"""        
class OnlineDataDisplay(QtGui.QDialog):

    def __init__(self, parent, datastack):    
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        self.datastack = datastack
        
        self.monitor = 0
        self.processed_files = []
        
        
        self.resize(500, 600)
        self.setWindowTitle('Online Data Monitor')
        
        pal = QtGui.QPalette()
        self.setAutoFillBackground(True)
        pal.setColor(QtGui.QPalette.Window,QtGui.QColor('white'))
        self.setPalette(pal)        
                            
        
        #panel 1
        sizer1 = QtGui.QGroupBox('Folder')
        vbox1 = QtGui.QVBoxLayout()
        
        hbox11 = QtGui.QHBoxLayout()
        button_selectfolder = QtGui.QPushButton('Select folder')
        button_selectfolder.clicked.connect(self.OnSelectFolder) 
        button_selectfolder.setMaximumWidth(200)  
        hbox11.addWidget(button_selectfolder)
 
        button_start = QtGui.QPushButton('Start')
        button_start.clicked.connect(self.OnStart) 
        button_start.setMaximumWidth(200)  
        hbox11.addWidget(button_start)
 
        button_stop = QtGui.QPushButton('Stop')
        button_stop.clicked.connect(self.OnStop) 
        button_stop.setMaximumWidth(200)  
        hbox11.addWidget(button_stop)
 
        vbox1.addLayout(hbox11)
   
        self.tc_folder = QtGui.QLabel(self)
        vbox1.addWidget(self.tc_folder)
        self.tc_folder.setText('Monitored folder: '+ self.datastack.monitored_folder)
        
        self.tc_mon = QtGui.QLabel(self)
        vbox1.addWidget(self.tc_mon)
        self.tc_mon.setText('Monitoring not running')
        
        vbox1.setContentsMargins(20,20,20,20)
        sizer1.setLayout(vbox1)
        
        #panel 2     
        vbox2 = QtGui.QVBoxLayout()
        
        self.tc_imageeng = QtGui.QLabel(self)
        self.tc_imageeng.setText(" ")
        vbox2.addWidget(self.tc_imageeng)
        

        hbox21 = QtGui.QHBoxLayout()
        
        frame = QtGui.QFrame()
        frame.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        fbox = QtGui.QHBoxLayout()
   
        self.absimgfig = Figure((PlotH*1.2, PlotH*1.2))
        self.AbsImagePanel = FigureCanvas(self.absimgfig)
        self.AbsImagePanel.setParent(self)
        
        fbox.addWidget(self.AbsImagePanel)
        frame.setLayout(fbox)
        hbox21.addWidget(frame)
        
       
        vbox2.addLayout(hbox21)
        
        
       
        vboxtop = QtGui.QVBoxLayout()
        
        hboxtop = QtGui.QHBoxLayout()
        
        hboxtop.addStretch (1)
        hboxtop.addLayout(vbox2)
        hboxtop.addStretch (1)
        
        vboxtop.addStretch (0.5)
        vboxtop.addLayout(hboxtop)
        vboxtop.addStretch (1)
        vboxtop.addWidget(sizer1)
        vboxtop.addStretch (0.5)


        vboxtop.setContentsMargins(20,20,20,20)
        self.setLayout(vboxtop)
        
        
#----------------------------------------------------------------------          
    def OnSelectFolder(self, event): 
                
        directory = QtGui.QFileDialog.getExistingDirectory(self, "Choose a directory", '', QtGui.QFileDialog.ShowDirsOnly|QtGui.QFileDialog.ReadOnly )
   
        if directory == '':
            return
             
        self.datastack.monitored_folder = directory
        self.tc_folder.setText('Monitored folder: '+ self.datastack.monitored_folder)
        
        
#----------------------------------------------------------------------          
    def OnStart(self, event): 
        
        self.tc_mon.setText('Monitoring running')
        
        self.monitor = 1
        
        if os.path.isdir(self.datastack.monitored_folder):
            print self.datastack.monitored_folder, ' exists'
            
        else:
            print self.datastack.monitored_folder, ' does not exist'
            return
        
        #Ignore old filenames
        dirList=os.listdir(self.datastack.monitored_folder)
        filename = ''
        for fname in dirList:
            if (fname[-4:] == '.nxs') : 
                if fname not in self.processed_files:
                    self.processed_files.append(fname)
     

        while self.monitor == 1:
            #Check if we have a new file in the directory
            
            dirList=os.listdir(self.datastack.monitored_folder)
            filename = ''
            for fname in dirList:
                if (fname[-4:] == '.nxs') : 
                    if fname not in self.processed_files:
                        filename = fname
                        break
            
            if filename != '':
                self.ShowImage(os.path.join(self.datastack.monitored_folder,filename))
                self.processed_files.append(filename)

            QtGui.qApp.processEvents()
            
            time.sleep(1.0)
        
#----------------------------------------------------------------------          
    def OnStop(self, event): 
        
        self.tc_mon.setText('Monitoring stopped')
        
        self.monitor = 0
        
        
#----------------------------------------------------------------------          
    def ShowImage(self, filename): 
        
        print 'showing', filename
        Ch5 = file_dls_nexus_hdf5.Ch5()       
        stack, fullSpectrum, dScanPars, realX, roimaps, roiinfo = Ch5.read_nxs(filename)
        
        image = np.sum(stack, axis=2)
        

        fig = self.absimgfig
        fig.clf()
        fig.add_axes(((0.0,0.0,1.0,1.0)))
        axes = fig.gca()
        fig.patch.set_alpha(1.0)
         
        im = axes.imshow(image.T, cmap=matplotlib.cm.get_cmap("gray"), interpolation='none') 
         
             
        
        axes.axis("off")      
        self.AbsImagePanel.draw()
        
""" ------------------------------------------------------------------------------------------------"""
class MainFrame(QtGui.QMainWindow):
    
    def __init__(self):
        super(MainFrame, self).__init__()
        
        self.initUI()
        


#----------------------------------------------------------------------          
    def initUI(self):  

        
        self.datastack = DataStack()
        
        self.Canalysis = analysis.roi_defs()
        
        self.ReadSettingsFile()
        
        self.resize(Winsizex, Winsizey)
        self.setWindowTitle('Mantis XRF v. '+version)  
        
               

        tabs = QtGui.QTabWidget()
        

        
        # create the page windows as tabs
        self.page0 = PageLoadData(self.datastack)
        self.page1 = PageMaps(self.datastack)
        self.page2 = PageROI(self.datastack, self.Canalysis)
        #self.page3 = PageMapsPy(self.datastack)
        self.page4 = PageElements(self.datastack, self.Canalysis)
        self.page5 = PageFit(self.datastack)
        if havexrftomo:
            self.page6 = PageXT(self.datastack)


        
        tabs.addTab(self.page0,"Load Data")
        tabs.addTab(self.page4,"Elements")
        tabs.addTab(self.page2,"ROI Analysis")
        #tabs.addTab(self.page3,"MapsPy")
        tabs.addTab(self.page5,"Spectrum Fitting")
        tabs.addTab(self.page1,"View Maps")
        if havexrftomo:
            tabs.addTab(self.page6,"XRF Tomo")
        

    
        layout = QVBoxLayout()
 
        layout.addWidget(tabs)
         
        mainWidget = QtGui.QWidget()
        mainWidget.setLayout(layout)
        mainWidget.setContentsMargins(0, 0, 0, 0)
        mainWidget.layout().setContentsMargins(0, 0, 0, 0)
         
 
        scrollWidget = QtGui.QScrollArea()
        scrollWidget.setWidget(mainWidget)
        scrollWidget.setWidgetResizable(True) 
 
        self.setCentralWidget(scrollWidget)


        # self.setCentralWidget(tabs)
        
        self.tabs = tabs
 
        
        self.show()
        
        
        
#----------------------------------------------------------------------          
    def ReadSettingsFile(self):  

        #Read settings from a file
        SettingsFileName = 'MantisXRF_settings.txt'
        try:
            f = open(SettingsFileName, 'rt')
            for line in f:
                
                if line.startswith('#'):
                    continue
    
                if 'ROI_ELEMENTS' in line : 
                    slist = line.split(':')
                    value = ''.join(slist[1:])
                    self.datastack.elementstofit = value.split(',')
                    
                if 'MONITORED_FOLDER' in line : 
                    slist = line.split(':')
                    value = ':'.join(slist[1:])
                    value = value.strip()
                    slist = value.split('\\')
                    value = os.path.join(slist[0],os.sep,*slist[1:])
                    #os.path.join(os.sep, 'usr', 'lib')
                    self.datastack.monitored_folder = value
                    
                if 'FS_TITLE' in line : 
                    slist = line.split(':')
                    self.datastack.fontsize_title = int(''.join(slist[1:]))
                    
                if 'FS_AXISLABELS' in line : 
                    slist = line.split(':')
                    self.datastack.fontsize_labels = int(''.join(slist[1:]))  
                    
                if 'ENG_SLOPE' in line : 
                    slist = line.split(':')
                    self.datastack.engcal_slope = float(''.join(slist[1:]))

                if 'ENG_OFFSET' in line : 
                    slist = line.split(':')
                    self.datastack.engcal_offset = float(''.join(slist[1:]))
                    
                if 'ANKA_GLITCH' in line : 
                    slist = line.split(':')
                    self.datastack.anka_glitch = int(''.join(slist[1:]))
                    
                    
            for i in range(len(self.datastack.elementstofit)):
                self.datastack.elementstofit[i] = self.datastack.elementstofit[i].strip()
                if ('_K' not in self.datastack.elementstofit[i]) and ('_L' not in self.datastack.elementstofit[i]) and ('_M' not in self.datastack.elementstofit[i]):
                    self.datastack.elementstofit[i] = self.datastack.elementstofit[i] + '_K'
                        
            f.close()
        except:
            print ' Could not read settings from MantisXRF_settings.txt file, using defaults.'
            return
        
                        
""" ------------------------------------------------------------------------------------------------"""
                        
def main():
    
    app = QtGui.QApplication(sys.argv)
    frame = MainFrame()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()