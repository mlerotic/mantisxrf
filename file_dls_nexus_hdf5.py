'''
Created on Feb 7, 2014

@author: Mirna Lerotic, 2nd Look Consulting
http://2ndlookconsulting.com
'''

from __future__ import division

import os
import numpy as np
import h5py 
import xml.etree.ElementTree as ET


from PIL import Image        
        
class TiffStackWrapper:
    def __init__(self,fname):
        '''fname is the full path '''
        self.im  = Image.open(fname)
        
        self.im.seek(0)
        # get image dimensions from the meta data the order is flipped
        # due to row major v col major ordering in tiffs and numpy
        self.im_sz = [self.im.tag[0x101][0],
                      self.im.tag[0x100][0]]
        self.cur = self.im.tell()

    def get_mode(self):
        return self.im.mode
        
    def get_frame(self,j):
        '''Extracts the jth frame from the image sequence.
        if the frame does not exist return None'''
        try:
            self.im.seek(j)
        except EOFError:
            return None

        self.cur = self.im.tell()
        return np.reshape(self.im.getdata(),self.im_sz)
    
    def __iter__(self):
        self.im.seek(0)
        self.old = self.cur
        self.cur = self.im.tell()
        return self

    def next(self):
        try:
            self.im.seek(self.cur)
            self.cur = self.im.tell()+1
        except EOFError:
            self.im.seek(self.old)
            self.cur = self.im.tell()
            raise StopIteration
        return np.reshape(self.im.getdata(),self.im_sz)  


#----------------------------------------------------------------------
class Ch5:
    def __init__(self):
        
        self.ROI_info = []        
        self.ROI_maps = []
        
        self.darkspot = 0.166

    
#----------------------------------------------------------------------
    def read_nxs(self, filename):
        
        print 'getting data from DSL file', filename
        
       
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
    
        if 'entry1' in f:
            e1Grp = f['entry1'] 
            
                        
            if 'xml' in e1Grp:
                xmlGrp = e1Grp['xml']
                
                if 'ScanParameters' in xmlGrp:
                    scanpars = xmlGrp['ScanParameters']
                    scanpars = scanpars[...]
                    scanpars = scanpars[0]          

                    root = ET.fromstring(scanpars)
                    keys = []
                    values = []
                    for child_of_root in root:
                        keys.append(child_of_root.tag.strip())
                        values.append(child_of_root.text.strip())
                        
                    dScanPars = dict(zip(keys, values))
                    #print dScanPars
                    
                if 'OutputParameters' in xmlGrp:
                    outpars = xmlGrp['OutputParameters']
                    outpars = outpars[...]
                    outpars = outpars[0]          

                    root = ET.fromstring(outpars)
                    keys = []
                    values = []
                    for child_of_root in root:
                        keys.append(child_of_root.tag.strip())
                        values.append(child_of_root.text.strip())
                        
                    dOutPars = dict(zip(keys, values))
                    #print dOutPars
                                        
                    
                if 'DetectorParameters' in xmlGrp:
                    detpars = xmlGrp['DetectorParameters']
                    detpars = detpars[...]
                    detpars = detpars[0]          

                    root = ET.fromstring(detpars)
                    keys = []
                    values = []
                    ndetgrp = 1
                    for child_of_root in root:
                        if child_of_root.text.strip() != '':
                            keys.append(child_of_root.tag.strip())
                            values.append(child_of_root.text.strip())
                        else:
                            nionch = 1
                            apstr1 = ''
                            if child_of_root.tag.strip() == 'detectorGroup':
                                apstr1 = str(ndetgrp)
                                ndetgrp += 1
                            ndet = 1
                            for gchild_of_root in child_of_root:
                                apstr2 = ''
                                if gchild_of_root.tag.strip() == 'detector':
                                    apstr2 = str(ndet)
                                    ndet += 1
                                if gchild_of_root.text.strip() != '':
                                    keys.append(child_of_root.tag.strip()+apstr1+'/'+gchild_of_root.tag.strip()+apstr2)
                                    values.append(gchild_of_root.text.strip())
                                else:
                                    apstr3 = ''
                                    if gchild_of_root.tag.strip() == 'ionChamber':
                                        apstr3 = str(nionch)
                                        nionch += 1                                                                                
                                    for ggchild_of_root in gchild_of_root:
                                        if ggchild_of_root.text.strip() != '':
                                            keys.append(child_of_root.tag.strip()+'/'+gchild_of_root.tag.strip()+apstr3+'/'+ggchild_of_root.tag.strip())
                                            values.append(ggchild_of_root.text.strip())
                                                                                

                 
                    dDetPars = dict(zip(keys, values))
                    #print dDetPars
                    
                if 'SampleParameters' in xmlGrp:
                    samplepars = xmlGrp['SampleParameters']
                    samplepars = samplepars[...]
                    samplepars = samplepars[0]          

                    root = ET.fromstring(samplepars)
                    keys = []
                    values = []
                    for child_of_root in root:
                        if child_of_root.text.strip() != '':
                            keys.append(child_of_root.tag.strip())
                            values.append(child_of_root.text.strip())
                        else:
                            for gchild_of_root in child_of_root:
                                if gchild_of_root.text.strip() != '':
                                    keys.append(child_of_root.tag.strip()+'/'+gchild_of_root.tag.strip())
                                    values.append(gchild_of_root.text.strip())


                 
                    dSamlePars = dict(zip(keys, values))
                    #print dSamlePars
                    
                #ROI description array:
                #[detectorelementnum, name, windowstart, windowend, counts]
                ROIS = []
                if 'VortexParameters' in xmlGrp:
                    vtxpars = xmlGrp['VortexParameters']
                    vtxpars = vtxpars[...]
                    vtxpars = vtxpars[0]          

                    root = ET.fromstring(vtxpars)
                    keys = []
                    values = []
                    ndetel = 1
                    for child_of_root in root:
                        if child_of_root.text.strip() != '':
                            keys.append(child_of_root.tag.strip())
                            values.append(child_of_root.text.strip())
                        else:
                            apstr1 = ''
                            nroi = 1
                            if child_of_root.tag.strip() == 'DetectorElement':
                                apstr1 = str(ndetel)
                                ndetel += 1
                            for gchild_of_root in child_of_root:
                                if gchild_of_root.text.strip() != '':
                                    keys.append(child_of_root.tag.strip()+apstr1+'/'+gchild_of_root.tag.strip())
                                    values.append(gchild_of_root.text.strip())
                                else:
                                    apstr2 = ''
                                    if gchild_of_root.tag.strip() == 'ROI':
                                        apstr2 = str(nroi)
                                        nroi += 1
                                    for ggchild_of_root in gchild_of_root:
                                        if ggchild_of_root.text.strip() != '':
                                            keys.append(child_of_root.tag.strip()+apstr1+'/'+gchild_of_root.tag.strip()+apstr2+'/'+ggchild_of_root.tag.strip())
                                            values.append(ggchild_of_root.text.strip())   
                                    if gchild_of_root.tag.strip() == 'ROI':
                                        roivals = [ndetel-1]
                                        for ggchild_of_root in gchild_of_root:
                                            roivals.append(ggchild_of_root.text.strip())
                                        ROIS.append(roivals)
                    
                    dVortexPars = dict(zip(keys, values))
                    #print dVortexPars
                    #print ROIS

            if 'xmapMca' in e1Grp:
                xmapmcaGrp = e1Grp['xmapMca']    
                
                self.roinames = []
                for i in range(len(ROIS)):
                    if  ROIS[i][1] not in self.roinames:
                        self.roinames.append(ROIS[i][1])
                        
                for i in range(len(self.roinames)):
                    if self.roinames[i] in xmapmcaGrp:
                        roimap = xmapmcaGrp[self.roinames[i]]
                        self.ROI_maps.append(roimap[...])                         
                        
                         
                if 'allElementSum' in xmapmcaGrp:
                    allElementSum = xmapmcaGrp['allElementSum']
                    allElementSum = allElementSum[...]                    
                
                if 'fullSpectrum' in xmapmcaGrp:
                    fullSpectrum = xmapmcaGrp['fullSpectrum']
                    fullSpectrum = fullSpectrum[...] 
                    
                if 'icr' in xmapmcaGrp:
                    icr = xmapmcaGrp['icr']
                    icr = icr[...] 
                    
                if 'ocr' in xmapmcaGrp:
                    ocr = xmapmcaGrp['ocr']
                    ocr = ocr[...] 
                                                            
                if 'realX' in xmapmcaGrp:
                    realX = xmapmcaGrp['realX']
                    realX = realX[...] 
                    
                if 'realY' in xmapmcaGrp:
                    realY = xmapmcaGrp['realY']
                    realY = realY[...] 
                else:
                    realY = np.array([])
                                                        
                        
        # Close
        f.close()
        
        self.ROI_info = ROIS

        if len(ROIS) > 0:
            nelements = len(self.ROI_maps)
            dims = self.ROI_maps[1].shape
            width = dims[0]
            height = dims[1]
            ndetectors = dims[2]
            
            
            ROImaps = np.zeros((nelements,width,height,ndetectors))
            for i in range(nelements):
                ROImaps[i,:,:,:] = self.ROI_maps[i]
            
            #ROI_info description array:
            #[detectorelementnum, name, windowstart, windowend, counts]
            ROIinfo = []
            
            for item in self.ROI_info:
                ROIinfo.append(item)
            
            
        else:
            ROImaps = []
            ROIinfo = []
        
        #print fullSpectrum.dtype, allElementSum.dtype
        
        realX = realX[0,:]
        
#         print fullSpectrum.shape
#         print allElementSum.shape
#         print realX.shape
#         print realY.shape
                
        return allElementSum, fullSpectrum, float(dScanPars['energy']), realX, realY, ROImaps, ROIinfo
    
    
#----------------------------------------------------------------------
    def read_i08_nxs(self, filename):
        
        print 'reading', filename
        
        absimage = []
        dfimage = []
        data = []
        
        havedata = 0
        
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
        
        if 'entry1' in f:
            e1Grp = f['entry1'] 
            
            if '_andorrastor' in e1Grp:
                andrGrp = e1Grp['_andorrastor']
                
                if 'data' in andrGrp:
                    data = andrGrp['data']
                    data = data[...]   
                    havedata = 1
        
        if havedata == 1:
            dims = data.shape
            ncols = data.shape[0]
            nrows = data.shape[1]
            absimage = np.zeros((ncols,nrows))
            
            
            #Calculate dark field image
            npix = data.shape[3]
            
            y,x = np.ogrid[-(npix-1)/2:npix/2, -(npix-1)/2:npix/2]
            mask = x*x + y*y >= (npix*self.darkspot)*(npix*self.darkspot)
            
            
            dfimage = np.zeros((ncols,nrows))
            for k in range(ncols):
                for l in range(nrows):
                    thispixel = data[k,l,:,:]
                    dfimage[k,l] = np.sum(thispixel[mask])
                    absimage[k,l] = np.sum(thispixel)
               
            xpcimage = np.zeros((ncols,nrows))
            ypcimage = np.zeros((ncols,nrows))
            x, y = np.mgrid[-(npix-1)/2:npix/2, -(npix-1)/2:npix/2]
            x = x*2/npix
            y = y*2/npix

            for k in range(ncols):
                for l in range(nrows):
                    thispixel = data[k,l,:,:]
                    xpcimage[k,l] = np.sum(x*thispixel)
                    ypcimage[k,l] = np.sum(y*thispixel)

            
        else:
            return [],[], [], []
        
#        print np.sum(absimage)
#        print np.sum(dfimage)
        
        return absimage, dfimage, xpcimage, ypcimage
    
    
#----------------------------------------------------------------------
    def save_i08_nxs(self, nfilename, hfilename, datastack):
                   
        #Save angor file
        print 'Saving .hdf file: ', hfilename
        # Open HDF5 file
        f = h5py.File(hfilename, 'w')
        entryGrp = f.create_group('entry')
        entryGrp.attrs['NX_class'] = 'NXentry'
        insGrp = entryGrp.create_group('instrument')
        insGrp.attrs['NX_class'] = 'NXinstrument'
        detGrp = insGrp.create_group('detector')
        detGrp.attrs['NX_class'] = 'NXdetector'
        
        ds_data = detGrp.create_dataset('data', data = datastack.astype(np.uint16))
        ds_data.attrs['NX_class'] = 'SDS'     
        ds_data.attrs['dim0'] = 'scan dimension X'
        ds_data.attrs['dim1'] = 'frame number n'
        ds_data.attrs['dim2'] = 'NDArray Dim1'
        ds_data.attrs['dim3'] = 'NDArray Dim0'
        ds_data.attrs['interpretation'] = 'image'
        ds_data.attrs['signal'] = 1
      
        f.close()
        
        #Save data to nxs file
        print 'Saving .nxs file: ', nfilename
        f = h5py.File(nfilename, 'w')
        entryGrp = f.create_group('entry1')
        entryGrp.attrs['NX_class'] = 'NXentry'        
        
        astGrp = entryGrp.create_group('_andorrastor')
        astGrp.attrs['NX_class'] = 'NXdata'
        
        astGrp['data'] = h5py.ExternalLink(hfilename, "/entry/instrument/detector/data")
        
        f.close()
    
#----------------------------------------------------------------------
    def read_i08_tif_stack(self, filename):
        
        tiffstack = TiffStackWrapper(filename)
        mode = tiffstack.get_mode()
        if mode == 'I;16B':
            imgmode = 16
        else:
            imgmode = 8
        
        frame0 = tiffstack.get_frame(0)
        imgstack = np.array((frame0))
          
        haveimg = True
        it = 1
        while haveimg:
            frame = tiffstack.get_frame(it)
            if frame == None:
                haveimg = False
            else:
                it+=1
                imgstack = np.dstack((imgstack,frame))
                
        if imgmode == 16:
            imgstack = imgstack.astype(np.uint16)
        else:
            imgstack = imgstack.astype(np.uint8)
            
        n_cols = imgstack.shape[0]
        n_rows = imgstack.shape[1]
        n_imgs = imgstack.shape[2]
        
        #print n_cols, n_rows, n_imgs
        #print np.sum(imgstack)
        
        
        return imgstack

# """ ------------------------------------------------------------------------------------------------"""
# def main():
#         
#     fname = 'D:/Python/DLS/dls/data/tiff/test_16bit_grey_1.tif'
#     
#     h5 = Ch5()
#     
#     h5.read_i08_tif_stack(fname)
#     
#     
#         
# #-----------------------------------------------------------------------------   
# if __name__ == '__main__':
#     
#        
#     main()
#         
    
    