#Default elements to use in ROI fitting
#ROI_ELEMENTS: Ti, Co, Cu
ROI_ELEMENTS: Si, P, S, Cl, Ar, K, Ca, Ti, Cr, Mn, Fe, Co, Ni, Cu, Zn, Pt_L, Au_M, Pb_M


#Font sizes for Maps View Tab (Default font size for title: 12, for labels: 8)
FS_TITLE: 12
FS_AXISLABELS: 8

#Energy calibration
ENG_SLOPE: 10.0
ENG_OFFSET: 0.0



