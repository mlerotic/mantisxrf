'''
Created on May 25, 2015

@author: Mirna Lerotic, 2nd Look Consulting
http://2ndlookconsulting.com
'''

from __future__ import division

import numpy as np
import os
import shutil
import h5py
import datetime

#-----------------------------------------------------------------------------
class Cah5:
    def __init__(self):   
        pass

#-----------------------------------------------------------------------------  
    def read_h5(self, filename, multiimg = 0, readimg = 0, fix_glitch = 0): 
        
        print 'getting data from ANKA file: ', filename
        
        inenergy = 12000.0
        realX = 0
        ecmean = 0
        roimaps = []
        roiinfo = []
        
        channum = []
    
        ecurrent = []
        ioncount = []
               
                
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
    

        #Information HDF5 group
        if 'data' in f:
            datagrp = f['data']
                 
                        
            if 'TXM' in datagrp:
                tgrp = datagrp['TXM']
                
                
                if 'processed' in datagrp:
                    prcgrp = datagrp['processed']
                     
                    if 'sensor1d' in prcgrp:
                        s1grp = prcgrp['sensor1d']
                         
                        datadet = []
                        nv = 0
                        #Read vortex_n entries
                        for i in range(len(s1grp)):
                            vname = 'vortex_'+str(nv)
                            if vname in s1grp:
                                dsx = s1grp[vname]
                                thisdata = dsx[...]
                                if multiimg > 0 :
                                    datadet.append(thisdata[readimg,:,:,:])
                                else:
                                    datadet.append(thisdata[:,:,:])
                                nv +=1            
                                    
        
                if 'actuator' in tgrp:
                    agrp = tgrp['actuator']

                    if 'energy' in agrp:                   
                        dsx = agrp['energy']
                        engarray = dsx[...]
                        if multiimg > 0 :
                            inenergy = engarray[0,0]
                            inenergy = inenergy[0]
                        else:
                            inenergy = engarray[0,0]
                        
                    if 'sample_x' in agrp:                   
                        dsx = agrp['sample_x']
                        sample_x = dsx[...]
                        if multiimg > 0 :
                            sample_x = sample_x[readimg,:,:]

                    if 'sample_y' in agrp:                   
                        dsx = agrp['sample_y']
                        sample_y = dsx[...]
                        if multiimg > 0 :
                            sample_y = sample_y[readimg,:,:]       

                    realX = sample_x[0,:]    
                    realY = sample_y[:,0]         

                if 'machine' in tgrp:
                    mgrp = tgrp['machine']    
                    if 'ecurrent' in mgrp:   
                        dsx = mgrp['ecurrent']
                        ecurrent = dsx[...] 
                        ecurrent = np.array(ecurrent)
                        if multiimg > 0 :
                            ecurrent = ecurrent[readimg,:,:]
                        ecurrent = ecurrent.T
                        ecmean = np.mean(ecurrent)
                        ecurrent = 100.0/ecurrent
                        
                    if 'ioncount' in mgrp:   
                        dsx = mgrp['ioncount']
                        ioncount = dsx[...] 
                        ioncount = np.array(ioncount)
                        if multiimg > 0 :
                            ioncount = ioncount[readimg,:,:]
                        ioncount = ioncount.T

        

                                                    
            if 'LikeSpec' in datagrp:
                lsgrp = datagrp['LikeSpec']                

                if 'processed' in datagrp:
                    prcgrp = datagrp['processed']
                     
                    if 'sensor1d' in prcgrp:
                        s1grp = prcgrp['sensor1d']
                         

                        nv = 0
                        #Read xmap_sumn entries
                        vname = 'xmap_sum0'
                        dsx = s1grp[vname]
                        datadet = dsx[...]                                 

                if 'Channel' in lsgrp:                   
                    dsx = lsgrp['Channel']
                    channum = dsx[...]

                    
                if 'dim_0' in lsgrp:                   
                    dsx = lsgrp['dim_0']
                    sample_x = dsx[...]

                if 'dim_1' in lsgrp:                   
                    dsx = lsgrp['dim_1']
                    sample_y = dsx[...]
                    
                if 'initial_state' in lsgrp:
                    isgrp = lsgrp['initial_state']
                    if 'actuators' in isgrp:
                        agrp = isgrp['actuators']                    
                        if 'Energy' in agrp:                   
                            dsx = agrp['Energy']
                            inenergy = dsx[...]
                            inenergy = inenergy[0]*1000.
                                        

                realX = sample_x[:]    
                realY = sample_y[:]
 

                
        # Close
        f.close()  
        
        
        
        fullSpectrum = np.array(datadet)
        
        
        if fullSpectrum.ndim > 3:
            fullSpectrum = np.transpose(fullSpectrum, (2,1,0,3))
        else:
            fullSpectrum = np.transpose(fullSpectrum, (2,1,0))
            
            

                
        #Transpose the arrays
        temp = realX    
        realX = realY 
        realY = temp        
     
#        
#        print 'Energy calibration:'
#        print 'Slope:', slope
#        print 'Offset:', offset

        print 'Incident energy', inenergy
        
        
        if ecurrent == []:
            dims = fullSpectrum.shape
            ecurrent = np.ones((dims[0], dims[1]))
            print 'Ecurrent not loaded from .hdf5 file - using 1s.'
            
        
        if len(fullSpectrum.shape) > 3:
            allElementSum = np.sum(np.asarray(fullSpectrum, dtype=np.float64), axis=2)
            #allElementSum = fullSpectrum[:,:,3,:]
        else:
            allElementSum = fullSpectrum.copy() 
            
            
        if fix_glitch == 1:
            print 'Fixing first 2 pixels glitch'
            
            fullSpectrum = np.delete(fullSpectrum, 0, axis = 1)
            allElementSum = np.delete(allElementSum, 0, axis = 1)    
            ecurrent = np.delete(ecurrent, 0, axis = 1)    
            realY = realY[1:]    
            if len(list(ioncount)) > 0:
                ioncount = np.delete(ioncount, 0, axis = 1)   
            
#             print fullSpectrum.shape
#             print allElementSum.shape
#             print realX.shape
#             print realY.shape  
                  
        #print 'Values for first two pixels:', np.sum(allElementSum[0,0,:]), np.sum(allElementSum[1,0,:]) 
        #print 'Average pixel value:', np.sum(allElementSum)/(allElementSum.shape[0]*allElementSum.shape[1])
                

        
        return allElementSum, fullSpectrum, inenergy, realX, realY, roimaps, roiinfo, channum, ecurrent, ecmean, ioncount
    

#-----------------------------------------------------------------------------  
    def read_h5_4D(self, filename, fix_glitch = 0): 
        
        print 'getting xrf-tomo data from ANKA file: ', filename
        
        inenergy = 12000.0
        realX = 0
        roimaps = []
        roiinfo = []
        
        channum = []
        
        ecmean = 0
    
        ecurrent = []
        ecurrent4D = []
        theta = []
        
        ioncount = []
               
                
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
    

        #Information HDF5 group
        if 'data' in f:
            datagrp = f['data']
                 
                        
            if 'TXM' in datagrp:
                tgrp = datagrp['TXM']
                
                
                if 'processed' in datagrp:
                    prcgrp = datagrp['processed']
                     
                    if 'sensor1d' in prcgrp:
                        s1grp = prcgrp['sensor1d']
                         
                        datadet = []
                        nv = 0
                        #Read vortex_n entries
                        for i in range(len(s1grp)):
                            vname = 'vortex_'+str(nv)
                            if vname in s1grp:
                                dsx = s1grp[vname]
                                thisdata = dsx[...]
                                datadet.append(thisdata[:,:,:,:])
                            nv +=1 

                                   
                                    
        
                if 'actuator' in tgrp:
                    agrp = tgrp['actuator']

                    if 'energy' in agrp:                   
                        dsx = agrp['energy']
                        engarray = dsx[...]
                        inenergy = engarray[0,0]
                        inenergy = inenergy[0]

                    if 'sample_t' in agrp:                   
                        dsx = agrp['sample_t']
                        sample_t = dsx[...]
                        sample_t = sample_t[:,0,:]
                        print 'Angles: ', sample_t[:,0]
                        theta = sample_t[:,0]
                        
                    if 'sample_x' in agrp:                   
                        dsx = agrp['sample_x']
                        sample_x = dsx[...]
                        sample_x = sample_x[0,:,:]

                    if 'sample_y' in agrp:                   
                        dsx = agrp['sample_y']
                        sample_y = dsx[...]
                        sample_y = sample_y[0,:,:]       

                    realX = sample_x[0,:]    
                    realY = sample_y[:,0]         

                if 'machine' in tgrp:
                    mgrp = tgrp['machine']    
                    if 'ecurrent' in mgrp:   
                        dsx = mgrp['ecurrent']
                        ecurrent4D = dsx[...] 
                        ecurrent4D = np.array(ecurrent4D)
                        ecurrent = ecurrent4D[0,:,:]
                        ecurrent = ecurrent.T
                        ecmean = np.mean(ecurrent4D)
                        ecurrent = 100.0/ecurrent
                        ecurrent4D = np.transpose(ecurrent4D, (2, 1, 0))
                        ecurrent4D = 100.0/ecurrent4D
                        
                    if 'ioncount' in mgrp:   
                        dsx = mgrp['ioncount']
                        ioncount = dsx[...] 
                        ioncount = np.array(ioncount)
                        ioncount = np.transpose(ioncount, (2, 1, 0))
        

                                                    
            if 'LikeSpec' in datagrp:
                lsgrp = datagrp['LikeSpec']                

                if 'processed' in datagrp:
                    prcgrp = datagrp['processed']
                     
                    if 'sensor1d' in prcgrp:
                        s1grp = prcgrp['sensor1d']
                         

                        nv = 0
                        #Read xmap_sumn entries
                        vname = 'xmap_sum0'
                        dsx = s1grp[vname]
                        datadet = dsx[...]                                 

                if 'Channel' in lsgrp:                   
                    dsx = lsgrp['Channel']
                    channum = dsx[...]

                    
                if 'dim_0' in lsgrp:                   
                    dsx = lsgrp['dim_0']
                    sample_x = dsx[...]

                if 'dim_1' in lsgrp:                   
                    dsx = lsgrp['dim_1']
                    sample_y = dsx[...]
                    
                if 'initial_state' in lsgrp:
                    isgrp = lsgrp['initial_state']
                    if 'actuators' in isgrp:
                        agrp = isgrp['actuators']                    
                        if 'Energy' in agrp:                   
                            dsx = agrp['Energy']
                            inenergy = dsx[...]
                            inenergy = inenergy[0]*1000.
                                        

                realX = sample_x[:]    
                realY = sample_y[:]
 

                
        # Close
        f.close()  
        
        
        
        fullSpectrum = np.array(datadet)
        
        #print fullSpectrum.shape
        
        if fullSpectrum.ndim > 4:
            fullSpectrum4D = np.transpose(fullSpectrum, (3,2,0,4,1))
            fullSpectrum = fullSpectrum4D[:,:,:,:,0]
        else:
            fullSpectrum4D = np.transpose(fullSpectrum, (3,2,0,1))
            fullSpectrum = fullSpectrum4D[:,:,:,0]


                
        #Transpose the arrays
        temp = realX    
        realX = realY 
        realY = temp        
     
#        
#        print 'Energy calibration:'
#        print 'Slope:', slope
#        print 'Offset:', offset

        print 'Incident energy', inenergy
        
        
        if ecurrent == []:
            dims = fullSpectrum.shape
            ecurrent = np.ones((dims[0], dims[1]))
            print 'Ecurrent not loaded from .hdf5 file - using 1s.'
            
        
        if len(fullSpectrum.shape) > 3:
            allElementSum = np.sum(np.asarray(fullSpectrum, dtype=np.float64), axis=2)
            #allElementSum = fullSpectrum[:,:,3,:]
        else:
            allElementSum = fullSpectrum.copy() 
            
        #print fullSpectrum.dtype, allElementSum.dtype

#         print fullSpectrum.shape
#         print allElementSum.shape
#         print realX.shape
#         print realY.shape     


        if fix_glitch == 1:
            print 'Fixing first 2 pixels glitch'
            
            fullSpectrum = np.delete(fullSpectrum, 0, axis = 1)
            fullSpectrum4D = np.delete(fullSpectrum4D, 0, axis = 1)
            allElementSum = np.delete(allElementSum, 0, axis = 1)    
            ecurrent = np.delete(ecurrent, 0, axis = 1)    
            ecurrent4D = np.delete(ecurrent4D, 0, axis = 1)    
            realY = realY[1:]    
            if len(list(ioncount)) > 0:
                ioncount = np.delete(ioncount, 0, axis = 1)    

        
        return allElementSum, fullSpectrum, fullSpectrum4D, theta, inenergy, realX, realY, roimaps, roiinfo, channum, ecurrent, ecurrent4D, ecmean, ioncount 
    
        
    
#----------------------------------------------------------------------
    def convert_anka_to_mapsh5(self, ankafilename, mapsfilename, multiimg, readimg, overwrite = True):
        
        
        # Open HDF5 file
        f = h5py.File(ankafilename, 'r') 
    

        #Information HDF5 group
        if 'data' in f:
            datagrp = f['data']
   
        
            if 'processed' in datagrp:
                prcgrp = datagrp['processed']
                 
                if 'sensor1d' in prcgrp:
                    s1grp = prcgrp['sensor1d']
                     
                    datadet = []
                    nv = 0
                    #Read vortex_n entries
                    for i in range(len(s1grp)):
                        vname = 'vortex_'+str(nv)
                        if vname in s1grp:
                            dsx = s1grp[vname]
                            thisdata = dsx[...]
                            if multiimg > 0 :
                                datadet.append(thisdata[readimg,:,:,:])
                            else:
                                datadet.append(thisdata[:,:,:])
                            nv +=1

                
                        
            if 'TXM' in datagrp:
                tgrp = datagrp['TXM']
                
                #Raw Data
#                 if 'sensor1d' in tgrp:
#                     s1grp = tgrp['sensor1d']
#                     
#                     datadet = []
#                     nv = 0
#                     #Read vortex_n entries
#                     for i in range(len(s1grp)):
#                         vname = 'vortex_'+str(nv)
#                         if vname in s1grp:
#                             dsx = s1grp[vname]
#                             thisdata = dsx[...]
#                             datadet.append(thisdata[2,:,:,:])
#                             nv +=1                
                

                if 'actuator' in tgrp:
                    agrp = tgrp['actuator']

                    if 'energy' in agrp:                   
                        dsx = agrp['energy']
                        engarray = dsx[...]
                        if multiimg > 0 :
                            inenergy = engarray[0,0]
                            inenergy = inenergy[0]
                        else:
                            inenergy = engarray[0,0]
                        
                    if 'sample_x' in agrp:                   
                        dsx = agrp['sample_x']
                        sample_x = dsx[...]
                        if multiimg > 0 :
                            sample_x = sample_x[readimg,:,:]

                    if 'sample_y' in agrp:                   
                        dsx = agrp['sample_y']
                        sample_y = dsx[...]
                        if multiimg > 0 :
                            sample_y = sample_y[readimg,:,:]                
                
        # Close
        f.close()  
        
        fullSpectrum = np.array(datadet)
        fullSpectrum = np.transpose(fullSpectrum, (1,2,0,3))
        
        print sample_x.shape
        print fullSpectrum.shape
        
        
        
        #fullSpectrum = fullSpectrum[:,1:,:,:]
        

        slope = 10.0
        offset = 0.0
        
        print 'Energy calibration:'
        print 'Slope:', slope
        print 'Offset:', offset
        print 'Incident energy', inenergy
        
        realX = sample_x[0,:]    
        realY = sample_y[:,0]

        
        if len(fullSpectrum.shape) > 3:
            allElementSum = np.sum(np.asarray(fullSpectrum, dtype=np.float64), axis=2)
            #allElementSum = fullSpectrum[:,:,3,:]
        else:
            allElementSum = fullSpectrum.copy() 
        
        
        mca_arr = allElementSum

       
        # set compression level where applicable:
        gzip = 5
        file_status = 0
        entry_exists = 0
        
        verbose = 0
        
      
        # test whether a file with this filename already exists:
        try:
            # Open HDF5 file
            f = h5py.File(mapsfilename, 'r')
            if verbose: print 'Have HDF5 file: ', mapsfilename
            file_exists = 1
            file_is_hdf = 1
            file_status = 2       
            
            #MAPS HDF5 group
            if 'MAPS' in f:
                if verbose: print 'MAPS group found in file: ', mapsfilename
                mapsGrp = f['MAPS']
                file_status = 3
                if 'mca_arr' in mapsGrp:
                    if verbose: print 'MAPS\\mca_arr found in file: ', mapsfilename
                    file_status = 4
                # at the moment, simply overwrite the mca_arr section of
                # the file; in the future, may want to test, and only
                # overwrite if specific flag is set.

            f.close()

        except:
            if verbose: print 'Creating new file: ', mapsfilename
            
        if verbose: print 'file_status: ', file_status
        
        if overwrite : file_status = 0
        
        #print mapsfilename
        if file_status <= 1 : 
            f = h5py.File(mapsfilename, 'w')
        else : 
            f = h5py.File(mapsfilename, 'a')

        if file_status <= 3 : 
            # create a group for maps to hold the data
            mapsGrp = f.create_group('MAPS')
            # now set a comment
            mapsGrp.attrs['comments'] = 'This is the group that stores all relevant information created (and read) by the the MAPS analysis software'

        if file_status >= 4 : 
            mapsGrp = f['MAPS']
            entry_exists = 1
            
        if entry_exists == 0:
            # create dataset and save full spectra
            data = np.transpose(mca_arr)
            dimensions = data.shape
            chunk_dimensions = (dimensions[0], 1, 1)
            comment = 'these are the full spectra at each pixel of the dataset'
            ds_data = mapsGrp.create_dataset('mca_arr', data = data, chunks=chunk_dimensions, compression='gzip', compression_opts=gzip)
            ds_data.attrs['comments'] = comment
        else:
            # save the data to existing array
            # delete old dataset, create new and save full spectra
            data = np.transpose(mca_arr)
            dimensions = data.shape
            chunk_dimensions = (dimensions[0], 1, 1)
            comment = 'these are the full spectra at each pixel of the dataset'
            del mapsGrp['mca_arr']
            ds_data = mapsGrp.create_dataset('mca_arr', data = data, chunks=chunk_dimensions, compression='gzip', compression_opts=gzip)
            ds_data.attrs['comments'] = comment
            
        
        
        
        entryname = 'x_axis'
        comment = 'stores the values of the primary fast axis positioner, typically sample x'
        data = realX
        ds_data = mapsGrp.create_dataset(entryname, data = data)
        ds_data.attrs['comments'] = comment

        entryname = 'y_axis'
        comment = 'stores the values of the slow axis positioner, typically sample y'
        data = realY
        ds_data = mapsGrp.create_dataset(entryname, data = data)
        ds_data.attrs['comments'] = comment
        
        
        f.close()
        
        
        return                

    

