'''
Created on Mar 5, 2014

@author: Mirna Lerotic
'''

from __future__ import division

import numpy as np
import csv
import time as tm
import os

import fit_parameters
import spectrum_fitting

from PyQt4 import QtCore, QtGui

def progress(data, *args):
    
    it=iter(data)
    widget = QtGui.QProgressDialog(*args+(0,it.__length_hint__()))
    c=0
    
    for v in it:
        QtCore.QCoreApplication.instance().processEvents()
        if widget.wasCanceled():
            raise StopIteration
        c+=1
        widget.setValue(c)

        yield(v)


#-----------------------------------------------------------------------------
class spectrum:
    def __init__(self, max_spec_channels, max_ICs, mode = 'spec_tool'):
        self.mode = mode
        self.name = ''
        self.data = np.zeros(max_spec_channels)
        self.used_chan = 0
        self.base_xsize = 0
        self.base_ysize = 0
        self.offset_xsize = 0
        self.offset_ysize = 0
        self.used = 0
        self.scan_time_stamp = ''
        self.real_time = 0.
        self.live_time = 0.
        self.srcurrent = 0.
        self.normalized = 0
        self.calib = {'off':0., 'lin':0., 'quad':0.}
        self.date = {'year':0, 'month':0, 'day':0, 'hour':0, 'minute':0, 'second':0}
        self.roi = {'number':0, 'area':0., 'pixels':0.}
        ic = {'cts':0., 'sens_num':0., 'sens_unit':0., 'sens_factor':0.}
        self.IC = [ic for count in range(max_ICs)]
        
#-----------------------------------------------------------------------------
def define_spectra(max_spec_channels, max_spectra, max_ICs, mode = 'spec_tool'):
    
    spectra = [spectrum(max_spec_channels, max_ICs, mode) for count in range(max_spectra)]
    
    return spectra
            
#-----------------------------------------------------------------------------
class element_info:
    def __init__(self):
        self.z = 0    
        self.name = '' 
        self.xrf = {'Ka1':0., 'Ka2':0., 
                    'Kb1':0., 'Kb2':0., 
                    'La1':0., 'La2':0., 'Lb1':0., 'Lb2':0., 'Lb3':0., 'Lb4':0., 'Lb5':0., 
                    'Lg1':0., 'Lg2':0., 'Lg3':0., 'Lg4':0., 'Ll':0., 'Ln':0., 
                    'Ma1':0., 'Ma2':0., 'Mb':0., 'Mg':0. }
        self.xrf_abs_yield = {'Ka1':0., 'Ka2':0., 
                              'Kb1':0., 'Kb2':0., 
                              'La1':0., 'La2':0., 'Lb1':0., 'Lb2':0., 'Lb3':0., 'Lb4':0., 'Lb5':0., 
                              'Lg1':0., 'Lg2':0., 'Lg3':0., 'Lg4':0., 'Ll':0., 'Ln':0., 
                              'Ma1':0., 'Ma2':0., 'Mb':0., 'Mg':0. }     
        self.yieldD = {'k':0., 'l1':0., 'l2':0., 'l3':0., 'm':0. }
        self.density = 1.
        self.mass = 1.
        self.bindingE = {
                         'K':0., 
                         'L1':0., 'L2':0., 'L3':0., 
                         'M1':0., 'M2':0., 'M3':0., 'M4':0., 'M5':0., 
                         'N1':0., 'N2':0., 'N3':0., 'N4':0., 'N5':0., 'N6':0., 'N7':0., 
                         'O1':0., 'O2':0., 'O3':0., 'O4':0., 'O5':0., 
                         'P1':0., 'P2':0., 'P3':0. }
        self.jump = {
                     'K':0., 
                     'L1':0., 'L2':0., 'L3':0., 
                     'M1':0., 'M2':0., 'M3':0., 'M4':0., 'M5':0., 
                     'N1':0., 'N2':0., 'N3':0., 'N4':0., 'N5':0., 
                     'O1':0., 'O2':0., 'O3':0. }
              
              
              
#-----------------------------------------------------------------------------
class Celements:
    def __init__(self):   
        
        pass
  
#-----------------------------------------------------------------------------  
    def get_element_info(self): 
           
        #Number of elements in table
        nels = 100  
        
        els_file = 'xrf_library.csv'
        
        try:
            f = open(els_file, 'r')
            csvf = csv.reader(f, delimiter=',')
        except:
            print 'Error: Could not find xrf_library.csv file! Please get the library file (e.g., from runtime maps at'
            print 'http://www.stefan.vogt.net/downloads.html) and make sure it is in the Python path'
            return None
        
        version = 0.
        for row in csvf:
            if row[0] == 'version:': 
                version = float(row[1])
                break
        if version != 1.2:
            print 'Warning: the only xrf_library.csv file that was found is out of date.  Please use the latest file.'
            print 'A copy can be downloaded, e.g., as part of the runtime maps release available at '
            print 'http://www.stefan.vogt.net/downloads.html'

        element = []
        for i in range(nels):
                element.append(element_info())

                
        rownum = 1 #skip header
        for row in csvf:
            if (row[0]=='version:') or (row[0]=='') or \
                (row[0]=='aprrox intensity') or (row[0]=='transition') or \
                (row[0]=='Z') :
                continue

            i = int(row[0])-1
            
            element[i].z = int(float(row[0]))
            element[i].name = row[1]
            element[i].xrf['ka1'] = float(row[2]) 
            element[i].xrf['ka2'] = float(row[3]) 
            element[i].xrf['kb1'] = float(row[4]) 
            element[i].xrf['kb2'] = float(row[5]) 
            element[i].xrf['la1'] = float(row[6]) 
            element[i].xrf['la2'] = float(row[7]) 
            element[i].xrf['lb1'] = float(row[8]) 
            element[i].xrf['lb2'] = float(row[9]) 
            element[i].xrf['lb3'] = float(row[10]) 
            element[i].xrf['lb4'] = float(row[11]) 
            element[i].xrf['lg1'] = float(row[12]) 
            element[i].xrf['lg2'] = float(row[13]) 
            element[i].xrf['lg3'] = float(row[14]) 
            element[i].xrf['lg4'] = float(row[15]) 
            element[i].xrf['ll'] = float(row[16]) 
            element[i].xrf['ln'] = float(row[17]) 
            element[i].xrf['ma1'] = float(row[18]) 
            element[i].xrf['ma2'] = float(row[19]) 
            element[i].xrf['mb'] = float(row[20]) 
            element[i].xrf['mg'] = float(row[21]) 
            element[i].yieldD['k'] = float(row[22]) 
            element[i].yieldD['l1'] = float(row[23]) 
            element[i].yieldD['l2'] = float(row[24]) 
            element[i].yieldD['l3'] = float(row[25]) 
            element[i].yieldD['m'] = float(row[26]) 
            element[i].xrf_abs_yield['ka1'] = float(row[27]) 
            element[i].xrf_abs_yield['ka2'] = float(row[28]) 
            element[i].xrf_abs_yield['kb1'] = float(row[29]) 
            element[i].xrf_abs_yield['kb2'] = float(row[30]) 
            element[i].xrf_abs_yield['la1'] = float(row[31]) 
            element[i].xrf_abs_yield['la2'] = float(row[32]) 
            element[i].xrf_abs_yield['lb1'] = float(row[33]) 
            element[i].xrf_abs_yield['lb2'] = float(row[34]) 
            element[i].xrf_abs_yield['lb3'] = float(row[35]) 
            element[i].xrf_abs_yield['lb4'] = float(row[36]) 
            element[i].xrf_abs_yield['lg1'] = float(row[37]) 
            element[i].xrf_abs_yield['lg2'] = float(row[38]) 
            element[i].xrf_abs_yield['lg3'] = float(row[39]) 
            element[i].xrf_abs_yield['lg4'] = float(row[40]) 
            element[i].xrf_abs_yield['ll'] = float(row[41]) 
            element[i].xrf_abs_yield['ln'] = float(row[42]) 
            element[i].xrf_abs_yield['ma1'] = float(row[43]) 
            element[i].xrf_abs_yield['ma2'] = float(row[44]) 
            element[i].xrf_abs_yield['mb'] = float(row[45]) 
            element[i].xrf_abs_yield['mg'] = float(row[46]) 
            
            if len(row) > 46 : 
                element[i].density = float(row[47]) 
                element[i].mass = float(row[48]) 

                element[i].bindingE['K'] = float(row[49]) 

                element[i].bindingE['L1'] = float(row[50]) 
                element[i].bindingE['L2'] = float(row[51]) 
                element[i].bindingE['L3'] = float(row[52]) 

                element[i].bindingE['M1'] = float(row[53]) 
                element[i].bindingE['M2'] = float(row[54]) 
                element[i].bindingE['M3'] = float(row[55]) 
                element[i].bindingE['M4'] = float(row[56]) 
                element[i].bindingE['M5'] = float(row[57]) 

                element[i].bindingE['N1'] = float(row[58]) 
                element[i].bindingE['N2'] = float(row[59]) 
                element[i].bindingE['N3'] = float(row[60]) 
                element[i].bindingE['N4'] = float(row[61]) 
                element[i].bindingE['N5'] = float(row[62]) 
                element[i].bindingE['N6'] = float(row[63]) 
                element[i].bindingE['N7'] = float(row[64]) 

                element[i].bindingE['O1'] = float(row[65]) 
                element[i].bindingE['O2'] = float(row[66]) 
                element[i].bindingE['O3'] = float(row[67]) 
                element[i].bindingE['O4'] = float(row[68]) 
                element[i].bindingE['O5'] = float(row[69]) 

                element[i].bindingE['P1'] = float(row[70]) 
                element[i].bindingE['P2'] = float(row[71]) 
                element[i].bindingE['P3'] = float(row[72]) 


                element[i].jump['K'] = float(row[73]) 

                element[i].jump['L1'] = float(row[74]) 
                element[i].jump['L2'] = float(row[75]) 
                element[i].jump['L3'] = float(row[76]) 

                element[i].jump['M1'] = float(row[77]) 
                element[i].jump['M2'] = float(row[78]) 
                element[i].jump['M3'] = float(row[79]) 
                element[i].jump['M4'] = float(row[80]) 
                element[i].jump['M5'] = float(row[81]) 

                element[i].jump['N1'] = float(row[82]) 
                element[i].jump['N2'] = float(row[83]) 
                element[i].jump['N3'] = float(row[84]) 
                element[i].jump['N4'] = float(row[85]) 
                element[i].jump['N5'] = float(row[86]) 

                element[i].jump['O1'] = float(row[87]) 
                element[i].jump['O2'] = float(row[88]) 
                element[i].jump['O3'] = float(row[89]) 
          
          
        f.close()

        return element
        
        

#-----------------------------------------------------------------------------
class Cfits:
    def __init__(self):   
        
        
        self.elements = None
  
  
#-----------------------------------------------------------------------------  
    def calc_fits(self, datastack, elementstofit, calibslope, caliboffset, incidenteng): 
        
        
        me = Celements()
        info_elements = me.get_element_info()
        
        fp = fit_parameters.Cfit_parameters()
        fitp = fp.define_fitp(info_elements)
        
 
        keywords = fitp.keywords
        
        dims = datastack.shape
        
        
        n_cols = dims[0]
        n_rows = dims[1]
        
        if datastack.ndim > 3:
            no_detectors = dims[2]
            nchannels = dims[3]
        else:
            no_detectors = 1
            nchannels =  dims[2]
            
            
        
        use_fit = 1
        if use_fit > 0:
            
            fit = spectrum_fitting.Cfitspectrum()
            
            seconds_fit_start = tm.time()
            # note: spectral binning needs to be even !!
            #data_temp = np.zeros((nchannels))
            data_line = np.zeros((nchannels,  n_rows))
            fitted_line = np.zeros((nchannels, n_rows))
            ka_line = np.zeros((nchannels, n_rows))
            kb_line = np.zeros((nchannels, n_rows))
            l_line = np.zeros((nchannels, n_rows))
            bkground_line = np.zeros((nchannels, n_rows))
            

                    
            fitted_temp = np.zeros((nchannels, no_detectors+1))
            Ka_temp = np.zeros((nchannels, no_detectors+1))
            Kb_temp = np.zeros((nchannels, no_detectors+1))
            l_temp = np.zeros((nchannels, no_detectors+1))
            bkground_temp = np.zeros((nchannels, no_detectors+1))
            raw_temp = np.zeros((nchannels, no_detectors+1))
            
#             add_plot_spectra = np.zeros((nchannels, 12, n_rows), dtype = np.float32)
#             temp_add_plot_spectra = np.zeros((nchannels, 12, n_rows), dtype = np.float32)
#             add_plot_names = ['fitted', 'K alpha', 'background', 'K beta', 'L lines', 'M lines', 'step', 'tail', 'elastic', 'compton', 'pileup', 'escape']
        
            values = np.zeros((n_cols, n_rows, fitp.g.n_fitp), dtype = np.float32)
            values_line = np.zeros((n_rows, fitp.g.n_fitp), dtype = np.float32)
            bkgnd = np.zeros((n_cols, n_rows), dtype = np.float32)
            bkgnd_line = np.zeros((n_rows), dtype = np.float32)
            tfy = np.zeros((n_cols, n_rows), dtype = np.float32)
            tfy_line = np.zeros((n_rows), dtype = np.float32)
            sigma = np.zeros((n_cols, n_rows, fitp.g.n_fitp), dtype = np.float32)       
            elt_line = np.ones((n_rows), dtype = np.float32)
            
            fitmaps = []
            
            for kkplus in range(no_detectors+1): 
            
                if kkplus == 0:
                    kk = 0
                else:
                    kk = kkplus-1
            
 
                matrix = 1
                temp = 0
             
                first = 1
                fitp.g.no_iters = 4
                string_only = 0
                filename = ' '
           
                fitp.s.use[:] = 1     
                
                fitp.s.val[min(fitp.keywords.kele_pos):max(fitp.keywords.mele_pos)-1] = 1e-10
                # execute below if do fixed fit per pixel
                if use_fit == 1 : 
                    for j in range(fitp.keywords.kele_pos[0]):
                        fitp.s.use[j] = fitp.s.batch[j,1]
                    # if matrix is not 1, then global variable NO_MATRIX is used to
                    # override, and will keep energy calibration floating. at every pixel
                    if matrix == 0 :
                        for j in range(fitp.keywords.kele_pos[0]):
                            fitp.s.use[j] = fitp.s.batch[j,4]
                    det = kk
                    pileup_string = ''
                    test_string = ''
                    #print 'maps_overridefile', maps_overridefile
                    #for ie in range(len(info_elements)): print info_elements[ie].xrf_abs_yield
                    maps_overridefile = 'maps_fit_parameters_override.txt'
                    fitp, test_string, pileup_string = fp.read_fitp(maps_overridefile, info_elements, 0)

                    #Use the elements we defined in the GUI
                    test_string = []
                    for item in elementstofit:
                        test_string.append(item.replace('_K',''))

                    for jj in range(fitp.g.n_fitp): 
                        if fitp.s.name[jj] in test_string :
                            fitp.s.val[jj] = 1.
                            fitp.s.use[jj] = 5
                            if temp == 0:
                                temp = jj 
                            else:
                                temp = [temp, jj]        
                                              
                                              
                    #Energy Calibration parameters from the GUI
                    fitp.s.val[keywords.energy_pos[0]] = caliboffset             
                    fitp.s.max[keywords.energy_pos[0]] = caliboffset + 0.5
                    fitp.s.min[keywords.energy_pos[0]] = caliboffset - 0.5
                       
                    fitp.s.val[keywords.energy_pos[1]] = calibslope
                    fitp.s.max[keywords.energy_pos[1]] = calibslope + 0.2
                    fitp.s.min[keywords.energy_pos[1]] = calibslope + 0.2
        
                    #Incident energy in keV
                    fitp.s.val[keywords.coherent_pos[0]] = incidenteng
                    fitp.s.max[keywords.coherent_pos[0]] = incidenteng+1.0
                    fitp.s.min[keywords.coherent_pos[0]] = incidenteng-1.0                                               
           
                    calib = {'off':0., 'lin':0., 'quad':0.}
                    calib['off'] =  caliboffset
                    calib['lin'] = calibslope
                    calib['quad'] = 0

                    fp.parse_pileupdef(fitp, pileup_string, info_elements)
                    
              
                add_matrixfit_pars = np.zeros((6))
                add_matrixfit_pars[0] = fitp.s.val[fitp.keywords.energy_pos[0]]
                add_matrixfit_pars[1] = fitp.s.val[fitp.keywords.energy_pos[1]]
                add_matrixfit_pars[2] = fitp.s.val[fitp.keywords.energy_pos[2]]
                add_matrixfit_pars[3] = fitp.s.val[fitp.keywords.added_params[1]]
                add_matrixfit_pars[4] = fitp.s.val[fitp.keywords.added_params[2]]
                add_matrixfit_pars[5] = fitp.s.val[fitp.keywords.added_params[3]]
                      
      
                old_fitp = fp.define_fitp(info_elements)
                old_fitp.s.val[:]=fitp.s.val[:]
                
                
 
#                 if (no_processors_to_use > 1) :
#                     print 'Multi-threaded fitting started'
#                     print 'no_processors_to_use = ', no_processors_to_use
#                     print 'cpu_count() = %d\n' % multiprocessing.cpu_count()
#                     #no_processors_to_use = multiprocessing.cpu_count() - 1
#                     #print 'new no_processors_to_use = ', no_processors_to_use
#                     print 'Creating pool with %d processes\n' % no_processors_to_use
#                     pool = multiprocessing.Pool(no_processors_to_use)                
#         
#         
#                     count = n_cols
#                     #count = 1
#                     data_lines = np.zeros((self.main['max_spec_channels'],  n_rows, n_cols))
#                     for i_fit in range(n_cols):
#                         for jj in range(n_rows):
#                             data_lines[0:scan.mca_arr[i_fit, jj, :].size, jj, i_fit] = scan.mca_arr[i_fit, jj, :]                    
#         
#                     output_dir = self.main['output_dir'] 
#         
# #                     #Single processor version for debugging 
# #                     for i_fit in range(count):
# #                         data_line = data_lines[:, :, i_fit]
# #                         print 'fitting row number ', i_fit, ' of ', count-1
# #                         elt_line[:] = elt1_arr[i_fit, :]
# #                            
# #                         for jj in range(n_rows):
# #                             fitted_temp[xmin:xmax+1, kk] = fitted_temp[xmin:xmax+1, kk] + fitted_line[xmin:xmax+1, jj]
# #                             Ka_temp[xmin:xmax+1, kk] = Ka_temp[xmin:xmax+1, kk] + ka_line[xmin:xmax+1, jj]
# #                             l_temp[xmin:xmax+1, kk] = l_temp[xmin:xmax+1, kk] + l_line[xmin:xmax+1, jj]
# #                             bkground_temp[xmin:xmax+1, kk] = bkground_temp[xmin:xmax+1, kk] + bkground_line[xmin:xmax+1, jj]
# #                             raw_temp[:, kk] = raw_temp[:, kk] + data_line[:, jj]
# #                                  
# #           
# #                         fitted_line, ka_line, l_line, bkground_line,  values_line, bkgnd_line, tfy_line, xmin, xmax = fit.fit_line(data_line, 
# #                                             output_dir, n_rows, matrix, spectral_binning, elt_line, values_line, bkgnd_line, tfy_line, 
# #                                             info_elements, fitp, fitp.add_pars, keywords, add_matrixfit_pars, xrf_bin, calib )
# #           
# 
#                     
#                     print 'Started fitting'
#                     sys.stdout.flush()
# 
#                     results_pool = []
# #                     start = 29
# #                     count = 4
# #                     for i_fit in range(start,33):
#                     start = 0
#                     for i_fit in range(count):
#                         data_line = data_lines[:, :, i_fit]
#                         #print 'fitting row number ', i_fit, ' of ', count
#                         elt_line[:] = elt1_arr[i_fit, :]
#                         
#                         fitp.s.val[:]=old_fitp.s.val[:]
#                         
#                         
#                         if (xrf_bin > 0) and (i_fit < count -2) : 
#                             if (xrf_bin == 2) and (n_cols > 5) and (n_rows > 5) :
#                                 if i_fit % 2 != 0 : 
#                                     continue
#       
#                             if (xrf_bin == 4) and (n_cols > 5) and (n_rows > 5) :
#                                 if i_fit % 3 != 0: 
#                                     continue
#                                 
#                         for jj in range(n_rows):
#                             raw_temp[:, kk] = raw_temp[:, kk] + data_line[:, jj]
#   
#                         results_pool.append(pool.apply_async(fit_line_threaded, (i_fit, data_line, 
#                                         output_dir, n_rows, matrix, spectral_binning, elt_line, values_line, bkgnd_line, tfy_line, 
#                                         info_elements, fitp, old_fitp, fitp.add_pars, keywords, add_matrixfit_pars, xrf_bin, calib)) )
#                     
#                                                         
#                     results = []
#                     for r in results_pool:
#                         results.append(r.get())
#       
#                     pool.terminate()
#                     pool.join()    
#                   
#                     for iline in range(count):
#                         results_line = results[iline]   
#                         #print 'results_line=', results_line  
#                         fitted_line = results_line[0]
#                         ka_line = results_line[1]
#                         l_line = results_line[2]
#                         bkground_line = results_line[3]
#                         values_line = results_line[4]
#                         bkgnd_line = results_line[5]
#                         tfy_line = results_line[6]
#                         xmin = results_line[7]
#                         xmax = results_line[8]  
#                         values[start+iline, :, :] = values_line[:, :]
#                         bkgnd[start+iline, :] = bkgnd_line[:]
#                         tfy[start+iline, :] = tfy_line[:]      
#                                  
#                         for jj in range(n_rows):
#                             fitted_temp[xmin:xmax+1, kk] = fitted_temp[xmin:xmax+1, kk] + fitted_line[xmin:xmax+1, jj]
#                             Ka_temp[xmin:xmax+1, kk] = Ka_temp[xmin:xmax+1, kk] + ka_line[xmin:xmax+1, jj]
#                             l_temp[xmin:xmax+1, kk] = l_temp[xmin:xmax+1, kk] + l_line[xmin:xmax+1, jj]
#                             bkground_temp[xmin:xmax+1, kk] = bkground_temp[xmin:xmax+1, kk] + bkground_line[xmin:xmax+1, jj]
#                             #raw_temp[:, kk] = raw_temp[:, kk] + data_line[:, jj]
#                 
#                     print 'before', thisdata.energy_fit[0, kk]
#                         
#                     thisdata.energy_fit[0, kk] = calib['off']
#                     thisdata.energy_fit[1, kk] = calib['lin']
#                     thisdata.energy_fit[2, kk] = calib['quad']  
#                     
#                     print 'after', thisdata.energy_fit[0, kk] 
# 
# #                         import matplotlib.pyplot as plt 
# #                         plt.plot(x,test)
# #                         #plt.plot(x,y)
# #                         #plt.semilogy(x,y+0.1)
# #                         #plt.show()
# #                         #plt.semilogy(x,fit+0.1)
# #                         plt.show()
                     
                #else:
                if True:
                      
                    #count = 1
                    #for i_fit in range(29,33):    #Used for testing
                    count = n_cols
                    #for i_fit in range(count):
                    QtGui.qApp.processEvents()
                    if kkplus == 0:
                        pbstr = 'Initializing fit parameters'
                    else:
                        pbstr = "Processing Detector "+str(kkplus)
                        
                    for i_fit in progress(range(count), pbstr, "Cancel"):
                        
                        print 'fitting row number ', i_fit, ' of ', count
        
                                
                        #This line was a major BUG!
                        #data_line[:, :] = 0.
                        
#                         for jj in range(n_rows):
#                             #data_line[0:scan.mca_arr[i_fit, jj, :].size, jj] = scan.mca_arr[i_fit, jj, :]
#                             data_line[0:scan.mca_arr[i_fit, jj, :].size, jj] = scan.mca_arr[i_fit, jj, :]
  
                        
                        if datastack.ndim > 3:
                            data_line = datastack[i_fit,:,kk,:].T
                        else:
                            data_line = datastack[i_fit,:,:].T
                            
                               
                        output_dir = os.getcwd()

                        
                        spectral_binning = 0
                        xrf_bin = 0
                        fitted_line, ka_line, kb_line, l_line, bkground_line,  values_line, bkgnd_line, tfy_line, xmin, xmax, parameternames = fit.fit_line(data_line, 
                                                output_dir, n_rows, matrix, spectral_binning, elt_line, values_line, bkgnd_line, tfy_line, 
                                                info_elements, fitp, old_fitp, fitp.add_pars, keywords, add_matrixfit_pars, xrf_bin, calib )
                      
#                         counts_background, counts_ka, counts_kb, counts_l, counts_m, \
#                         counts_elastic, counts_compton, counts_step, counts_tail, \
#                         counts_pileup, counts_escape = fit.get_counts()

                        
                        for jj in range(n_rows):
                            fitted_temp[xmin:xmax+1, kk] = fitted_temp[xmin:xmax+1, kk] + fitted_line[xmin:xmax+1, jj]
                            Ka_temp[xmin:xmax+1, kk] = Ka_temp[xmin:xmax+1, kk] + ka_line[xmin:xmax+1, jj]
                            Kb_temp[xmin:xmax+1, kk] = Kb_temp[xmin:xmax+1, kk] + kb_line[xmin:xmax+1, jj]
                            l_temp[xmin:xmax+1, kk] = l_temp[xmin:xmax+1, kk] + l_line[xmin:xmax+1, jj]
                            bkground_temp[xmin:xmax+1, kk] = bkground_temp[xmin:xmax+1, kk] + bkground_line[xmin:xmax+1, jj]
                            raw_temp[:, kk] = raw_temp[:, kk] + data_line[:, jj]
                            
                        values[i_fit, :, :] = values_line[:, :]
                        bkgnd[i_fit, :] = bkgnd_line[:]
                        tfy[i_fit, :] = tfy_line[:]  


                if kkplus > 0:
                    thisfitmap = np.zeros((n_cols,n_rows,len(parameternames)))
                    
                    for i_fit in range (n_cols): 
                        for j_fit in range(n_rows): 
                            for jj in range(len(parameternames)): 
                                if parameternames[jj] in fitp.s.name:
                                    wo = np.where(fitp.s.name == parameternames[jj])[0]
        #                         else:
        #                             if parameternames[jj,1] == 's_e' :
        #                                 wo = np.where(fitp.s.name == 'coherent_sct_amplitude')[0]
        #                             if parameternames[jj,1] == 's_i' :
        #                                 wo = np.where(fitp.s.name == 'compton_amplitude')[0]
        #                             if parameternames[jj,1] == 's_a' :
        #                                 wo = np.concatenate((np.where(fitp.s.name == 'compton_amplitude')[0], np.where(fitp.s.name == 'coherent_sct_amplitude')[0]), axis=0)
                          
                                if len(wo) == 0 : continue
                                thisfitmap[i_fit, j_fit, jj] = np.sum(values[i_fit, j_fit, wo])
                   
                  
         
        #                     for ie in range(len(elementstofit)):
        #                         if 'TFY' == make_maps_conf.chan[elements_to_use[ie]].name:
        #                             thisdata.dataset_orig[i_fit, j_fit, ie, 1] = tfy[i_fit, j_fit]
        #                         if 'Bkgnd' == make_maps_conf.chan[elements_to_use[ie]].name:
        #                             thisdata.dataset_orig[i_fit, j_fit, ie, 1] = bkgnd[i_fit, j_fit]
                
                    fitmaps.append(thisfitmap)

            seconds_fit_end = tm.time()
            print 'fitting of this scan  finished in ',  seconds_fit_end-seconds_fit_start, ' seconds'
            
            fitted_temp = np.sum(fitted_temp, axis=1)
            Ka_temp = np.sum(Ka_temp, axis=1)
            Kb_temp = np.sum(Kb_temp, axis=1)
            l_temp = np.sum(l_temp, axis=1)
            bkground_temp = np.sum(bkground_temp, axis=1)
            
            
            return fitmaps, parameternames, fitted_temp, Ka_temp, Kb_temp, l_temp, bkground_temp, info_elements
                    

#             for kk in range(no_detectors): 
#                 name_pre = 'fit_'
#                 if kk == no_detectors :
#                     name_after = '_integrated' 
#                 else:
#                     name_after = '_det'+str(kk).strip()
#                     
#                 spectra[self.main_max_spectra-8].data[:] = fitted_temp[:, kk]
#                 spectra[self.main_max_spectra-7].data[:] = Ka_temp[:, kk]
#                 spectra[self.main_max_spectra-4].data[:] = bkground_temp[:, kk]
#                 spectra[0].data[:] = raw_temp[:, kk]
#                 spectra[0].name = name_pre+header+name_after
#                 # need to be in here for B station, for files w/o standards
#                 if beamline == '2-ID-B' :
#                     spectra[self.main_max_spectra-8].name = 'fitted'
#                     spectra[self.main_max_spectra-7].name = 'alpha'
#                     spectra[self.main_max_spectra-4].name = 'background'
#         
#                 spectra[0].used_chan = raw_temp[:, 0].size 
#                 spectra[0].calib['off'] = calib['off']
#                 spectra[0].calib['lin'] = calib['lin']
#                 if spectral_binning >0 : spectra[0].calib['lin'] = spectra[0].calib['lin'] * spectral_binning
#                 spectra[0].calib['quad'] = calib['quad']
#             
#                 for isp in range(self.main_max_spectra-8,self.main_max_spectra-3):
#                     spectra[isp].used_chan = spectra[0].used_chan 
#                     spectra[isp].calib['off'] = spectra[0].calib['off']
#                     spectra[isp].calib['lin'] = spectra[0].calib['lin']
#                     spectra[isp].calib['quad'] = spectra[0].calib['quad']
#                 # need to be in here for B station, for files w/o standards
#                 if beamline == '2-ID-B' : 
#                     names = spectra[np.where(spectra.name != '')].name
#                     names.insert(0, 'none')
#                     n_names =len(names)
#             
#                  
#                 temp_this_max = max_chan_spec[:, 0].size
#                 temp = np.repeat(fitted_temp[:, 0], 2)
#                 max_chan_spec[0:temp_this_max, 2] = temp[0:temp_this_max]
#                 temp = np.repeat(Ka_temp[:, 0], 2)
#                 max_chan_spec[0:temp_this_max, 3] = temp[0:temp_this_max]
#                 temp = np.repeat(bkground_temp[:, 0], 2)
#                 max_chan_spec[0:temp_this_max, 4] = temp[0:temp_this_max]
#                  
#                 add_plot_spectra[:, 0, kk] = fitted_temp[:, kk]
#                 add_plot_spectra[:, 1, kk] = Ka_temp[:, kk]
#                 add_plot_spectra[:, 2, kk] = bkground_temp[:, kk]
#                 add_plot_spectra[:, 4, kk] = l_temp[:, kk]        
#                 this_add_plot_spectra = np.zeros((nchannels, 12))
#                 this_add_plot_spectra[:, :] = add_plot_spectra[:, :, kk]
#                 self.plot_fit_spec(info_elements, spectra = spectra, add_plot_spectra = this_add_plot_spectra, add_plot_names = add_plot_names, fitp = fitp)
#                  


    
#-----------------------------------------------------------------------------    
    def do_fits(self, fitp, spectra, elementstofit, incidenteng, calibslope, caliboffset, per_pix = 0, generate_img = 0, maxiter = 500, suffix = '', info_elements = 0):
         
        this_w_uname = "DO_MATRIX_FIT"
        
  
        keywords = fitp.keywords

        fp = fit_parameters.Cfit_parameters()
        avg_fitp = fp.define_fitp(info_elements)
 
        avg_fitp.s.val[:] = 0.
        avg_n_fitp = 0.   
         
        max_spectra = 4096
        
        max_spec_channels = spectra[0].used_chan
 
             
        used_chan = []
        for i in range(max_spectra-11):
            used_chan.append(spectra[i].used_chan)
        wo = np.where(np.array(used_chan) > 0)[0]
        tot_wo = len(wo)
        #print 'fiting n spectra', tot_wo
        if tot_wo == 0 : return  
        names = ['none']
        for i in range(len(spectra)):
            if spectra[i].name != '': names.append(spectra[i].name)
 
 
         
        #n_names = len(names)
        # now go one by one through all spectra loaded into the plot_spec window
        for i in range(tot_wo):    
            old_fitp = fp.define_fitp(info_elements)
            old_fitp.s.val[:]=fitp.s.val[:]
            
            for j in range(keywords.kele_pos[0]):  fitp.s.use[j] = fitp.s.batch[j,0]
     
            if spectra[wo[i]].date['year'] == 0 : spectra[wo[i]].date['year'] = 1. # avoid error of julday routine for year number zero
            full_test_string = ['Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 
                          'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 
                          'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 
                          'In', 'Sn', 'Sb', 'Te', 'I', 'Cd_L', 'I_L', 'Cs_L', 'Ba_L', 'Pt_L', 'Au_L', 'Hg_L', 'Pb_L', 'Pt_M',
                          'Au_M', 'U_M', 'Pu_L', 'Pb_M'] 
            # first disable fitting for all elements, and set the values to be
            # close to zero (i.e., set fit pars small)
            fitp.s.val[keywords.kele_pos] = 1e-10
            fitp.s.val[keywords.mele_pos] = 1e-10
            fitp.s.val[keywords.lele_pos] = 1e-10
            fitp.s.use[keywords.kele_pos] = 1.
            fitp.s.use[keywords.mele_pos] = 1.
            fitp.s.use[keywords.lele_pos] = 1.
                          
            # now enable selected elements, either from read file, or from
            # knowning it is an nbs standard
            #print 'spectra[wo[i]].name', spectra[wo[i]].name
            if 'nbs' in spectra[wo[i]].name:
                if '32' in spectra[wo[i]].name:
                    test_string = ['Al', 'Si', 'Ar', 'Ca', 'V', 'Mn', 'Co', 'Cu']
                    for jj in range(fitp.g.n_fitp) : 
                        if fitp.s.name[jj] in test_string:
                            #wo_a = test_string.index(fitp.s.name[jj])
                            fitp.s.val[jj] = 1.
                            fitp.s.use[jj] = 5
                if '33' in spectra[wo[i]].name:
                    test_string = ['Si', 'Pb_M', 'Ar', 'K', 'Ti', 'Fe', 'Zn', 'Pb_L']
                    for jj in range(fitp.g.n_fitp) : 
                        if fitp.s.name[jj] in test_string:
                            #wo_a = test_string.index(fitp.s.name[jj])
                            fitp.s.val[jj] = 1.
                            fitp.s.use[jj] = 5
 
            else: 
                print 'fitting spectrum'
                # if not NBS standard then look here
                det = 0   
                try:
                    maps_overridefile = 'maps_fit_parameters_override.txt'
                    fitp, test_string, pileup_string = fp.read_fitp(maps_overridefile, info_elements, det=det)
                    which_par_str = 'found override file (maps_fit_parameters_override.txt). Using the contained parameters.', test_string
                except:        
                    print 'warning: did not find override file (maps_fit_parameters_override.txt). Will abort this action'
                    return

                #Use the elements we defined in the GUI
                test_string = []
                for item in elementstofit:
                    if ('_L' in item) and (item not in full_test_string):
                        test_string.append(item.replace('_L',''))
                    else:
                        test_string.append(item.replace('_K',''))
                                      
                for jj in range(fitp.g.n_fitp) : 
                    if fitp.s.name[jj] in test_string:
                        #wo_a = test_string.index(fitp.s.name[jj])
                        fitp.s.val[jj] = 1.
                        fitp.s.use[jj] = 5
 
#             temp_fitp_use = fitp.s.use[np.amin(fitp.keywords.kele_pos):np.amax(fitp.keywords.mele_pos)+1]
#             temp_fitp_name = fitp.s.name[np.amin(fitp.keywords.kele_pos):np.amax(fitp.keywords.mele_pos)+1]
#             which_elements_to_fit = (np.nonzero(temp_fitp_use != 1))[0]
#             print 'elements to fit:'
#             print temp_fitp_name[which_elements_to_fit]  
              
            #Energy Calibration parameters from the GUI
            fitp.s.val[keywords.energy_pos[0]] = caliboffset             
            fitp.s.max[keywords.energy_pos[0]] = caliboffset + 0.5
            fitp.s.min[keywords.energy_pos[0]] = caliboffset - 0.5
               
            fitp.s.val[keywords.energy_pos[1]] = calibslope
            fitp.s.max[keywords.energy_pos[1]] = calibslope + 0.2
            fitp.s.min[keywords.energy_pos[1]] = calibslope + 0.2

            #Incident energy in keV
            fitp.s.val[keywords.coherent_pos[0]] = incidenteng
            fitp.s.max[keywords.coherent_pos[0]] = incidenteng+1.0
            fitp.s.min[keywords.coherent_pos[0]] = incidenteng-1.0
  
  
            det = 0   
            pileup_string = ''
#             try:
#                 fitp, test_string, pileup_string = fp.read_fitp(maps_overridefile, info_elements, det=det)
#             except:
#                 print 'error reading fit paramenters'
                      
            if avg_n_fitp == 0 :
                # make sure that avg_fitp gets redefined here, so that changes,
                # etc, in the override file get translateed into the avg file on
                # the first round

                avg_fitp = fp.define_fitp(info_elements)
                avg_fitp.s.val[:] = 0.
        
            if (this_w_uname == "DO_FIT_ALL_W_TAILS") :                 
                for j in range(keywords.kele_pos[0]) :  fitp.s.use[j] = fitp.s.batch[j,2]
            if (this_w_uname == "DO_MATRIX_FIT") :                  
                for j in range(keywords.kele_pos[0]):  fitp.s.use[j] = fitp.s.batch[j,1]
            if (this_w_uname == "DO_FIT_ALL_FREE") :                
                for j in range(keywords.kele_pos[0]):  fitp.s.use[j] = fitp.s.batch[j,3] 
            if (this_w_uname == "DO_FIT_ALL_FREE_E_FIXED_REST") :                  
                for j in range(keywords.kele_pos[0]):  fitp.s.use[j] = fitp.s.batch[j,4]
     
            fp.parse_pileupdef(fitp, pileup_string, info_elements)
 
   
            if (this_w_uname == "DO_MATRIX_FIT") :
                matrix = 1 
            else: 
                matrix = 0
            first = 1   
 
             
            temp_fitp_use = fitp.s.use[np.amin(fitp.keywords.kele_pos):np.amax(fitp.keywords.mele_pos)+1]
            temp_fitp_name = fitp.s.name[np.amin(fitp.keywords.kele_pos):np.amax(fitp.keywords.mele_pos)+1]
            which_elements_to_fit = (np.nonzero(temp_fitp_use != 1))[0]
            print 'elements to fit:'
            print temp_fitp_name[which_elements_to_fit]  
         
            fit = spectrum_fitting.Cfitspectrum()
            u, fitted_spec, background, xmin, xmax, perror, parameternames = fit.fit_spectrum(fitp, spectra[wo[i]].data, spectra[wo[i]].used_chan, spectra[wo[i]].calib, 
                            first = first, matrix = matrix, maxiter = maxiter)
 
 
            
            counts_background, counts_ka, counts_kb, counts_l, counts_m, \
            counts_elastic, counts_compton, counts_step, counts_tail, \
            counts_pileup, counts_escape = fit.get_counts()
 
            if (this_w_uname == "DO_FIT_ALL_FREE") :                  
                fitp.s.val[:] = u[:]       
                fitp.s.val[keywords.peaks] = 10.0**u[keywords.peaks]
                for j in range(keywords.kele_pos[0]): fitp.s.use[j] = fitp.s.batch[j, 3]
                u, fitted_spec, background, xmin, xmax, perror = fit.fit_spectrum(fitp, spectra[wo[i]].data, spectra[wo[i]].used_chan, spectra[wo[i]].calib, 
                                                                                  first = first, matrix = matrix, maxiter = maxiter)                
 
                counts_background, counts_ka, counts_kb, counts_l, counts_m, \
                counts_elastic, counts_compton, counts_step, counts_tail, \
                counts_pileup, counts_escape = fit.get_counts()
             
            add_plot_spectra = np.zeros((max_spec_channels, 12))
            add_plot_names = ['fitted', 'K alpha', 'background', 'K beta', 'L lines', 'M lines', 'step', 'tail', 'elastic', 'compton', 'pileup', 'escape']
     
            add_plot_spectra[xmin:xmax+1, 0] = fitted_spec[:]
            add_plot_spectra[xmin:xmax+1, 1] = counts_ka[:]
            add_plot_spectra[0:np.amin([spectra[wo[i]].used_chan, len(background)-1]), 2] = background[0:np.amin([spectra[wo[i]].used_chan, len(background)-1])] 
            add_plot_spectra[xmin:xmax+1, 3] = counts_kb[:]
            add_plot_spectra[xmin:xmax+1, 4] = counts_l[:]
            add_plot_spectra[xmin:xmax+1, 5] = counts_m[:]
            add_plot_spectra[xmin:xmax+1, 6] = counts_step[:]
            add_plot_spectra[xmin:xmax+1, 7] = counts_tail[:]
            add_plot_spectra[xmin:xmax+1, 8] = counts_elastic[:]
            add_plot_spectra[xmin:xmax+1, 9] = counts_compton[:]
            add_plot_spectra[xmin:xmax+1, 10] = counts_pileup[:]
            add_plot_spectra[xmin:xmax+1, 11] = counts_escape[:]
     
 
            fitp.s.val[:] = u[:]       
            fitp.s.val[keywords.peaks] = 10.0**u[keywords.peaks]
 
            # this is not quite correct. the 1 sigma values are calculated for
            # the fit pars, which is used in the exponential. to translate them
            # into a meaning ful number, just calculate the upper bound and call
            # that +/- error
            perror[keywords.peaks] = 10.0**(perror[keywords.peaks] + u[keywords.peaks]) - 10.0**u[keywords.peaks]
     
            spectra[wo[i]].calib['off'] = fitp.s.val[keywords.energy_pos[0]]
            spectra[wo[i]].calib['lin'] = fitp.s.val[keywords.energy_pos[1]]
            spectra[wo[i]].calib['quad'] = fitp.s.val[keywords.energy_pos[2]]

            
            spectra[max_spectra-8].data[:] = 0.
            spectra[max_spectra-8].data[xmin:xmax+1] = fitted_spec[:]
            spectra[max_spectra-8].name = 'fitted'
            for isp in range(max_spectra-8,max_spectra-3):
                spectra[isp].used_chan = spectra[wo[i]].used_chan 
                spectra[isp].calib['off'] = spectra[wo[i]].calib['off'] 
                spectra[isp].calib['lin'] = spectra[wo[i]].calib['lin']
                spectra[isp].calib['quad'] = spectra[wo[i]].calib['quad']
            spectra[max_spectra-7].data[:] = 0.
            spectra[max_spectra-7].data[xmin:xmax+1] = counts_ka[:]
            spectra[max_spectra-7].name = 'ka_only'
            spectra[max_spectra-6].data[:] = 0.
            spectra[max_spectra-6].data[xmin:xmax+1] = counts_kb[:]
            spectra[max_spectra-6].name = 'kb_only'
            spectra[max_spectra-5].data[:] = 0.
            spectra[max_spectra-5].data[xmin:xmax+1] = counts_tail[:]
            spectra[max_spectra-5].name = 'tails'
            spectra[max_spectra-4].data[:] = 0.
            spectra[max_spectra-4].data[0:np.amin([spectra[wo[i]].used_chan, len(background)-1])] = background[0:np.amin([spectra[wo[i]].used_chan, len(background)-1])] 
            spectra[max_spectra-4].name = 'background' 
 
#             filename = 'specfit_'+names[wo[i]+1]+suffix
#             maps_tools.plot_spectrum(info_elements, spectra = spectra, i_spectrum = wo[i], add_plot_spectra = add_plot_spectra, 
#                                      add_plot_names = add_plot_names, ps = 0, fitp = fitp, filename= filename,
#                                      outdir = self.main['output_dir'])
#      
# 
# 
#             if per_pix == 0 : 
# 
#                 dirt = self.main['output_dir']
#                 if not os.path.exists(dirt):   
#                     os.makedirs(dirt)
#                     if not os.path.exists(dirt):
#                         print 'warning: did not find the output directory, and could not create a new output directory. Will abort this action'
#                         return 0
#             else:
#                 if generate_img > 0 :
#                     filename = os.path.join(self.main['output_dir'],'fit_'+names[wo[i]+1])
#                     #write_spectrum, main.output_dir+strcompress('fit_'+names[wo[i]+1]), spectra, droplist_spectrum
# 
# 
#             avg_fitp.s.val[:] = avg_fitp.s.val[:] + fitp.s.val[:]
#             avg_n_fitp = avg_n_fitp + 1
#   
#         
#         avg_fitp.s.val[:] = avg_fitp.s.val[:]/avg_n_fitp
#         avg_fitp.s.max[:] = fitp.s.max[:]
#         avg_fitp.s.min[:] = fitp.s.min[:]
#         avgfilename = os.path.join(self.main['master_dir'],'average_resulting_maps_fit_parameters_override.txt')
#         fp.write_fit_parameters(self.main, avg_fitp, avgfilename, test_string, pileup_string = pileup_string, suffix = suffix)
                                 
                                    
        return fitp, avg_fitp, spectra, add_plot_spectra, add_plot_names, fit.fitinfo
 

