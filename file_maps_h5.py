'''
Created on Jul 22, 2015


@author: Mirna Lerotic, 2nd Look Consulting
http://2ndlookconsulting.com
'''
from __future__ import division

import numpy as np
import os
import shutil
import h5py
import datetime


#-----------------------------------------------------------------------------
class Cmapsh5:
    
#-----------------------------------------------------------------------------  
    def read_h5(self, filename): 
        
        print 'getting data from MAPS file ', filename
        
        
        inenergy = 12000.0
        realX = 0
        realY = 0
        roimaps = []
        roiinfo = []
        
        
        
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
        

        #Information HDF5 group
        if 'MAPS' in f:
            mapsgrp = f['MAPS']

            ds = mapsgrp['version']
            version = ds[...]
            
            #print 'version = ', version        
        
            if 'XRF_fits' in mapsgrp:
                dsx = mapsgrp['XRF_fits']
                xrffits = dsx[...]

                
            if 'mca_arr' in mapsgrp:
                dsx = mapsgrp['mca_arr']
                fullSpectrum = dsx[...]
                fullSpectrum = np.transpose(fullSpectrum, (1,2,0))

            if 'x_axis' in mapsgrp:
                dsx = mapsgrp['x_axis']
                x_axis = dsx[...]
                realX = x_axis     
                
            if 'y_axis' in mapsgrp:
                dsx = mapsgrp['y_axis']
                y_axis = dsx[...]
                realY = y_axis  
            else:
                realY = []
                
        # Close
        f.close()  


        
        if len(fullSpectrum.shape) > 3:
            allElementSum = np.sum(fullSpectrum, axis=3)
        else:
            allElementSum = fullSpectrum.copy()
        
#         print fullSpectrum.shape
#         print allElementSum.shape
#         print realX.shape
#         print realY.shape
        
        return allElementSum, fullSpectrum, inenergy, realX, realY, roimaps, roiinfo
    
    
    

#-----------------------------------------------------------------------------  
    def get_fits_h5(self, filename): 
        
        #print 'getting fits from ', filename
        
        
        xrffits = []
        channel_names = []
        
       
        # Open HDF5 file
        f = h5py.File(filename, 'r') 
    

        #Information HDF5 group
        if 'MAPS' in f:
            mapsgrp = f['MAPS']
     
        
            if 'XRF_fits' in mapsgrp:
                dsx = mapsgrp['XRF_fits']
                xrffits = dsx[...]
                #print 'read fits from a file'
                
            #Get channel names
            if 'channel_names' in mapsgrp:
                dsx = mapsgrp['channel_names']
                channel_names = dsx[...]
                #print channel_names
            
                
        # Close file
        f.close()  

        
        
        return xrffits, channel_names
    

